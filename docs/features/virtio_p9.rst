.. _VirtIO_P9_label:

Virtio-P9
=========

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of P9 filesystem
-------------------------

9P (or the Plan 9 Filesystem Protocol) is a network protocol developed for the
Plan 9 from Bell Labs distributed operating system as the means of connecting
the components of a Plan 9 system.  As mentioned at `lwn`_ 9P is somewhat
equivalent to NFS or CIFS, but with its own particular approach. It is not as
much a way of sharing files as a protocol definition aimed at the sharing of
resources in a networked environment. It works in a connection-oriented manner
in which each client makes one or more connections to the server(s) of interest.
The client can create file descriptors, use them to navigate around the filesystem,
read and write files, create, rename and delete files, and close things down.


Overview of Virtio-P9 device
----------------------------

Few Arm reference design Fixed Virtual Platforms (FVPs) for infrastructure implement
a subset of the Plan 9 file protocol over a virtio transport. This component is called
Virtio-P9 device and it enables accessing a directory on the host's filesystem within
Linux, or another operating system that implements the protocol, running on a platform
model. Put simply 9P filesystem protocol enables communicating the file I/O operations
between guest systems or clients and the 9p server.

Linux running on the host uses the `v9fs`_ which is a Unix implementation of the Plan 9
9p remote filesystem protocol, in conjunction with the virtio transport protocol to
allow filesystem I/O operations between host and the FVP.

As mentioned on the Arm Fixed Virtual Platform page (`FVP`_) the component implements
a subset of the Linux 9P2000.L protocol with the limitation that the guest can mount
only one host directory per instance of the component.


Build the platform software
---------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Refer to the :ref:`Busybox Boot <Busybox_boot_label>` page to build the
reference design platform software stack and boot into busybox on the Neoverse
RD FVP.


Running the test to validate Virtio-P9 device
---------------------------------------------

- To begin validating the Virtio-P9 device create a directory on the host Linux
  machine from which the target platform FVP is launched. This directory is used
  as a shared directory between the host and target FVP.

  ::

    mkdir /tmp/hostsharedir

- Copy few files that can be shared to the target platform into this hostsharedir.
  The files can be read/written from the booted target platform for validating
  Virtio-P9.

- To enable Virtio-P9 device on the platform pass the following additional parameter
  when launching the target platform FVP:

  ::

    -C board.virtio_p9.root_path=<Path_to_shared_dir>

Example,

  ::

    -C board.virtio_p9.root_path=/tmp/hostsharedir

- As mentioned in the :ref:`Busybox Boot <Busybox_boot_label>` guide boot to
  busybox using the commands mentioned below.

  ::

    ./boot.sh -p <platform name> -a <additional_params> -n [true|false]

Here the supported command line options are:

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -n [true|false] (optional)

   - Controls the use of network ports by the model. If network ports have
     to be enabled, use 'true' as the option. Default value is set to
     'false'.

*  -a <additional_params> (optional)

   - Specify any additional model parameters to be passed. The model
     parameters and the data to be passed to those parameters can be found
     in the FVP documentation.

Example command to boot a RD-N2-Cfg1 platform upto busybox prompt with Virtio-P9
device enabled:

  ::

    ./boot.sh -p rdn2cfg1 -a '-C board.virtio_p9.root_path=/tmp/hostsharedir'

- Once the platform is booted mount the 9P filesystem from busybox prompt:

  ::

    mount -t 9p -o trans=virtio,version=9p2000.L FM <mount_point>

Example,

  ::

    mount -t 9p -o trans=virtio,version=9p2000.L FM /mnt

- Now access the files present in the mounted /mnt directory and verify that the
  files can be read from and written to.

- Try creating a new test file in the mounted path to transfer some data from the
  booted target platform to the host PC.

  ::

    dmesg > /mnt/kernel_logs.txt

- Try to access the shared directory on the host PC to verify that the file created
  on the target platform is also visible in host PC.

  ::

    cat /tmp/hostsharedir/kernel_logs.txt

- Once the file accesses are validated between the host PC and target FVP platform
  unmont the 9P filesystem from the target platform's busybox prompt.

  ::

    umount /mnt


This completes the validation of Virtio-P9 component on Arm infrastructure reference
design platforms.


.. _lwn: https://lwn.net/Articles/137439/
.. _v9fs: https://www.kernel.org/doc/Documentation/filesystems/9p.txt
.. _FVP: https://developer.arm.com/documentation/100964/1119/Fast-Models-components/Peripheral-components/VirtioP9Device
