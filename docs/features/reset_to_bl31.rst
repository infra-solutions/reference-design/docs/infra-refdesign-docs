.. _reset_to_bl31_label:

AP Boot from BL31 (Reset to BL31 Flow)
======================================

.. important::
    This feature might not be applicable to all platforms. Please check
    individual platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of Reset to BL31
-------------------------

Trusted Firmware-A (TF-A) has three bootloader stages called BL1, BL2 and BL31.
BL1 and BL2 serve the purpose of platform initialization and loading other
firmware images (including BL31). BL31 is the runtime resident. TF-A has a
feature called RESET_TO_BL31 which allows the application processor to reset
directly to the BL31 stage, bypassing BL1 and BL2.

Building the platform software
------------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Reset to BL31 is disabled for all platforms by default. To build the platform
software for Reset to BL31 boot flow, an environment variable
$PLAT_RESET_AP_TO_BL31 needs to be set to 1. If this variable is not defined or
is set to 0, Reset to BL31 remains disabled. Any values other than 0 and 1 will
result in a build failure.

When $PLAT_RESET_AP_TO_BL31 is 1, all the AP binaries that are needed for the
setup of subsequent images are preloaded in the model.

For example, to build the software for Busybox boot, the commands are as
follows:

::

  export PLAT_RESET_AP_TO_BL31=1
  ./build-scripts/rdinfra/build-test-busybox.sh -p <platform name> <command>

Supported command line options are listed below.

* <platform name>

  - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - ``clean``
  - ``build``
  - ``package``
  - ``all`` (all of the three above)

Booting platforms with Reset to BL31 boot flow
----------------------------------------------

When platform software is built with $PLAT_RESET_AP_TO_BL31 set to 1, a file
called boot_info.sh is created in the platform's output directory. This file
contains the necessary information for BL31-based boot. No additional parameters
are needed to be passed for booting.
The boot commands remain the same as those specified under
:ref:`boot_index_label`
