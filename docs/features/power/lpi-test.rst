.. _low_power_idle_label:

ACPI Low Power Idle (LPI)
=========================

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of LPI test
--------------------

ACPI Low Power Idle (LPI) mechanism allows an operating system to manage the
power states of the processor power domain hierarchy. Neoverse Reference Design
platforms support the c-states c0 (run state), c1 (WFI) and c3 (WFI with core
powered down).

This document describes the procedure to validate LPI functionality, determining
the number of times a particular CPU core switched to idle state and the total
time the core has been in a idle state.


Download and build the required platform software
-------------------------------------------------

For downloading and building the platform firmware, refer :ref:`Buildroot boot
<Buildroot_boot_label>`. To enable LPI from ACPI, update the ``LPI_EN`` variable
from ``SgiPlatform.dsc.inc`` before build. Also remember to enable stress-ng
binary from the buildroot config.


Procedure for validating LPI states
-----------------------------------

1. Boot the platform to buildroot command line prompt.
2. Run the command 'nproc' to get the cpu count in the system.
3. Read the idle state descriptor entry to know about the c-state information.

   ::

      cat /sys/devices/system/cpu/cpu<x>/cpuidle/state<j>/desc

      Here, x = 0, 1, 2, ... (nproc -1)
            y = 0, 1, 2, ...

   generally for RD platform:

     state0: c1 (LPI1) state for CPUx

     state1: c3 (LPI3) state for CPUx

     state2: available only for plaforms having power control for CPU container
     and is the combined c3 (LPI3 for core and LPI2 for cluster) state for CPU
     and cluster.

4. To get the LPI statistics, read the 'usage' and 'time' entries:

   ::

      cat /sys/devices/system/cpu/cpu<x>/cpuidle/state<y>/usage
      cat /sys/devices/system/cpu/cpu<x>/cpuidle/state<y>/time

5. Wake up all CPUs from sleep. The example shown below uses the 'stress-ng'
   utility. Run stress-ng utility for one second for all CPUs using the command

   ::

      stress-ng -c <num_cpu> -t 1

      Here num_cpu is the value obtained on step 2

6. Repeat step 4 and compare the usage and time values.

In a system with idle states enabled, the expectation is the 'usage' count
should increment on each suspend-resume cycle. The value for 'time' specifies
the total time period the core was in that particular state.

.. note::
   In a system that supports state2, the usage count will increment for either
   state1 or for state2. This is applicable when a core is the last one to
   undergo sleep inside a container, then the core will request for a combined
   sleep state instead of core only power down.
