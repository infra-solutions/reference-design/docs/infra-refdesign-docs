.. _cppc_test_label:

Collaborative Processor Performance Control (CPPC)
==================================================

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of CPPC test
---------------------

Collaborative Processor Performance Control (CPPC) is a mechanism for the OS to
manage the performance of the processor core on a contiguous and abstract
performance scale. The CPPC support as implemented for Neoverse Reference Design
platforms requires the CPUs to support the Arm v8.4 AMU functionality. So the
support for CPPC is applicable for platforms that have Arm v8.4 or higher CPU.

The CPPC kernel framework has two parts, monitoring the CPU performance and
scale the CPU performance. In the monitoring part, the OS uses the AMU extension
which is introduced in ARMv8.4. Especially the 'Processor frequency counter'
and the 'Constant frequency counter'. For calculating the processor frequency,
the values of the processor frequency counter and constant frequency counter
are captured at two instances, say 2 microseconds between the instances and get
the delta between these two counts. The constant frequency is known hence the
processor frequency is calculated as:

::

  (delta processor frequency count / delta constant frequency count)
           * constant frequency

In the controlling part, the OS requests the desired performance to the platform
firmware through a non-secure channel between the OS and platform firmware.

This document focus on the procedure to validate CPPC functionality, obtaining
the CPU's current operating frequency, procedure to scale CPU frequency and the
scaling governor.


Download and build the required platform software
-------------------------------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

For downloading and building the platform firmware, refer :ref:`Busybox Boot
<Busybox_boot_label>` or :ref:`Buildroot boot <Buildroot_boot_label>`
documentation. To enable CPPC from ACPI, update the ``CPPC_EN=TRUE`` variable
from ``SgiPlatform.dsc.inc`` before build.

.. note::
   For :ref:`Busybox Boot <Busybox_boot_label>` or
   :ref:`Buildroot boot <Buildroot_boot_label>`, in Linux kernel defconfig
   ensure ``CONFIG_ACPI_CPPC_CPUFREQ=y``.

Changing the scaling governor
-----------------------------

For changing the frequency governor, the procedure is:

1. Boot the platform to command line prompt.
2. Read the scaling governor entry to get the current governor in action.

   ::

      cat /sys/devices/system/cpu/cpufreq/policy<x>/scaling_governor

      For RD platforms, x = 0, 1, 2, ... (number of CPUs - 1)

3. Read the scaling available governors entry to get list of supported
   governors.

   ::

      cat /sys/devices/system/cpu/cpufreq/policy_x/scaling_available_governors

4. To change governor, write the preferred governor to scaling governor entry.

   ::

      echo governor_name > /sys/devices/system/cpu/cpufreq/policy<x>/scaling_governor

      Here the governor_name is obtained from step 3.

5. Repeat step 3 to confirm the governor change is taken into effect.


Validating CPPC functionality
-----------------------------

For validating the CPPC functionality, it is recommended to use 'userspace'
governor. The procedure for validation is:

1. Set 'userspace' governor as the scaling governor.

   ::

      echo userspace > /sys/devices/system/cpu/cpufreq/policy<x>/scaling_governor

2. Write the desired frequency in KHz to the scaling setspeed entry.

   ::

      echo freq_in_KHz > /sys/devices/system/cpu/cpufreq/policy<x>/scaling_set_speed

      For RD-V1 variants, the supported frequencies in GHz are 1.3, 1.5, 1.7, 2.1 and 2.6
      For RD-N2 variants, the supported frequencies in GHz are 2.3, 2.6 and 3.2
      For RD-V3 variants, the supported frequencies in GHz are 1.7, 2.0, 2.3, 2.6, 2.9 and 3.2

3. Read the cpuinfo current frequency entry, to obtain the current operating
   frequency of the CPU, using the AMU extension.


Additional precautions for FVP based platforms
----------------------------------------------

The CPPC frequency monitoring part should be executed with highest time
precision. For FVP based platforms, to improve the time precision, follow the
steps below.

1. Export these variables before launching the model

   ::

      export FM_SCX_ENABLE_TIMER_LOCAL_TIME=1
      export FASTSIM_DISABLE_TA=0
      export FASTSIM_AUTO_SYNC=1

2. Pass ``--quantum=1`` as model parameter.
3. For single-chip platforms, pass ``--min-sync-latency=0`` and for multichip
   platforms, pass ``--min-sync-latency=1`` also as model parameter.
