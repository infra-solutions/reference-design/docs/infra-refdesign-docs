****************
Power Management
****************

.. toctree::
   :maxdepth: 1

   lpi-test
   cppc-test
   reboot-test
   smcf
