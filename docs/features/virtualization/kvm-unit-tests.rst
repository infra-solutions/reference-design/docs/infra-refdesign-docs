.. _KVM_unit_test_label:

KVM Unit Tests
==============

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of kvm-unit-tests
--------------------------

KVM unit testing started off alongside of original project `KVM`_. It's purpose
was to validate all the supported features of *KVM*. With evolving development
of KVM, very quickly it became necessary to standardize the process of
validation which motivated the *kvm-unit-tests* project. It's basically a
collection of small standalone programs which are used as tiny guest operating
systems (OS) to test KVM. Since KVM is part of Linux kernel, any userspace
virtual machine manager (VMM) such as, *qemu* or *kvmtool* can be used for
launching these guest OSes which then validate specific feature as implemented
by the Linux KVM. Running of these unit testcases will also help in validation
of the userspace hypervisor tool. The *kvm-unit-tests* framework supports
multiple architectures e.g. i386, x86_64, armv7 (arm), armv8 (arm64), ppc64,
ppc64le, and s390x. To learn more about the framework and testdevs please
follow the official page for `kvm-unit-tests`_.


Build the platform software
---------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

This section describes the procedure to build the sofware stack required to
perform KVM unit testing. Following software packages from the Neoverse
reference platform software stack are needed to do the testing:

  - Software stack for buildroot boot as given in :ref:`Buildroot
    <Buildroot_boot_label>` guide,
  - kvm-unit-tests built for kvmtool target,
  - Kvmtool VMM.

Skip this section if a :ref:`Buildroot <Buildroot_boot_label>` build is already
completed for the platform as the ``kvmtool`` and ``kvm-unit-tests`` are already
built as part of the buildroot build of the platform software stack.

- To build the software stack for buildroot boot, the command to be used is

  ::

   ./build-scripts/rdinfra/build-test-buildroot.sh -p <platform name> <command>

  Supported command line options are listed below

  * <platform name>

    - Lookup a platform name in :ref:`Platform Names <platform_names>`.

  * <command>

    - Supported commands are

      - ``clean``
      - ``build``
      - ``package``
      - ``all`` (all of the three above)

  Examples of the build command are:

  - Command to clean, build and package the software stack needed for the
    buildroot boot on RD-N2 platform:

    ::

     ./build-scripts/rdinfra/build-test-buildroot.sh -p rdn2 all


Booting the platform for validation
-----------------------------------

- Boot the target platform FVP with Buildroot filesystem up to the buildroot
  prompt.

  - Set ``MODEL`` path before launching the model:

    ::

     cd model-scripts/rdinfra
     export MODEL=<absolute path to the platform FVP binary>

  - Launch buildroot boot:

    ::

     ./boot-buildroot.sh -p <platform name> -a <additional_params> -n [true|false]

  - For example, to start the execution of the RD-N2 model to boot up to the
    buildroot prompt with network enabled:

    ::

     ./boot-buildroot.sh -p rdn2 -n true

Running Unit Testcases
^^^^^^^^^^^^^^^^^^^^^^

The kvmtool (lkvm) and kvm-unit-tests binaries are available in the filesystem
on the second partition of the virtio-disk. Mount the vda2 partition on /mnt

  ::

   mount /dev/vda2 /mnt
   ls
     Image                  lost+found             test_smmute.sh
     kvm-ut                 ramdisk-buildroot.img
     kvmtool                smmute

 - Navigate to the kvm-unit-tests directory and launch the tests script to start
   all kvm-unit-tests. The test script uses LKVM environment variable with the
   correct path to lkvm binary.

   ::

    cd kvm-ut
    LKVM=/mnt/kvmtool/lkvm ./run_tests_kvmtool_arm.sh

 - This launches all kvm-unit-tests on the platform. Output would look as below:

   ::

    === selftest-setup ===

    PASS: selftest: setup: smp: number of CPUs matches expectation
    INFO: selftest: setup: smp: found 2 CPUs
    PASS: selftest: setup: mem: memory size matches expectation
    INFO: selftest: setup: mem: found 256 MB
    SUMMARY: 2 tests

    EXIT: STATUS=1

       === selftest-vectors-kernel ===

    PASS: selftest: vectors-kernel: und
    PASS: selftest: vectors-kernel: svc
    PASS: selftest: vectors-kernel: pabt
    SUMMARY: 3 tests

    EXIT: STATUS=1

       === selftest-vectors-user ===

    PASS: selftest: vectors-user: und
    PASS: selftest: vectors-user: svc
    SUMMARY: 2 tests

    EXIT: STATUS=1

       === selftest-smp ===

    INFO: selftest: smp: PSCI version: 1.1
    INFO: selftest: smp: PSCI method: hvc
    INFO: selftest: smp: CPU  1: MPIDR=0080000001
    INFO: selftest: smp: CPU  2: MPIDR=0080000002
    INFO: selftest: smp: CPU  3: MPIDR=0080000003
    INFO: selftest: smp: CPU  4: MPIDR=0080000004
    INFO: selftest: smp: CPU  5: MPIDR=0080000005
    ....
    ....
    ....

This completes the procedure to run kvm-unit-tests for KVM based virtualization
validation on the platform.

.. _KVM: https://www.linux-kvm.org/page/Main_Page
.. _kvm-unit-tests: https://www.linux-kvm.org/page/KVM-unit-tests
