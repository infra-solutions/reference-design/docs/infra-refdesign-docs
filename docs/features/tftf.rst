.. _TFA_boot_tests_label:

TF-A Tests
==========

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview of tf-a-tests
----------------------

The Trusted Firmware-A Tests (TF-A-Tests) is a suite of baremetal tests to
exercise the Trusted Firmware-A (TF-A) features from the Normal World. Neoverse
Reference Design (RD) platform software stack supports booting TF-A-Tests. This
enables strong TF-A functional testing without dependency on a Rich OS. Refer
the `Trusted Firmware-A Tests Documentation`_ for more details.

This document describes how to build the Neoverse RD platform software stack and
and use it to boot TF-A-Tests on the Neoverse RD FVP.


Build the platform software
---------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

This section describes the procedure to build the platform firmware required to
boot TF-A-Tests on Neoverse RD platforms.

To build the software stack, the command to be used is

::

  ./build-scripts/build-test-tf-a-tests.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - Supported commands are

    - ``clean``
    - ``build``
    - ``package``
    - ``all`` (all of the three above)

.. note::
   On networks where git port is blocked, the build procedure might not
   progress. Refer the :ref:`troubleshooting guide <troubleshooting>`
   for possible ways to resolve this issue.

Examples of the build command are

- Command to clean, build and package the RD-N2 software stack required for
  TF-A-Tests boot on RD-N2 platform:

  ::

    ./build-scripts/build-test-tf-a-tests.sh -p rdn2 all

- Command to perform an incremental build of the software components included in
  the software stack for the RD-N2 platform.

  ::

    ./build-scripts/build-test-tf-a-tests.sh -p rdn2 build

.. note::
   This command should be followed by the ``package`` command to complete the
   preparation of the FIP.

- Command to package the previously built software stack and prepare the FIP.

  ::

    ./build-scripts/build-test-tf-a-tests.sh -p rdn2 package


Boot TF-A-Tests
---------------

After the build of the platform software stack for TF-A-Tests is complete, the
following commands can be used to start the execution of the *selected platform
fastmodel* and boot the TF-A-Tests. Examples on how to use the command are
listed below.

To boot TF-A-Tests, the commands to be used are

- Set ``MODEL`` path before launching the model:

  ::

    export MODEL=<absolute path to the platform FVP binary>

- If platform is SGI-575:

  ::

    cd model-scripts/sgi

- If platform is an RD:

  ::

    cd model-scripts/rdinfra

- Launch TF-A-Tests boot:

  ::

    ./tftf.sh -p <platform name> -a <additional_params> -n [true|false]


Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -n [true|false] (optional)

   - Controls the use of network ports by the model. If network ports have
     to be enabled, use 'true' as the option. Default value is set to
     'false'.

*  -a <additional_params> (optional)

   - Specify any additional model parameters to be passed. The model
     parameters and the data to be passed to those parameters can be found
     in the FVP documentation.

Example commands to boot TF-A-Tests are as listed below.

- Command to start the execution of the RD-N2 model to boot TF-A-Tests:

  ::

    ./tftf.sh -p rdn2

- Command to start the execution of the RD-N2 model to boot TF-A-Tests with
  network enabled. The model supports virtio.net allowing the software running
  within the model to access the network:

  ::

    ./tftf.sh -p rdn2 -n true

- Command to start the execution of the RD-N2 model with networking enabled and
  to boot TF-A-Tests. Additional parameters to the model are supplied using the
  ``-a`` command line parameter:

  ::

    ./tftf.sh -p rdn2 -n true -a "-C board.flash0.diagnostics=1"

- Once the tests complete, a message similar to the following output will be
  displayed on the non-secure UART terminal. This demonstrates the usage of
  TF-A Tests on Arm infrastructure reference design platforms.

  ::

    ******************************* Summary *******************************
    > Test suite 'Framework Validation'
                                                                    Passed
    > Test suite 'Timer framework Validation'
                                                                    Passed
    > Test suite 'Boot requirement tests'
                                                                    Passed
    > Test suite 'Query runtime services'
                                                                    Passed
    > Test suite 'PSCI Version'
                                                                    Passed
    > Test suite 'PSCI Affinity Info'
                                                                    Passed
    > Test suite 'CPU Hotplug'
                                                                    Passed
    > Test suite 'PSCI CPU Suspend'
                                                                    Passed
    > Test suite 'PSCI STAT'
                                                                    Passed
    > Test suite 'PSCI NODE_HW_STATE'
                                                                    Passed
    > Test suite 'PSCI Features'
                                                                    Passed
    > Test suite 'PSCI MIGRATE_INFO_TYPE'
                                                                    Passed
    > Test suite 'PSCI mem_protect_check'
                                                                    Passed
    > Test suite 'SDEI'
                                                                    Passed
    > Test suite 'Runtime Instrumentation Validation'
                                                                    Passed
    > Test suite 'TRNG'
                                                                    Passed
    > Test suite 'IRQ support in TSP'
                                                                    Passed
    > Test suite 'TSP handler standard functions result test'
                                                                    Passed
    > Test suite 'Stress test TSP functionality'
                                                                    Passed
    > Test suite 'TSP PSTATE test'
                                                                    Passed
    > Test suite 'EL3 power state parser validation'
                                                                    Passed
    > Test suite 'State switch'
                                                                    Passed
    > Test suite 'CPU extensions'
                                                                    Passed
    > Test suite 'ARM_ARCH_SVC'
                                                                    Passed
    > Test suite 'Performance tests'
                                                                    Passed
    > Test suite 'SMC calling convention'
                                                                    Passed
    > Test suite 'FF-A Setup and Discovery'
                                                                    Passed
    > Test suite 'SP exceptions'
                                                                    Passed
    > Test suite 'FF-A Direct messaging'
                                                                    Passed
    > Test suite 'FF-A Power management'
                                                                    Passed
    > Test suite 'FF-A Memory Sharing'
                                                                    Passed
    > Test suite 'SIMD,SVE Registers context'
                                                                    Passed
    > Test suite 'FF-A Interrupt'
                                                                    Passed
    > Test suite 'SMMUv3 tests'
                                                                    Passed
    > Test suite 'FF-A Notifications'
                                                                    Passed
    > Test suite 'PMU Leakage'
                                                                    Passed
    > Test suite 'DebugFS'
                                                                    Passed
    > Test suite 'Realm payload tests'
                                                                    Passed
    =================================
    Tests Skipped : 122
    Tests Passed  : 60
    Tests Failed  : 0
    Tests Crashed : 0
    Total tests   : 182
    =================================
    NOTICE:  Exiting tests.


.. _Trusted Firmware-A Tests Documentation: https://trustedfirmware-a-tests.readthedocs.io/en/latest/#trusted-firmware-a-tests-documentation
