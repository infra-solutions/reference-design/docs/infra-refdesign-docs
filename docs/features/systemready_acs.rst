.. _systemready_acs:

******************************
SystemReady Compliance Program
******************************

Arm SystemReady is a compliance program that helps ensure the interoperability
of an operating system on Arm-based hardware. Developers can build software
once and deploy it on any compliant Arm-based chip. Arm SystemReady benefits
the entire ecosystem as existing software can move through hardware generations
and hardware providers can reach a broader addressable market.

Arm SystemReady is implemented in alignment with the two OS environment and
firmware approaches commonly used today. These are split into the following
bands:

    * SystemReady Band
    * SystemReady Devicetree Band

Neoverse Reference Designs focus on the SystemReady Band only. For more
information, visit: `Arm SystemReady Compliance Program`_.

SystemReady Band
================

`Arm SystemReady band`_ is part of the `Arm SystemReady Compliance Program`_.
It helps ensure operating system interoperability for advanced configuration
and power interface (ACPI) environments where generic operating systems (OS)
can be installed on either new or old hardware without modification. Old OSs
can run on new hardware, and new OSs can run on old hardware, without
customization.

Arm SystemReady band-compliant hardware helps reduce the cost of supporting
multiple versions of operating systems through seamless interoperability with
standard operating systems, hypervisors, and software.

Arm SystemReady band is relevant for systems using Windows, Linux, VMware, and
BSD environments.

Arm SystemReady band compliant systems must conform to the following:

    * `Base System Architecture (BSA) specification`_
    * `Server Base System Architecture (SBSA) supplement specification`_
    * SBBR recipe of the `Arm Base Boot Requirements (BBR) Specification`_

Arm SystemReady band compliance and testing requirements are specified in the
`Arm SystemReady Requirements Specification (SRS)`_.

System Architecture Compliance Suites (ACS)
===========================================

`SystemReady band`_ is a subset of `System Architecture Compliance Suites`_
which are a series of test suites that check the compliance of a system against
arm architectural specifications.

This chapter describes how to build Neoverse Reference Designs software stack
and execute the test-suites that achieve compliance against the specifications.

It is recommended to use prebuilt systemready-images as shown bellow, but for
reference on the sources and instructions to build from source refer to
`SystemReady band Github`_ repository.

Build the Platform Software
---------------------------

.. important::

    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

.. note::

    * This section assumes the user has completed the chapter
      :doc:`Getting Started </user_guides/getting_started>` and has a
      functional working environment.

    * Example provided here is for RDN2 Platform

    * The target ``all`` executes the individual ``clean``, ``build`` and
      ``package`` in sequence, if calling ``build`` alone due to an incremental
      build, it must be followed by ``package``.

The components of the RD platform software stack that are built are limited to
those that provide the EFI implementation and the EFI shell (i.e, SCP, TF-A and
EDK2).

Command to build the software stack is as follows:

.. code-block:: shell

    ./build-scripts/rdinfra/build-test-acs.sh -p rdn2 all

Prepare Test Image
------------------

The prebuilt systemready-image is generic for the different SystemReady Bands,
so an extra step is required to execute the SBSA test specification in
automation mode.

Prebuilt system-ready images are available to download from Github in
`prebuilt_images`_. After the download is complete, extract the image using:

.. code-block:: shell

    unzip systemready_acs_live_image.img.xz.zip
    xz -d systemready_acs_live_image.img.xz

Inspect the image to check the offset needed to mount the partition:

.. code-block:: shell

    sudo fdisk -l systemready_acs_live_image.img.xz

The output should be similar to:

.. code-block:: shell

    Disk systemready_acs_live_image.img: 642 MiB, 673185792 bytes, 1314816 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: gpt
    Disk identifier: 6F4BF28C-FC58-45A7-9614-B52782B7F71B

    Device                          Start     End Sectors  Size Type
    systemready_acs_live_image.img1  2048 1312766 1310719  640M Microsoft basic data

The partition starts at sector *2048*, so we need to multiply the *sector size*
with the *start sector*, thus ``512*2048=1048576``

Mount the image in the filesystem to edit ``acs_run_config.ini`` file and
change the property ``SbsaRunEnabled`` from ``0`` to ``1``:

.. code-block:: shell

    mkdir sr-image
    sudo mount -t vfat systemready_acs_live_image.img sr-image -o loop,offset=1048576,uid=$(id -u),gid=$(id -g)
    vim sr-image/acs_tests/config/acs_run_config.ini

Execute Test Image
------------------

.. note::

   A new copy of the disk image shall be used every time the tests are to be
   executed, this ensures tests are not skipped due to presence of log files
   from the previous executions.

Supported command line options are listed below:

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -v <systemready_acs_live_image.img>

   -  The absolute path to the systemready_acs_live_image.img has to be supplied as the
      parameter.

*  -n [true|false] (optional)

   -  Controls the use of network ports by the model. If network ports have to
      be enabled, use 'true' as the option. Default value is set to 'false'.

*  -a <additional_params> (optional)

   -  Specify any additional model parameters to be passed. The model parameters
      and the data to be passed to those parameters can be found in the FVP
      documentation.

Execute RD-N2 platform with networking enabled and the systemready-image
located at ``/tmp/systemready_acs_live_image.img``.

.. code-block:: shell

    export MODEL=<absolute path to the platform FVP binary>
    cd model-scripts/rdinfra
    ./acs.sh -p rdn2 -v /tmp/systemready_acs_live_image.img -n true


The SBSA/SBBR tests are split into two phases - tests that execute from linux
and the tests that execute from an EFI interface level.

Let the boot progress to the Grub menu. To execute SystemReady band tests,
choose the option ``SystemReady band ACS (Automation)`` from Grub menu, which
launches the EFI shell.

Press ``Enter`` key or wait until the timeout in the EFI shell to finish.

The systemready-image by default executes SBBR SCT tests. To skip this suite,
wait until ``Press any key to stop the EFI SCT running`` displays in the log
and press any key. If SBBR SCT is not skipped, the SBSA will execute after
completion of SBBR SCT.

On SBSA test completion, the script reboots the platform, follow the steps
mentioned above until skipping SCT.

This time SBSA is not executed as the results are already captured in the
systemready_acs_live_image.img image and the validation OS starts to boot.

The Linux part of the test will be executed on validation OS boot complete.

On completion of SBSA and SBBR tests, the execution stops at the command line
prompt. The execution can be stopped by terminating the FVP.

In case it is not required to run the complete ACS compliance, i.e.: validate
only the SBSA, the systemready_acs_live_image.img has the provision for this.
SBSA test should be run from the EFI shell manually by executing the command
listed below:

.. code-block:: shell

    Shell> EFI\BOOT\bsa\sbsa\Sbsa.efi -skip 800


Running the test manually will not store the test result into
systemready_acs_live_image.img disk image, instead the test results will be
available on the console as the test proceeds to completion.

Retrieve Test Results
---------------------

On completion of SBSA/SBBR tests, test results can be retrieved by mounting
the partition of systemready-image that was used for the test. The offset is
calculated the same as shown above, thus execute:

.. code-block:: shell

    mkdir sr-image
    sudo mount -t vfat systemready_acs_live_image.img sr-image -o loop,offset=1048576,uid=$(id -u),gid=$(id -g)

The test results can be found in the directories below:

  - UEFI SBSA test report: sr-image/acs_results/uefi/
  - Linux SBSA test report: sr-image/acs_results/linux/
  - FWTS result:  sr-image/acs_results/fwts/

Unmount the image after analysing the logs using the following command:

.. code-block:: shell

    sudo umount sr-image


Select a SBSA Compliance Level (Optional)
-----------------------------------------

SBSA specification classifies hardware into different levels, level-3 through
level-7. The systemready-image defaults to level 4. To select a different level
press ``ESC`` from UEFI shell and run the SBSA efi binary manually to select
the appropriate compliance level to be tested. An example command to select the
compliance level is:

.. code-block:: shell

    Shell> EFI\BOOT\bsa\sbsa\Sbsa.efi -l <y>

    # Where, y can be 3 to 7 for the SBSA compliance level.

.. _Arm SystemReady Compliance Program: https://www.arm.com/architecture/system-architectures/systemready-compliance-program
.. _Arm SystemReady Band: https://www.arm.com/architecture/system-architectures/systemready-compliance-program/systemready-band
.. _Base System Architecture (BSA) specification: https://developer.arm.com/documentation/den0094/latest
.. _Server Base System Architecture (SBSA) supplement specification: https://developer.arm.com/documentation/den0029/latest
.. _Arm Base Boot Requirements (BBR) Specification: https://developer.arm.com/documentation/den0044/latest
.. _Arm SystemReady Requirements Specification (SRS): https://developer.arm.com/documentation/den0109/latest
.. _SystemReady band: https://developer.arm.com/Architectures/Arm%20SystemReady%20Band
.. _System Architecture Compliance Suites: https://developer.arm.com/Architectures/Architectural%20Compliance%20Suite
.. _SystemReady band Github: https://github.com/ARM-software/arm-systemready/tree/main/SystemReady-band
.. _prebuilt_images: https://github.com/ARM-software/arm-systemready/tree/main/SystemReady-band/prebuilt_images
