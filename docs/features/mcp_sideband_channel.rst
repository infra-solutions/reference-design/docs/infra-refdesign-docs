MCP sideband channel
====================

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

Overview
--------

Server systems generally hosts a myriad of IPs/controllers within a single
system. For Neoverse Reference Design platforms, this could range from SCP
(system control processor), AP (application processor), MCP (manageability
control processor) etc. In production environments, an additional board
management controller would also be added to the system which would help system
administrators to monitor the live status of the system remotely. In such
environments, communication between these controllers and especially between the
BMC and other components is of utmost importance. The system administrator can
rely on the status of the system from the BMC only if such a reliable
communication enables these controllers to talk to the BMC. MCP sideband channel
feature aims at show-casing one such means of communication using ``PLDM<->MCTP``
protocol stack.

SBMR specification recommends certain guidelines in using these stacks to
implement the MCP sideband channel. Care has been taken to align to the
specification in areas where it was feasible to do so. However, please note that
Neoverse Reference Design platforms doesn't support a BMC and therefore all the
communication as of now is implemented via a loopback on MCP itself. MCP has
been chosen as the controller for showcasing the feature as one of its core
responsibilities is to to communicate and share information with the BMC. If the
system supports sensors and effecters with little or no-intelligence to it, the
MCP can read and write data from them and transfer them over to the BMC.


What does MCP sideband channel showcase?
----------------------------------------

MCP sideband channel show-cases packet transactions over a ``PLDM<->MCTP``
stack based system implemented on MCP. Packets are sent and received on MCP over
a loopback interface which mimics the physical layer.

Firmware on MCP has been segregated to implement a MCP terminal and a BMC
terminal. The feature show-cases BMC as the primary terminal trying to discover
information about the secondary terminal, MCP. ``PLDM discovery``, as quoted in
the PLDM specification has been implemented in firmware. BMC terminal uses
``PLDM discovery`` to send out request packets to figure out the terminal ID,
PLDM types, PLDM commands and the version for these commands. MCP also holds a
dummy PDR record. A PDR record could be thought of as a block of semantic
information required to understand how sensor/effecter data on a particular
terminal could be parsed at a node remote to the one that forms it. In our
example, if we assume the MCP terminal to be connected to a sensor, it is
essential for the BMC terminal to understand how to read/parse the sensor data.
MCP terminal is required to form PDR records and transfer it to the BMC terminal
on request to aid in this scenario. The dummy PDR held by the MCP terminal is
retrieved as part of ``PLDM discovery``.

For more information on the design of firmware, refer to
`MCP sideband channel design`_


Building and running MCP sideband channel
-----------------------------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Follow the instructions as given in :ref:`Busybox Boot <Busybox_boot_label>` to
boot linux busybox on the platform.

The feature is setup in such a way that the BMC firmware automatically starts
off with ``PLDM discovery`` to identify and retrieve information from MCP
terminal. On MCP controller's uart terminal, you should be able to see the logs
starting with the following prints.

  ::

     [    0.000000] [MCP]: mcp context:
     [    0.000000] [PLDM_FW]:
     [    0.000000] [PLDM_FW]: pldm tid:                       0
     [    0.000000] [PLDM_FW]: pldm type count:                2
     [    0.000000] [PLDM_FW]: global enable:                  0
     [    0.000000] [PLDM_FW]: receiver addr:                  0
     [    0.000000] [PLDM_FW]: heartbeat timer:                0
     [    0.000000] [PLDM_FW]: transport protocol type:        0


Decoding output logs
--------------------

The output logs can be decoded as follows.

The first part of the logs point to the PLDM terminal information for MCP
terminal within the segregated firmware. This represents the PLDM TID and
different sets of PLDM features supported on the MCP terminal. At this point,
the BMC terminal is yet to discover this information.

  ::

     [    0.000000] [MCP]: mcp context:
     [    0.000000] [PLDM_FW]:
     [    0.000000] [PLDM_FW]: pldm tid:                       0
     [    0.000000] [PLDM_FW]: pldm type count:                2
     [    0.000000] [PLDM_FW]: global enable:                  0
     [    0.000000] [PLDM_FW]: receiver addr:                  0
     [    0.000000] [PLDM_FW]: heartbeat timer:                0
     [    0.000000] [PLDM_FW]: transport protocol type:        0
     [    0.000000] [PLDM_FW]: pldm type:                      0
     [    0.000000] [PLDM_FW]: version count:                  1
     [    0.000000] [PLDM_FW]: version[0] :           f1.f0.f0.0
     [    0.000000] [PLDM_FW]: commands count:                 4
     [    0.000000] [PLDM_FW]: command[0]:                     2
     [    0.000000] [PLDM_FW]: command[1]:                     3
     [    0.000000] [PLDM_FW]: command[2]:                     4
     [    0.000000] [PLDM_FW]: command[3]:                     5
     [    0.000000] [PLDM_FW]: pldm type:                      2
     [    0.000000] [PLDM_FW]: version count:                  1
     [    0.000000] [PLDM_FW]: version[0] :           f1.f2.f0.0
     [    0.000000] [PLDM_FW]: commands count:                 3
     [    0.000000] [PLDM_FW]: command[0]:                    49
     [    0.000000] [PLDM_FW]: command[1]:                    57
     [    0.000182] [PLDM_FW]: command[2]:                    81

This is followed by the BMC terminal initiating the actual ``PLDM discovery``.

  ::

    [    0.000282] [BMC]: pldm discovery start ...

What follows is a set of ``PLDM<->MCTP`` based requests and responses to transfer
MCP terminal's PLDM terminal information. Each command transaction involves 2
cycles of ``PLDM<->MCTP`` stack walk. This is better explained in the
`MCP sideband channel design`_ section. For brevity, a small snippet of the
transaction has been pasted below.

  -   ``A request being sent``

  ::

     [    0.000482] [MCTP]: sending pkt, len 8
     [    0.000607] [LOOPBACK]: sending packet onto loopback bus
     [    0.000982] [LOOPBACK]: receiving packet from loopback bus


  -  ``Corresponding response  being sent``

  ::

     [    0.001082] [MCTP]: sending pkt, len 9
     [    0.001214] [LOOPBACK]: sending packet onto loopback bus
     [    0.001388] [LOOPBACK]: receiving packet from loopback bus

  -  ``Similar cycle for other PLDM commands``

  ::

     [    0.001482] [MCTP]: sending pkt, len 12
     [    0.001682] [LOOPBACK]: sending packet onto loopback bus
     [    0.001822] [LOOPBACK]: receiving packet from loopback bus
     [    0.001982] [MCTP]: sending pkt, len 7
     [    0.002082] [LOOPBACK]: sending packet onto loopback bus
     [    0.002182] [LOOPBACK]: receiving packet from loopback bus

     [    0.002382] [MCTP]: sending pkt, len 17
     [    0.002482] [LOOPBACK]: sending packet onto loopback bus
     [    0.002582] [LOOPBACK]: receiving packet from loopback bus
     [    0.002782] [MCTP]: sending pkt, len 44
     [    0.002882] [LOOPBACK]: sending packet onto loopback bus
     [    0.003037] [LOOPBACK]: receiving packet from loopback bus


Once all transactions are done, the discovery completes gracefully.

  ::

     [    0.007282] [PLDM_FW]: pldm discovery complete

Finally, BMC terminal prints all the data it received from MCP terminal. This
has to match with the prints put out by MCP terminal before the transactions
started.

  ::

     [    0.007482] [PLDM_FW]:
     [    0.007549] [PLDM_FW]: pldm tid:                       0
     [    0.007682] [PLDM_FW]: pldm type count:                2
     [    0.007782] [PLDM_FW]: global enable:                  2
     [    0.007982] [PLDM_FW]: receiver addr:                  8
     [    0.008082] [PLDM_FW]: heartbeat timer:                0
     [    0.008282] [PLDM_FW]: transport protocol type:        0
     [    0.008382] [PLDM_FW]: pldm type:                      0
     [    0.008503] [PLDM_FW]: version count:                  1
     [    0.008682] [PLDM_FW]: version[0] :           f1.f0.f0.0
     [    0.008850] [PLDM_FW]: commands count:                 4
     [    0.008982] [PLDM_FW]: command[0]:                     2
     [    0.009082] [PLDM_FW]: command[1]:                     3
     [    0.009284] [PLDM_FW]: command[2]:                     4
     [    0.009382] [PLDM_FW]: command[3]:                     5
     [    0.009482] [PLDM_FW]: pldm type:                      2
     [    0.009718] [PLDM_FW]: version count:                  1
     [    0.009805] [PLDM_FW]: version[0] :           f1.f2.f0.0
     [    0.009982] [PLDM_FW]: commands count:                 3
     [    0.010152] [PLDM_FW]: command[0]:                    49
     [    0.010282] [PLDM_FW]: command[1]:                    57
     [    0.010412] [PLDM_FW]: command[2]:                    81

The fields ``global enable``, ``receiver addr``, ``heartbeat timer`` and
``transport protocol type`` could hold different values on BMC terminal when
compared to MCP terminal. ``receiver addr`` corresponds to the address of BMC
terminal and rest of fields corresponds to configurations that enable events
that BMC terminal is interested in. This data is send to the MCP terminal
from the BMC terminal along the discovery process to let the MCP terminal
know what all events it is interested in receiving notification from and the
address to which those events needs to be forwarded. At the time when MCP
context is printed, these fields are not yet set.

In addition to the PLDM information that BMC terminal has received, a PDR record
has also been received (rather retrieved) by the BMC terminal. This is the last
set of data to appear in the logs. The PDR record is printed as raw bytes here.

  ::

     [    0.010582] [PLDM_FW]: pdr [0]:
     [    0.010682] [PLDM_FW]:                                1
     [    0.010782] [PLDM_FW]:                                0
     [    0.010882] [PLDM_FW]:                                0
     [    0.011082] [PLDM_FW]:                                0
     [    0.011193] [PLDM_FW]:                                2
     [    0.011382] [PLDM_FW]:                                3
     [    0.011540] [PLDM_FW]:                                4
     [    0.011682] [PLDM_FW]:                                0
     [    0.011800] [PLDM_FW]:                                5
     [    0.011982] [PLDM_FW]:                                0
     [    0.012082] [PLDM_FW]:                                6
     [    0.012182] [PLDM_FW]:                                0
     [    0.012408] [PLDM_FW]:                                7
     [    0.012494] [PLDM_FW]:                                0
     [    0.012682] [PLDM_FW]:                                8
     [    0.012842] [PLDM_FW]:                                0
     [    0.012982] [PLDM_FW]:                                9
     [    0.013082] [PLDM_FW]:                                0
     [    0.013282] [PLDM_FW]:                               10
     [    0.013382] [PLDM_FW]:                                0
     [    0.013482] [PLDM_FW]:                               11
     [    0.013709] [PLDM_FW]:                               12
     [    0.013782] [PLDM_FW]:                               13
     [    0.013982] [PLDM_FW]:                               14


MCP sideband channel design
---------------------------

``PLDM<->MCTP`` transactions are in a way analogous to TCP/IP transaction for
any application protocol. Take the example of an FTP server running over TCP/IP.
FTP, the application layer deals with transferring chunks of file data as
packets. Further, we have TCP as the transport layer underneath which deals
with fragmentation, re-ordering, acknowledgment of receipt etc to make sure
the transport went through well. Similarly PLDM acts as the application layer.
PLDM specification dictates what data to transferred in each packet. MCTP is
the transport layer. Like TCP, it deals with fragmentation and re-ordering.

Following PLDM commands have been used in the in the feature.

+---------------------+-------------------+----------------+
| PLDM Command        | PLDM Type         | Code Value     |
+=====================+===================+================+
| GetTID              | PLDM BASE         | 0x02           |
+---------------------+-------------------+----------------+
| GetPLDMVersion      |  PLDM BASE        | 0x03           |
+---------------------+-------------------+----------------+
| GetPLDMTypes        |  PLDM BASE        | 0x04           |
+---------------------+-------------------+----------------+
| GetPLDMCommands     |  PLDM BASE        | 0x05           |
+---------------------+-------------------+----------------+
| SetEventReceiver    |  PLDM PLATFORM    | 0x04           |
+---------------------+-------------------+----------------+
| GetPDR              |  PLDM PLATFORM    | 0x51           |
+---------------------+-------------------+----------------+

PLDM specification defines the request and response formats for each of these
commands. To better understand the transactions, GetTID could be taken as an
example. BMC terminal forms the GetTID PLDM packet and transfers it to MCTP
layer. MCTP forwards the command to the loopback interface which sends the
packet to itself. Loopback receiver then forwards the packet to MCTP which
forwards it to the MCP terminal. This could be thought as the first cycle or
the request cycle.

MCP terminal decodes the packet, forms the response and sends it back to MCTP.
The packet essentially traverses one more cycle until it finally reaches BMC
terminal. This could be thought of as the second cycle or the response cycle.
For multi-part transactions, the number of cycles to complete one command
transfer may not be limited to two cycles.

MCP sideband channel software makes use of the following specifications.

     - `PLDM Base specification`_
     - `PLDM Platform specification`_
     - `PLDM Codes`_
     - `PLDM over MCTP Binding`_
     - `MCTP specification`_

Following thrid party libraries also have been used.

    - `libpldm`_
    - `libmctp`_


.. _PLDM Base specification: https://www.dmtf.org/sites/default/files/standards/documents/DSP0240_1.0.0.pdf
.. _PLDM Platform specification: https://www.dmtf.org/sites/default/files/standards/documents/DSP0248_1.2.0.pdf
.. _PLDM Codes: https://www.dmtf.org/sites/default/files/standards/documents/DSP0245_1.2.0.pdf
.. _PLDM over MCTP Binding: https://www.dmtf.org/sites/default/files/standards/documents/DSP0241_1.0.0.pdf
.. _MCTP specification: https://www.dmtf.org/sites/default/files/standards/documents/DSP0236_1.3.0.pdf
.. _libpldm: https://github.com/openbmc/pldm/tree/master/libpldm
.. _libmctp: https://github.com/openbmc/libmctp
