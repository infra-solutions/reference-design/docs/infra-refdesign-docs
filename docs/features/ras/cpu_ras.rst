CPU Error Injection
^^^^^^^^^^^^^^^^^^^

The Neoverse RD-N2 platforms has support for 2 error nodes,
and the presence of these nodes enable the RAS extension.

* Node 0: Includes the L3 memory system in the DSU.
* Node 1: Includes the private L1 and L2 memory systems in the cpu.

RD-V3 only supports one error node.

* Node 0: Includes the private L1 and L2 memory systems in the cpu.

CPU support SED parity (Single Error Detect) and SECDED ECC
(Single Error Correct Double Error Detect) capabilities.

Rd-V3-Cfg1 and RdN2 platforms also supports injecting error's
to verify error handling software.

.. note::

   The Neoverse RD-V3 reference design platforms are based on direct connect
   configuration and has no DSU. Hence they only support one error node
   i.e Node0.

Error Injection Software Sequence
"""""""""""""""""""""""""""""""""

CPU implements Pseudo Fault Generation registers. With the help of these
registers, software can inject either CE, DE or UE into the cache RAMs.

Detailed error injection software sequence:

*  Select error record for L1 and L2 memory systems i.e. Node0

    - write_errselr_el1 (0)

*  Program the Error Control Register to enable Error Detection, FHI for CE,
   DE and UE.

    - write_erxctlr_el1 (0x109) (Note: To enable ERI on UE write 0x10D)

*  Program the PFG Control Register to 0.

    - write_cpu_pfg_ctrl_register (0)

*  Clear the Error Status Register to 0.

    - write_erxstatus_el1 (0xFFC00000)

*  Set PFG countdown register to 1.

    - write_cpu_pfg_cdn_register (1)

*  For Deferred Error injection write

    - write_cpu_pfg_ctrl_register (0x80000020)   [Generates FHI interrupt]


Procedure to Perform Error Injection
""""""""""""""""""""""""""""""""""""

.. note::

   This section assumes the user has completed the
   :doc:`Getting Started </user_guides/getting_started>` chapter and has a
   functional working environment.


Error Handling Mode Selection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CPU supports both *Firmware First* and *Kernel First* error
handling modes, and the default mode is set to *Firmware First*.

.. important::

   Only one error handling mode can be enabled at a time.

The error handling modes are a build time option, in order to select either
the user needs to navigate to the ``<workspace>`` and edit the configuration
file of the platform of interest and look for ``TF_A_RAS_FW_FIRST`` flag.

As an example for RD-V3 Cfg1 platform:

.. code-block:: shell

   vim <workspace>/build-scripts/configs/rdv3cfg1/rdv3cfg1

* Firmware First Selection:

.. code-block:: shell

   TF_A_RAS_FW_FIRST = 1

* Kernel First Selection

.. code-block:: shell

   TF_A_RAS_FW_FIRST = 0

.. note::

   Clean and build once you switch error handling mode.

Build and Boot Operating System(s)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Refer to any of the bellow list of supported operating systems, to build the
reference design platform software stack and boot into the OS.


* :ref:`Busybox Boot <Busybox_boot_label>`
* :ref:`Buildroot Boot <Buildroot_boot_label>`

Inject Error
~~~~~~~~~~~~

After the boot is complete, based on the error handling scheme selected
use EINJ table debugfs entries to inject the error.

* :ref:`Firmware First Error Injection. <cpu_ff_einj>`
* :ref:`Kernel First Error Injection. <cpu_kf_einj>`

The field ``sel-firmware-first`` in oem-einj is used to toggle firmware first
error injection, with the default being kernel first error injection. Field
``sel-error-type`` is used to choose the type of error injection, where the
current implementation only support's deferred errors.


Firmware First Error Injection
++++++++++++++++++++++++++++++

.. _cpu_ff_einj:

.. code-block:: shell

   mount -t debugfs none /sys/kernel/debug # Step needed for Buildroot only
   echo 0x80020000 > /sys/kernel/debug/apei/einj/error_type
   echo 1 > /sys/kernel/debug/apei/einj/oem-einj/sel-firmware-first
   echo 2 > /sys/kernel/debug/apei/einj/oem-einj/sel-component
   echo 2 > /sys/kernel/debug/apei/einj/oem-einj/sel-error-type
   echo 1 > /sys/kernel/debug/apei/einj/error_inject


On successful error injection the firmware reception log's this error
information on the console.

Check the secure uart terminal (window with the name ``FVP terminal_sec_uart``)
for a log similar to below.

.. code-block:: shell

 SP 8001: ErrAddr = 0x8F840
 SP 8001: MmEntryPoint Done
 INFO:    EINJ event received 83
 INFO:    cpu_id 2
 INFO:    Injecting DE...
 INFO:    ErrStatus = 0x0
 INFO:    [CPU RAS] CPU intr received = 17 on cpu_id = 2
 INFO:    [CPU RAS] ERXMISC0_EL1 = 0x0
 INFO:    [CPU RAS] ERXSTATUS_EL1 = 0x40800000
 INFO:    [CPU RAS] ERXADDR_EL1 = 0x0 buff_base = 0xf4600000


Check the non-secure uart terminal (window with the name ``FVP terminal_nsec_
uart``) for a log similar to below.

.. code-block:: shell

 {2}[Hardware Error]: Hardware error from APEI Generic Hardware Error Source: 10
 {2}[Hardware Error]: event severity: recoverable
 {2}[Hardware Error]:  Error 0, type: recoverable
 {2}[Hardware Error]:   section_type: ARM processor error
 {2}[Hardware Error]:   MIDR: 0x00000000410fd840
 {2}[Hardware Error]:   Multiprocessor Affinity Register (MPIDR): 0x0000000081020000
 {2}[Hardware Error]:   running state: 0x1
 {2}[Hardware Error]:   Power State Coordination Interface state: 0
 {2}[Hardware Error]:   Error info structure 0:
 {2}[Hardware Error]:   num errors: 1
 {2}[Hardware Error]:    first error captured
 {2}[Hardware Error]:    error_type: 0, cache error
 {2}[Hardware Error]:    error_info: 0x000000000002001f
 {2}[Hardware Error]:     transaction type: Generic
 {2}[Hardware Error]:     operation type: Generic error (type cannot be determined)
 {2}[Hardware Error]:     cache level: 0
 {2}[Hardware Error]:     processor context not corrupted
 {2}[Hardware Error]:     the error has not been corrected
 {2}[Hardware Error]:    physical fault address: 0x0000000000000000
 {2}[Hardware Error]:   Context info structure 0:
 {2}[Hardware Error]:    register context type: AArch64 general purpose registers
 {2}[Hardware Error]:    00000000: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000010: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000020: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000030: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000040: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000050: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000060: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000070: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000080: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    00000090: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000a0: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000b0: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000c0: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000d0: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000e0: 00000000 00000000 00000000 00000000
 {2}[Hardware Error]:    000000f0: 00000000 00000000 00000000 00000000

Kernel First Error Injection
++++++++++++++++++++++++++++

.. _cpu_kf_einj:

.. code-block:: shell

   mount -t debugfs none /sys/kernel/debug # Step needed for Buildroot only
   echo 0x80020000 > /sys/kernel/debug/apei/einj/error_type
   echo 0 > /sys/kernel/debug/apei/einj/oem-einj/sel-firmware-first
   echo 2 > /sys/kernel/debug/apei/einj/oem-einj/sel-component
   echo 2 > /sys/kernel/debug/apei/einj/oem-einj/sel-error-type
   echo 1 > /sys/kernel/debug/apei/einj/error_inject


On successful error injection the kernel receives a error event which is
received in the irq handler. The handler traverses through the error record
info and logs the error.

Check the non-secure uart terminal (window with the name ``FVP terminal_nsec_
uart``) for a log similar to below.

.. code-block:: shell

   [ 2365.760926] Injecting DE-
   [ 2365.760928] ARM RAS: error from CPU7
   [ 2365.760930] ERR0STATUS: 0x40800000

EDAC ( Error Detection and Correction)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The EDAC(Eror Detection and Correction) Linux interface provides a framework,
for reporting memory and CPU errors encountered on a system. It allow the
Kernel to detect and manage errors, providing valuable information for
diagnostics and troubleshooting hardware issue.

We currently only enabled EDAC support for CPU for both RD-N2 and RD-V3
Platforms. Error count is exposed through sysfs inteface this interface
allows user to access information about Corrected (CE) and Uncorrected (UE)
errors that have occurred in the system aiding in monitoring and diagnosing
hardware issues.

.. note::

   This feature is only supported on RD-V3-Cfg1 and RD-N2-Cfg1 Platforms
   if Kernel first error Handling is enabled.

.. code-block:: shell

   cat /sys/devices/system/edac/cpu/cpu*/ue_count
