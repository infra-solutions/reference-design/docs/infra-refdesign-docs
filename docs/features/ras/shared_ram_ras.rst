Shared RAM Error Injection
^^^^^^^^^^^^^^^^^^^^^^^^^^

RD-V3 and RD-N2 platform have support for Shared RAM that is shared between AP,
MCP, SCP and RSS. The shared RAM is protected with SECDED (Single Error Correct
Double Error Detect). RD-V3 platform defines ECC RAS registers to log any
ECC errors that occur during Shared RAM access from each master AP, SCP, MCP or
RSS. For RD-V3 4 sets of ECC RAS registers defined for each master to log
errors based on master's PAS and 2 sets of ECC Ras registers for RD-N2 platform.

**RD-V3:**
The list for Shared RAM ECC RAS registers is defined below:

 * AP Secure RAM ECC RAS registers
 * AP Non-Secure RAM ECC RAS registers
 * AP Realm RAM ECC RAS registers
 * AP Root RAM ECC RAS registers
 * SCP Secure RAM ECC RAS registers
 * SCP Non-Secure RAM ECC RAS registers
 * SCP Realm RAM ECC RAS registers
 * SCP Root RAM ECC RAS registers
 * MCP Secure RAM ECC RAS registers
 * MCP Non-Secure RAM ECC RAS registers
 * MCP Realm RAM ECC RAS registers
 * MCP Root RAM ECC RAS registers

**RD-N2:**
The list for Shared RAM ECC RAS registers is defined below:

 * AP Secure RAM ECC RAS registers
 * AP Non-Secure RAM ECC RAS registers
 * SCP Secure RAM ECC RAS registers
 * SCP Non-Secure RAM ECC RAS registers
 * MCP Secure RAM ECC RAS registers
 * MCP Non-Secure RAM ECC RAS registers

.. note::

   This test is only supported on RD-V3-Cfg1 and RD-N2-Cfg1 Platforms.
   :ref:`ff_error_handling`


Error Injection on Shared RAM
"""""""""""""""""""""""""""""

Each ECC RAS register set implements SRAMECC_ERRMISC1 register which provides a
way to inject Corrected Error (CE) or Uncorrected Error (UE) in the Shared RAM.
The error injection only takes effect if the register programming is followed by
a read access to shared RAM. If the injection is successful the error records
pertaining to the master and respective access are populated with error
information and an error interrupt is delivered to the master.

RD-V3 Shared SRAM
~~~~~~~~~~~~~~~~~

Detailed Error injection software sequence is illustrated to inject 1-bit CE
into Root Shared RAM from AP executing in RD-V3.

*  Add memory map for the Shared RAM ECC RAS registers memory space.
*  Add memory map for the Shared memory space.
*  Program the SRAMECC_ERRCTRL register to enable ED(Error detection),
   FI(Fault Interrupt) and CFI(Corrected Fault Interrupt)
*  Program the SRAMECC_ERRMISC1 register to enable INJECT_CE.
*  Read from memory mapped shared memory space to inject the error.

RD-N2 Shared SRAM
~~~~~~~~~~~~~~~~~

Detailed Error injection software sequence is illustrated to inject 1-bit CE
into Non-Secure Shared RAM from AP executing in RD-N2.

*  Add memory map for the Shared RAM ECC RAS registers memory space.
*  Add memory map for the Shared memory space.
*  Program the SRAMECC_ERRCTRL register to enable RAM_ECC_EN and
   set INJECT_ERROR to [01] for Correctable error.
*  Read from memory mapped shared memory space to inject the error.

Procedure to Perform Error Injection on Shared RAM
""""""""""""""""""""""""""""""""""""""""""""""""""

.. note::

   This section assumes the user has completed the
   :doc:`Getting Started </user_guides/getting_started>` chapter and has a
   functional working environment.

Error Handling Mode Selection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Both platform only supports *Firmware First* SRAM error handling mode,
and the default mode is set to *Firmware First*.

.. important::

   Only Firmware first mode is supported for SRAM-Errors.

The error handling modes are a build time option, in order to select either
the user needs to navigate to the ``<workspace>`` and edit the configuration
file of the platform of interest and look for ``TF_A_RAS_FW_FIRST`` flag.

As an example for RD-V3 Cfg1 platform:

.. code-block:: shell

   vim <workspace>/build-scripts/configs/rdv3cfg1/rdv3cfg1

* Firmware First Selection:

.. code-block:: shell

   TF_A_RAS_FW_FIRST = 1

.. note::

   Clean and build once you switch error handling mode.


Build and Boot Operating System(s)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Refer to any of the bellow list of supported operating systems, to build the
reference design platform software stack and boot into the OS.

* :ref:`Busybox Boot <Busybox_boot_label>`
* :ref:`Buuidroot Boot <Buildroot_boot_label>`

Inject Error on Shared RAM
~~~~~~~~~~~~~~~~~~~~~~~~~~

Run below command to inject 1-bit CE to the Shared RAM. This test uses EINJ ACPI
table to perform error injection. Shared RAM is not a standard defined
error_type in EINJ ACPI table so use the vendor defined error type. Bit 31 of
error_type field represents vendor error type. Use error_type value 0x8002_0000
to represent Shared RAM errors.

.. code-block:: shell

   mount -t debugfs none /sys/kernel/debug # Step needed for Buildroot only
   echo 0x80020000 > /sys/kernel/debug/apei/einj/error_type
   echo 1 > /sys/kernel/debug/apei/einj/oem-einj/sel-firmware-first
   echo 1 > /sys/kernel/debug/apei/einj/oem-einj/sel-component
   echo 1 > /sys/kernel/debug/apei/einj/oem-einj/sel-error-type
   echo 1 > /sys/kernel/debug/apei/einj/error_inject

Shared RAM error handling happens in Firmware first mode. The EL3 firmware
receives the fault handling interrupt (FHI) for the corrected error detected
and logs the error on the secure console.

.. code-block:: shell

 EDAC MC0: 1 CE unknown error on unknown memory
 ( page:0x8f offset:0x840 grain:-281474976710655 syndrome:0x0 - APEI location: )
 {1}[Hardware Error]: Hardware error from APEI Generic Hardware Error Source: 20
 {1}[Hardware Error]: It has been corrected by h/w and requires no further action
 {1}[Hardware Error]: event severity: corrected
 {1}[Hardware Error]:  Error 0, type: corrected
 {1}[Hardware Error]:   section_type: memory error
 {1}[Hardware Error]:   physical_address: 0x000000000008f840
 {1}[Hardware Error]:   physical_address_mask: 0x0000ffffffffffff
