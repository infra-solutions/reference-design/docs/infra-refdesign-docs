The error injection utility is referred to as einj-util in this document.
Einj-util is a command-line utility designed for SCP. This utility integrates
with the SCP CLI Debugger, enabling users to insert commands at runtime.
Einj-util facilitates error injection into various RAS-supported components
when a user provides error injection command input in the CLI. This utility
helps in validating the RAS capable hardware components' behavior when error
is detected and reported.

The term "Component" defines the RAS-supported components for which error
injection is supported. "Sub-component" signifies the next level of error
categorization for each component, and it varies for different
components. For instance, in the context of SRAM, sub-components represent
error injection in different worlds: Root, Secure, Realm, and Non-Secure.
"Type" defines the various types of errors supported by each component. Error
types supported are Correctable Error(CE), Deferred Error(DE), Uncorrectable
Error(UE).


Procedure to Perform Error Injection into Various Components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   This section assumes the user has completed the
   :doc:`Getting Started </user_guides/getting_started>` chapter and has a
   functional working environment.


Build Software Stack
""""""""""""""""""""

This procedure doesn't require a full host OS to be present, but the
:ref:`Busybox Boot <Busybox_boot_label>` is still recommended as it is the
simplest method to build the required components.


Boot up to SCP CLI Debugger Shell
"""""""""""""""""""""""""""""""""

Once the build step is completed, boot the Busybox stack on FVP as normal but
identify the window with the name ``FVP terminal_uart_scp`` once it shows up,
as this window is the one to interact with. The steps are as follows:

* Launch the FVP and access the SCP UART.
* Once in the SCP UART terminal, use ``Ctrl + e`` to enter the CLI.
* To access the help menu for the einj-util utility, run the command

.. code-block:: shell

   einj-util -h

* The "help" command displays the CLI usage.

.. code-block:: text

   > einj-util -h
       Inject error into various components.

       Usage: einj-util -comp <n> -subcomp <n> -type <n>

       -comp: sram (0), tcm (1), cpu (2), rsm (3)

       -subcomp:

               sram: root (0), secure (1), non-secure (2), realm (3)

               tcm: itcm (0), dtcm (1)

               cpu: always 0 for now

               rsm: secure (0), non-secure (1)

       -type:

               sram/tcm/rsm: correctable (0), uncorrectable (1)

               cpu: correctable (0), uncorrectable (1), deferred (2)

       example:

               1) ce into shared sram from secure world:
                       einj-util -comp 0 -subcomp 1 -type 0
               2) ce into scp itcm:
                       einj-util -comp 1 -subcomp 0 -type 0
               3) cpu ue:
                       einj-util -comp 2 -subcomp 0 -type 1

* To exit the CLI Debugger, press ``Ctrl + d``.

Various Error Injection Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-------------+------------------+---------------+--------------+
| Component   | Subcomponent     | Type of Error | Error Status |
+=============+==================+===============+==============+
| Shared SRAM | Secure World     | CE            | 0x86000000   |
+             +------------------+---------------+--------------+
|             | Root World       | UE            | 0xa4000000   |
+-------------+------------------+---------------+--------------+
| RSM SRAM    | Secure World     | CE            | 0x86000000   |
+             +------------------+---------------+--------------+
|             | Non-Secure World | UE            | 0xa4000000   |
+-------------+------------------+---------------+--------------+
| TCM         | ITCM             | CE            | 0x5          |
+             +------------------+---------------+--------------+
|             | DTCM             | UE            | 0x7          |
+-------------+------------------+---------------+--------------+
| CPU         | Core             | CE            | 0xC6000000   |
+             +                  +---------------+--------------+
|             |                  | UE            | 0x60000000   |
+             +                  +---------------+--------------+
|             |                  | DE            | 0x40800000   |
+-------------+------------------+---------------+--------------+

Shared SRAM Error Injection
"""""""""""""""""""""""""""

Run the following command to inject a correctable error into shared SRAM
from the secure world.

.. code-block:: text

   > einj-util -comp 0 -subcomp 1 -type 0

After triggering the error, the interrupt handler is invoked, logging error
records.

.. code-block:: text

   [SRAM_INT] ErrStatus = 0x86000000
   [SRAM_INT] fwk_int number = 24
   [SRAM_INT] ErrAddr   = 0x10

SRAM ECC Error Status Register Bit Descriptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text

   AV[31:31]  :  Address Valid

   MV[26:26]  :  Miscellaneous Registers Valid

   CE[25:24]  :  Correctable error has occurred

   DE[23:23]  :  Deferred Error

   UET[21:20] :  Uncorrected Error Type

   SERR[7:0]  :  Primary Error code


CPU Error Injection
"""""""""""""""""""

Run the following command to inject a CPU correctable error.

.. code-block:: text

      > einj-util -comp 2 -subcomp 0 -type 0

The ErrorStatus register captures information about the triggered CPU error.

.. code-block:: text

   Injecting CPU CE
   ErrStatus  0xC6000000
   ErrAddress 0x0

Core Error Injection ERXSTATUS_EL1 Register Description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text

   AV[31:31]  :  Address Valid

   V[30:30]   :  Status Register Valid

   MV[26:26]  :  Miscellaneous Registers Valid

   CE[25:24]  :  Corrected Error

   DE[24:24]  :  Deferred Error

   UET[21:20] :  Uncorrected Error Type

   SERR[4:0]  :  Primary Error code


SCP ITCM/DTCM Error Injection
"""""""""""""""""""""""""""""

Invoke the following command to inject a correctable error into SCP ITCM.

.. code-block:: text

   > einj-util -comp 1 -subcomp 0 -type 0

The error record information will be logged in the following manner.

.. code-block:: text

   ITCM
   Injecting CE
   [TCM_INT] fwk_int number = 21
   [TCM_INT] ErrCode   = 0x9
   [TCM_INT] ErrStatus = 0x5
   [TCM_INT] ErrAddr   = 0x34d8

TCMECC_ERRSTATUS Bit Descriptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text

   OF[2:2] : Multiple errors occurred before SW cleared the current error

   UE[1:1] : Uncorrectable and uncontainable error have occurred

   CE[0:0] : Correctable error has occurred


RSM SRAM Error Injection
""""""""""""""""""""""""

Invoke the following command to trigger a correctable error in RSM SRAM from
the secure world.

.. code-block:: text

   > einj-util -comp 3 -subcomp 0 -type 0

The error record information is logged as follows:

.. code-block:: text

   Injecting CE into RSM SRAM
   [RSM_INT] ErrStatus = 0x86000000
   [RSM_INT] fwk_int number = 29
   [RSM_INT] ErrAddr   = 0x10

.. note::

   Refer to the SRAM ECC Error Status register bit descriptions to decode
   the error status for RSM SRAM errors.


Expected Output for the Various Scenarios
"""""""""""""""""""""""""""""""""""""""""

============================= ======================================= =====================================
Description                   Command                                 Expected Output
============================= ======================================= =====================================
Shared SRAM Secure World CE   *einj-util -comp 0 -subcomp 1 -type 0*  .. code-block:: bash

                                                                        Injecting CE into Shared SRAM
                                                                        [SRAM_INT] ErrStatus = 0x86000000
                                                                        [SRAM_INT] fwk_int number = 24
                                                                        [SRAM_INT] ErrAddr = 0x10

Shared SRAM Secure World UE   *einj-util -comp 0 -subcomp 1 -type 1*  .. code-block:: bash

                                                                        Injecting UE into Shared SRAM
                                                                        [SRAM_INT] ErrStatus = 0xa4000000
                                                                        [SRAM_INT] fwk_int number = 24
                                                                        [SRAM_INT] ErrAddr = 0x1

Shared SRAM Root CE           *einj-util -comp 0 -subcomp 0 -type 0*  .. code-block:: bash

                                                                        Injecting CE into Shared SRAM
                                                                        [SRAM_INT] ErrStatus = 0x86000000
                                                                        [SRAM_INT] fwk_int number = 26
                                                                        [SRAM_INT] ErrAddr = 0x10

Shared SRAM Root UE           *einj-util -comp 0 -subcomp 0 -type 1*  .. code-block:: bash

                                                                        Injecting UE into Shared SRAM
                                                                        [SRAM_INT] ErrStatus = 0xa4000000
                                                                        [SRAM_INT] fwk_int number = 26
                                                                        [SRAM_INT] ErrAddr = 0x10

RSM SRAM Secure World CE      *einj-util -comp 3 -subcomp 0 -type 0*  .. code-block:: bash

                                                                        Injecting CE into RSM SRAM
                                                                        [RSM_INT] ErrStatus = 0x86000000
                                                                        [RSM_INT] fwk_int number = 29
                                                                        [RSM_INT] ErrAddr = 0x10

RSM SRAM Secure World UE      *einj-util -comp 3 -subcomp 0 -type 1*  .. code-block:: bash

                                                                        Injecting UE into RSM SRAM
                                                                        [RSM_INT] ErrStatus = 0xa4000000
                                                                        [RSM_INT] fwk_int number = 29
                                                                        [RSM_INT] ErrAddr = 0x10

RSM SRAM Non-secure World CE  *einj-util -comp 3 -subcomp 1 -type 0*  .. code-block:: bash

                                                                        Injecting CE into RSM SRAM
                                                                        [RSM_INT] ErrStatus = 0x86000000
                                                                        [RSM_INT] fwk_int number = 29
                                                                        [RSM_INT] ErrAddr = 0x10

RSM SRAM Non-secure World UE  *einj-util -comp 3 -subcomp 1 -type 1*  .. code-block:: bash

                                                                        Injecting UE into RSM SRAM
                                                                        [RSM_INT] ErrStatus = 0xa4000000
                                                                        [RSM_INT] fwk_int number = 29
                                                                        [RSM_INT] ErrAddr = 0x10

TCM ITCM CE                   *einj-util -comp 1 -subcomp 0 -type 0*  .. code-block:: bash

                                                                        ITCM
                                                                        Injecting CE
                                                                        [TCM_INT] ErrStatus = 0x5
                                                                        [TCM_INT] fwk_int number = 21
                                                                        [TCM_INT] ErrCode = 0x9
                                                                        [TCM_INT] ErrAddr = 0x6b38

TCM ITCM UE                   *einj-util -comp 1 -subcomp 0 -type 1*  .. code-block:: bash

                                                                        ITCM
                                                                        Injecting UE
                                                                        [TCM_INT] ErrStatus = 0x7
                                                                        [TCM_INT] fwk_int number = 21
                                                                        [TCM_INT] ErrCode = 0x9
                                                                        [TCM_INT] ErrAddr = 0x6a46

TCM DTCM CE                   *einj-util -comp 1 -subcomp 1 -type 0*  .. code-block:: bash

                                                                        DTCM
                                                                        Injecting CE
                                                                        [TCM_INT] ErrStatus = 0x7
                                                                        [TCM_INT] fwk_int number = 21
                                                                        [TCM_INT] ErrCode = 0xb
                                                                        [TCM_INT] ErrAddr = 0x6b3c

TCM DTCM UE                   *einj-util -comp 1 -subcomp 1 -type 1*  .. code-block:: bash

                                                                        DTCM
                                                                        Injecting UE
                                                                        [TCM_INT] ErrStatus = 0x7
                                                                        [TCM_INT] fwk_int number = 21
                                                                        [TCM_INT] ErrCode = 0xb
                                                                        [TCM_INT] ErrAddr = 0x6a46

CPU Core CE                   *einj-util -comp 2 -subcomp 0 -type 0*  .. code-block:: bash

                                                                        Injecting CPU CE
                                                                        ErrStatus  0xC6000000
                                                                        ErrAddress 0x0

CPU Core UE                   *einj-util -comp 2 -subcomp 0 -type 1*  .. code-block:: bash

                                                                        Injecting CPU UE
                                                                        ErrStatus  0x60000000
                                                                        ErrAddress 0x0

CPU Core DE                   *einj-util -comp 2 -subcomp 0 -type 2*  .. code-block:: bash

                                                                        Injecting CPU DE
                                                                        ErrStatus  0x40800000
                                                                        ErrAddress 0x0
============================= ======================================= =====================================
