.. _ras:

***************************************************
Reliability, Availability, and Serviceability (RAS)
***************************************************

Overview
========

Reliability, Availability and Serviceability (RAS) is a measure that defines
the robustness of the system. A RAS enabled platform ensures that the system
produces correct outputs, is always operational and is easily maintainable.
RAS reduces the systems downtime by detecting the hardware errors and correcting
them when possible. The level of RAS to be achieved is implementation dependent.
There are various techniques that help achieve RAS targets e.g Fault prevention
and fault removal, error handling and recovery and fault handling. A well
designed RAS system ensures that the software and hardware collectively work
to minimize the impact of hardware faults on entire system operation and hence
boost performance.

`RAS specification`_ divides the entire RAS architectural extension support
into two categories:

* ARMv8-A RAS Extension
* RAS System Architecture

RAS architectural specification defines the hardware RAS extensions that the
cpu and the system could implement to achieve the desired level of RAS support.

ARMv8-A RAS Extension defines the RAS extensions that are mandatory for CPU
implementation that are based on ARMv8.2 and above. To enable RAS extension
architectural support in software the RAS_EXTENSION flag must be set to 1.

RAS system architecture defines the architectural support required to enable
system level RAS support on a platform. It defines a reusable component
architecture that can detect, record errors and also signal them to Processing
Element (PE). PE is implementation defined, it can be anything that is capable
of handling the given error e.g AP, SCP or MCP. This architectural definitions
makes designing the software easier.

Component Definitions by RAS System Architecture
================================================

Below are some component definitions that the RAS System architecture defines:

Node
----

A node is one such component architecture defined by RAS. A system can have
single or multiple error nodes. Architecturally a node:

* Implements one or more standard error record.
* Records detected and consumed errors.
* Might include control to disable the error reporting and recording while the
  software initializes.
* Reports recorded errors with asynchronous error reporting mechanism like
  interrupts e.g Fault Handling Interrupt (FHI).
* Implements a counter for counting corrected errors.
* Logs timestamps in each error record.
* Report uncorrected error by in-band error reporting signaling (external abort)
* Report critical error condition via Critical Error Interrupt (CRI).

Error Record
------------

RAS system architecture defines standard error record. A node captures entire
error information as part of these error records. Spec defines a mechanism to
access error records as system register or memory mapped registers. A standard
error record comprises of:

* ERR<n>STATUS: characterizes the error and marks valid status fields.
* ERR<n>ADDR: error address register.
* ERR<n>MISC<m>: miscellaneous error register. To be used for:

  - Identifying the Field Replaceable Unit (FRU).
  - Locating the error within the FRU.
  - Implementing corrected error counter to count the corrected errors.
  - Storing the timestamp value for recorded errors.

An Error record records following component error states:

* Corrected Error (CE).
* Deferred Error (DE).
* Uncorrected Error (UE): UE has following sub-types:

  - Uncontainable error (UC).
  - Unrecoverable error (UEU).
  - Recoverable error or Signaled error (UER).
  - Restartable error or Latent error (UEO).


Error Handling
==============

There are two approaches to achieve error handling in software:

* :ref:`Firmware First Error Handling. <ff_error_handling>`
* :ref:`Kernel First Error Handling. <kf_error_handling>`

.. _ff_error_handling:

Firmware First Error Handling
-----------------------------

Firmware First error handling requires the error events that occur are handled
in EL3 and then relayed to OSPM for logging. On error firmware consumes the
error information generates a standard Common Platform Error Record (CPER)
information buffer which is defined by `UEFI specification`_ to store error
information. CPER is placed in firmware reserved memory that is later shared
with the OSPM when it is notified about the error.

On Arm Neoverse Reference design platforms the Firmware First error handling is
achieved using Hardware Error Source Table (HEST) and Software Delegated
Exception Interface (SDEI) tables. The Secure Partition (Standalone MM driver)
is used to generate CPER info for the error. At boot the HEST table is
published and OSPM is made aware about the hardware error source(s) the
platform supports.

During the runtime when hardware fault is detected the corresponding error or
fault handling interrupt is generated. This interrupt is taken to EL3 runtime
firmware which calls into Secure Partition that generates CPER record and
places it in firmware reserved memory. EL3 runtime firmware using SDEI
notifies the OSPM about the error.

.. _kf_error_handling:

Kernel First Error Handling
---------------------------

Kernel First errors are handled directly by the OSPM without firmware
intervention. The fault and error events that are generated by the platform
are taken directly to OSPM.

Arm Neoverse Reference design platforms use Arm Error Source Table (AEST) to
achieve kernel first error handling. AEST table is defined in `ACPI
specification for RAS`_ specification. AEST table defines the hardware error
sources that are present on the platform. AEST table comprises of one or more
error nodes.  A AEST node entry has information of component the node belongs
to e.g Processor, Memory, SMMU, GIC etc. It defines interface type for
accessing the node e.g memory mapped or system register. A node also defines
the list of interrupts the node supports.

OSPM implements a AEST driver module to traverse through the AEST table. The
module registers Irq handlers for all supported node interrupts. The fault event
occurring on that node or error source is directly forwarded to OSPM for
handling.


Error Injection
===============

Error injection feature is a micro-architecture feature defined by RAS to
inject errors in the RAS supported system components. Software can use these
registers to inject the error and test the error handling software implemented
by the platform.

Arm Neoverse Reference Designs use the Error Injection (EINJ) ACPI table
defined in the `ACPI specification`_ to implement error injection feature.
EINJ is action and instruction based table that defines set of actions and
their corresponding instructions. Each action is also assigned a firmware
reserved memory space to store action specific data. An instruction is
essentially a read or a write operation that is performed on that reserved
memory.

On Arm Neoverse Reference Platforms the firmware at EL3 implements the
functionality to program the error injection registers. OSPM initiates the
injection and generates an SPI interrupt to call in to firmware. EINJ
defines a action to program the GICD register that triggers a SPI interrupt
that is handled in EL3.

Firmware-first and Kernel-first software use the EINJ ACPI table to validate
the software functionality.

.. note::

   Error injection, whether firmware-first or kernel-first, are both
   initiated from the kernel.


Error Injection via Kernel
--------------------------

.. _ras_tests:

.. include:: ./cpu_ras.rst
.. include:: ./shared_ram_ras.rst


Error Injection via SCP Utility
-------------------------------

.. _scp_einj_util:

.. include:: ./scp_einj_util.rst

.. include:: ./rasdaemon.rst


Other components supporting RAS
-------------------------------

.. _cmn_cyprus_kfh:

.. include:: ./cmn_ras.rst


.. _RAS specification: https://developer.arm.com/documentation/ddi0487/latest/
.. _ACPI specification: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_3_A_Oct_6_2020.pdf
.. _UEFI specification: https://uefi.org/sites/default/files/resources/UEFI_specification_2_9_2021_03_18.pdf
.. _ACPI specification for RAS: https://developer.arm.com/documentation/den0085/latest/
