CMN Cyprus Kernel First Handling (KFH)
======================================

.. important::
    This feature might not be applicable to all Platforms. Please check
    section **Supported Features** of individual platform pages to confirm if
    this feature is listed as supported. Also this feature can be validated only
    on a pre-silicon validation platform. Current support is limited to RASv1.

CMN Cyprus RAS support
----------------------

CMN Cyprus implements RAS as a distributed architecture with set of logging,
reporting registers and a central interrupt handling unit. The logging and
reporting registers are implemented in the XP, HN-I, HN-F/S, SBSX and CCG device
nodes.

Logging registers implemented in the device node are:

* Error Feature register (ErrFr)

* Error Control register (ErrCtlr)

* Error Status register (ErrStatus)

* Error Address register (ErrAddr)

* Error Misc register 0 (ErrMisc0)

* Error Misc register 1 (ErrMisc1)

Two sets of these registers are implemented by each device node, one to log
error that occur when in root address space and other to log the error when
executing in non-secure address space. Each device node also implements ErrGsr
(Error group status register) that is set when that node is has non-zero
ErrStatus register. CMN Cyprus supports following error types:

* Corrected Error (CE)

* Deferred Error (DE)

* Uncorrected Error Unrecoverable (UEU)

Example: In RD-V3-Cfg1 platform implements a CMN mesh of size 3*3. That has
9 XP's, 8 HNS, 1 SBSX, 4 HN-I and 5 CCG device nodes. Each of these nodes
implement a set of error records to log the detected RAS errors.

Each device node also implements the Pseudo Fault Generation (PFG) registers
that allows to inject the pseudo errors within the device node and validate the
software error handling flow. The PFG registers defined for each node are:

* Error Pseudo Fault Generation Feature register (ErrPfgf)

* Error Pseudo Fault Generation Control register (ErrPfgctl)

* Error Pseudo Fault Generation Count Down register (ErrPfgcdn)

There are 2 sets of PFG registers implemented per device node. One for root
world error injection and other for NS world error injection.

Error/Fault injection in CMN Cyprus
-----------------------------------

Sequence to be followed to perform SW induced error injection:

* Program the Error Control register to enable error detection and enable the
  FHI interrupt

.. code-block:: text

   mmio_write_errctlr ((CMN_BASE + NODE_OFF + ErrCtlr), (BIT3 | BIT0))

* Program the PFG count down register to 1, to inject error on first clock
  tick.

.. code-block:: text

   mmio_write_pfgcdn ((CMN_BASE + NODE_OFF + ErrPfgCdn), 1)

* Program the PFG control register with following fields:

  - Type of error, if CE set BIT6, if DE set BIT5, if UEU set BIT2

  - Set BIT11 to update ErrStatus.AV field on fault injection

  - Set BIT12 to update ErrStatus.MV field on fault injection

  - Set BIT31 to enable the injection by reading the PFG count down register

.. code-block:: text

   mmio_write_pfgCtlr ((CMN_BASE + NODE_OFF + PfgCtlr), (BIT<Error_Type> |
   BIT11 | BIT12 | BIT31))

Run this same sequence in order to inject the error in any of the CMN device
node. NODE_OFF for each node must be known before performing the injection,
which can be determined from the CMN discovery process.

CMN KFH Software
----------------

To enable CMN KFH following SW components are required.

* Arm Error Source Table (AEST) ACPI table to represent CMN errors

* SSDT table

* AEST device driver for CMN.

SSDT Table
^^^^^^^^^^

Add one entry in the SSDT table to define the CMN cyprus device memory
CRS object. Refer `ACPI for Arm Components`_ spec for more information on
various field details.

.. code-block:: text

   // CMN 800 device
    Device (CMN8) { // CMN-800 device object for an X * Y
      Name (_HID, "ARMHC800")
      Name (_UID, Zero)
      Name (_CRS, ResourceTemplate () {
        // Descriptor for 1 GB of the CFG region at offset PERIPHBASE
        QWordMemory (
          ResourceConsumer,
          PosDecode,
          MinFixed,
          MaxFixed,
          NonCacheable,
          ReadWrite,
          0x00000000,       // Granularity
          0x100000000,      // Min, set to PERIPHBASE
          0x13FFFFFFF,      // Max
          0x000000000,      // Translation
          0x040000000,      // Range Length 1GB
          ,                 // ResourceSourceIndex
          ,                 // ResourceSource
          CFGM              // DescriptorName
        )
      })
    } // Device(CMN8)

AEST table
^^^^^^^^^^

Each RAS capable device node is represented as AEST node within the AEST table.
E.g below is the AEST node entry for HNF0, where 0 represent the logical ID of
the HNF. For more information refer `ACPI for the RAS`_ and `ACPI for Arm
Components`_ specs. These specs describes all the necessary fields to be
populated to define a AEST node for a given CMN device node.

.. code-block:: text

   {
     .NodeResource = {
       .Vendor = {
         {
           EFI_ACPI_AEST_NODE_TYPE_VENDOR_DEFINED,    /* Type */
           sizeof (EFI_ACPI_AEST_NODE_DATA),          /* Length */
           0,                                         /* Reserved */
           sizeof (EFI_ACPI_AEST_NODE_STRUCT),        /* Offset to Node data */
           sizeof (EFI_ACPI_AEST_NODE_RESOURCE),      /* Offset to Node Interface */
           (sizeof (EFI_ACPI_AEST_NODE_RESOURCE) +    /* Offset to Node Interrupt */
            sizeof (EFI_ACPI_AEST_INTERFACE_STRUCT)),
           1,                                         /* Interrupt array size */
           0,                                         /* Timestamp */
           0,                                         /* Reserved1 */
           0,                                         /* Injection countdown rate */
         },
         // Vendor Node Structure
         AEST_NODE_TYPE_VENDOR_HID,                   /* Hardware ID */
         1,                                           /* Unique ID */
         // Vendor Data
         {
           0x00,                                      /* Offset HNF0 0x1700000 */
           0x00,
           0x70,
           0x01,
           0,
           0,
           0,
           0,
           0x00,                                      /* Offset HND 0x0000 */
           0x00,
           0,
           0,
         },
       },
     },
     {
       EFI_ACPI_AEST_INTERFACE_TYPE_MMIO,       /* Interface type */
       {0, 0, 0},                               /* Reserved */
       0,                                       /* Flags */
       0,                                       /* Base Address */
       0,                                       /* Record Index */
       0,                                       /* Num Error records */
       0,                                       /* Record implemented */
       0,                                       /* Group status reporting */
       0,                                       /* Addressing mode */
       0,                                       /* ACPI ARM error node device */
       0,                                       /* Processor Affinity */
       0,                                       /* ErrGsr base address */
     },
     {
      {
         EFI_ACPI_AEST_INTERRUPT_TYPE_FAULT_HANDLING,     /* Interrupt type */
         {0, 0},                                          /* Reserved */
         EFI_ACPI_AEST_INTERRUPT_FLAG_TRIGGER_TYPE_LEVEL, /* Flags */
         79,                                              /* GSIV */
         0,                                               /* ID */
         {0, 0, 0},                                       /* Reserved */
       },
     },
   },

Note that HNF0 error node does not define anything in the interface structure.
CMN relies completely on the Vendor-defined nodedata structure to communicate
the device node offset and respective HND node offset.

AEST CMN driver for CMN
^^^^^^^^^^^^^^^^^^^^^^^

The AEST driver for CMN is implemented as an extension to the AEST ACPI table
driver. The AEST CMN driver at boot reads the SSDT table and reads the CRS
object to determine the CMN base address and size and creates virtual mapping
the CMN address space.

Each CMN device error node data is represented using the vendor-defined
structure in the AEST ACPI table. At boot the AEST ACPI driver parses the AEST
table and when it locates a vendor node, it adds the node data to a platform
device structure and registers a platform device. AEST ACPI driver registers a
platform device driver to process the vendor defined errors. For each AEST node
of type vendor error that is detected by the AEST ACPI driver it registers a
platform device and calls into the probe function. For each platform device
registered if the vendor HID is set to CMN HID, it is registered with the AEST
CMN driver.

The AEST CMN driver reads the vendor platform device information into a driver
specific data structure. The AEST CMN driver maintains the device structure in
the linked list. Each list entry holds the information for all the error nodes
of same device type. Driver also registers the IRQ handlers to process the FHI
interrupt generated when a device node detects CE, DE or UE. On an error event
the IRQ handler parses through all the device node structures and reads the
ErrGsr register for each node. For a non-zero ErrGsr located the handler logs
the error records, clears the interrupt and returns. Below is a example log for
DE detected on HNS0 and HNI1

.. code-block:: text

   [    2.117375] AEST_CMN: RAS v2 enabled = 0
   [    2.118373] AEST_CMN: Error record registers for device node HNS0
   [    2.119858] AEST_CMN: [HNS0] ErrFr_NS = 0x5200008012c9a2
   [    2.121154] AEST_CMN: [HNS0] ErrCtlr_NS = 0x10d
   [    2.122263] AEST_CMN: [HNS0] ErrStatus_NS = 0xc4800000
   [    2.123512] AEST_CMN: [HNS0] ErrAddr_NS = 0x0
   [    2.124573] AEST_CMN: [HNS0] ErrMisc0_NS = 0x0
   [    2.125656] AEST_CMN: [HNS0] ErrMisc1_NS = 0x0
   [    2.140341] AEST_CMN: RAS v2 enabled = 0
   [    2.141305] AEST_CMN: Error record registers for device node HNI1
   [    2.142784] AEST_CMN: [HNI1] ErrFr_NS = 0x120000801201a2
   [    2.144077] AEST_CMN: [HNI1] ErrCtlr_NS = 0x10d
   [    2.145181] AEST_CMN: [HNI1] ErrStatus_NS = 0xc4800000
   [    2.146430] AEST_CMN: [HNI1] ErrAddr_NS = 0x0
   [    2.147491] AEST_CMN: [HNI1] ErrMisc0_NS = 0x0
   [    2.148570] AEST_CMN: [HNI1] ErrMisc1_NS = 0x0


.. _ACPI for the RAS: https://developer.arm.com/documentation/den0085/latest/
.. _ACPI for Arm Components: https://developer.arm.com/documentation/den0093/latest/
