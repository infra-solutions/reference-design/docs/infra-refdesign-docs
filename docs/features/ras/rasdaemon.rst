Rasdaemon
=========


Overview
--------

Rasdaemon is error logging tool that is used to log RAS (Reliability,
Availability and Serviceability) events. The daemon uses the kernel trace
sub-system to capture the error events reported by the kernel modules. The trace
events that are captured in /sys/kernel/debug/tracing are reported by the
rasdaemon.

Enabling rasdaemon creates a "instances/rasdaemon" directory inside
"/sys/kernel/debug/tracing" debugfs directory. All the tracing events that are
enabled by the rasdaemon are captured in this directory.

.. note::

   This test is only supported on RD-V3-Cfg1 and RD-N2-Cfg1 Platforms.
   :ref:`ff_error_handling`


Enabling Rasdaemon
------------------

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

RD-N2-Cfg1 and RD-V3-Cfg1 platform have rasdaemon package enabled by
default on the buildroot file system. Buildroot repository has support added to
enable rasdaemon, any platform performing a buildroot boot can enable rasdaemon
package.

To enable rasdaemon on other platform variants add following code to the
buildroot defconfig file.

.. code-block:: text

   BR2_PACKAGE_RASDAEMON=y
   BR2_GLOBAL_PATCH_DIR="board/aarch64-efi/rdinfra/patches/"

To add rasdaemon support on RD-V3 platform add above two lines to file
configs/rdv3/buildroot/aarch64_rdinfra_defconfig

Build the software stack for buildroot. Refer :ref:`Build the platform
software<Buildroot_boot_label>`.

Perform buildroot filesystem boot. Refer :ref:`Booting with Buildroot as the
filesystem<Buildroot_boot_label>`.

On the buildroot shell type following command to enable rasdaemon

.. code-block:: shell

   mount -t debugfs none /sys/kernel/debug
   rasdaemon -e

This command starts rasdaemon and enables trace events for memory controller,
aer, non_standard error records, arm event and arm ras external events.

.. code-block:: shell

   rasdaemon: ras:mc_event event enabled
   rasdaemon: ras:aer_event event enabled
   rasdaemon: ras:non_standard_event event enabled
   rasdaemon: ras:arm_ras_ext_event event enabled
   rasdaemon: ras:arm_event event enabled

Test to validate rasdaemon
--------------------------

To validate the logging of RAS events by rasdaemon requires a platform with RAS
support enabled. Here we look at the 1-bit DE reported by the CPU on
RD-V3-Cfg1 platform that has RAS support enabled. Perform the test for
firmware first error handling for 1-bit DE on CPU. The kernel logs this event
and also reports an arm_event for this error to the tracing subsystem. Rasdaemon
captures this arm_event trace log and prints it.

Refer :ref:`CPU Error Injecton <ras_tests>` to perform CPU firmware first
error handling test on RD-V3-Cfg1 platform. On the error injection the
kernel logs the error and also the arm_event. The trace event is also recorded
as part of rasdaemon buffer. To log the trace from rasdaemon run following
command.

.. code-block:: shell

   cat /sys/kernel/debug/tracing/instances/rasdaemon/trace_pipe

The above command outputs following log from rasdaemon.

.. code-block:: shell

   <idle>-0       [004] d.h1.   555.977157: arm_event: affinity level: 255;
   MPIDR: 0000000081040000; MIDR: 00000000410fd840; running state: 1; PSCI state: 0
