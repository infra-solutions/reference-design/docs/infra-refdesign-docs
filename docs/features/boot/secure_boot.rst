.. _UEFI_secureboot_label:

UEFI Secure Boot
================

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Secure boot is a mechanism to build and maintain a complete chain of trust on
all the software layers executed in a system and preventing malicious code to be
stored and loaded in place of the authenticated one. When the device starts, the
firmware checks the signature of each piece of boot software, including UEFI
firmware drivers, EFI applications, and the operating system. If the signatures
are valid, the device boots, and the firmware gives control to the operating
system. Fundamental to the success of the secure boot is the ability to securely
store (also referred to as secure storage) and access the keys used for
authentication during the various stages of boot.

Secure boot and Secure storage mechanisms are defined by the UEFI
specifications. In short, the UEFI specifications define the use of two
asymmetric key pairs, platform key (PK) and Key Exchange Key (KEK), and
databases for valid and invalid signatures. These keys and databases are
used during the secure boot phase which implies that the platform should provide
a tamper proof mechanism to store these keys.

The RD platform software allows validation of the secure boot process. This
document explains the procedure to build the platform software stack and
validate UEFI secure boot on the RD platforms.

Though secure boot process have to be validated using a linux distribution as
the target OS, the RD platform software stack currently limits this feature
validation to boot of a signed busybox OS.


Generate key pairs
------------------

The one-time generation of the following key pairs is mandatory - PK, KEK, DB
and DBX. The following commands can be used to generate these key pairs.

- Key Pair Creation : PK, KEK, DB and DBX

  ::

        cd rd-workspace
        cd tools/efitools
        openssl req -new -x509 -newkey rsa:2048 -subj "/CN=PK/" -keyout PK.key -out PK.crt -days 3650 -nodes -sha256
        openssl req -new -x509 -newkey rsa:2048 -subj "/CN=KEK/" -keyout KEK.key -out KEK.crt -days 3650 -nodes -sha256
        openssl req -new -x509 -newkey rsa:2048 -subj "/CN=DB_Key/" -keyout DB.key -out DB.crt -days 3650 -nodes -sha256
        openssl req -new -x509 -newkey rsa:2048 -subj "/CN=DBX_Key/" -keyout DBX.key -out DBX.crt -days 3650 -nodes -sha256

- Convert crt certificate to der format

  ::

        openssl x509 -in PK.crt -outform der -out PK.der
        openssl x509 -in KEK.crt -outform der -out KEK.der
        openssl x509 -in DB.crt -outform der -out DB.der
        openssl x509 -in DBX.crt -outform der -out DBX.der

The signing of the grub and linux images are performed as a part of build script
“build-test-secureboot.sh”. There is no explicit user action required to sign
these images.


Build the platform software
---------------------------

The procedure to build the platform software stack for secure boot test is
listed below.

To build the software stack, the command to be used is

::

  ./build-scripts/rdinfra/build-test-secureboot.sh -p <plaform name> <command>

Supported command line options are listed below

* <platform name>

  - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - Supported commands are

    - ``clean``
    - ``build``
    - ``package``
    - ``all`` (all of the three above)


Examples of the build command are

- Command to clean, build and package the software stack needed for the secure
  boot test for RD-N2 platform.

  ::

     ./build-scripts/rdinfra/build-test-secureboot.sh -p rdn2 all

- Command to perform an incremental build of the software components included in
  the software stack for the RD-N2 platform.

  ::

    ./build-scripts/rdinfra/build-test-secureboot.sh -p rdn2 build

.. note::
   This command should be followed by the ``package`` command to complete the
   preparation of the fip and the disk image.

- Command to package the previously built software stack and prepare the fip and
  the disk image.

  ::

    ./build-scripts/rdinfra/build-test-secureboot.sh -p rdn2 package


Securely boot upto Busybox
--------------------------

After the build of the platform software stack for UEFI secure boot is complete,
the following command starts the execution of the *selected platform fastmodel*
and the software boots up to the busybox prompt. Examples on how to use the
command are listed below.

.. note::
   The steps to enroll signatures required to successfully secure boot the
   platform is listed as well. It is important to execute those steps at least
   once to validate secure boot support.

To boot up to the busybox prompt, the commands to be used are

- Set ``MODEL`` path before launching the model:

  ::

    export MODEL=<absolute path to the platform FVP binary>

- If platform is SGI-575:

  ::

    cd model-scripts/sgi

- If platform is an RD:

  ::

    cd model-scripts/rdinfra

- Launch busybox boot:

  ::

    ./secure_boot.sh -p <platform name> -a <additional_params> -n [true|false]


Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -n [true|false] (optional)

   - Controls the use of network ports by the model. If network ports have
     to be enabled, use 'true' as the option. Default value is set to
     'false'.

*  -a <additional_params> (optional)

   - Specify any additional model parameters to be passed. The model
     parameters and the data to be passed to those parameters can be found
     in the FVP documentation.


Example commands to validate the secure boot functionality are as listed below.

- Command to start the execution of the RD-N2 model to boot up to the
  Busybox prompt with secure boot enabled:

  ::

    ./secure_boot.sh -p rdn2

- Command to start the execution of the RD-N2 model to boot up to the
  Busybox prompt with secure boot and network enabled. The model supports
  virtio.net allowing the software running within the model to access the
  network:

  ::

    ./secure_boot.sh -p rdn2 -n true

- Command to start the execution of the RD-N2 model with networking
  enabled and to boot up to the Busybox prompt with secure boot enabled.
  Additional parameters to the model are supplied using the -a command
  line parameter:

  ::

    ./secure_boot.sh -p rdn2 -n true -a "-C board.flash0.diagnostics=1"

To setup the secure boot process follow the steps listed below on the first
boot. Subsequent boots will not need these.
Several terminal windows will pop-up in the screen, and the one to interact
with has the window title: ``FVP terminal_ns_uart_ap``.

1. Interrupt the boot at EDK2 by pressing escape key and dropping into the EDK2
   boot menu.
2. Select Device Manager → Secure Boot Configuration → Secure Boot Mode →
   choose Custom mode and then press enter.
3. Select "Custom Secure Boot Options” and then press enter.
4. Select “DBX Options” → "Enroll Signature" then press enter →
   "Enroll Signature Using File" and then press enter → Select “NO VOLUME LABEL”
   and then press enter.
5. Select EFI and press enter → select BOOT and press enter → now Select
   “DBX.der” and press enter → “Commit Changes and Exit”.
6. Repeat steps “4” and “5” for “DB options” for “DB.der”.
7. Repeat steps “4” and “5” for “KEK options” for “KEK.der”.
8. Repeat steps “4” and “5” for “PK options” for “PK.der”.
9. Press Escape and press F10 to save. Ensure that the “Current Secure Boot
   State” is set as “Enabled”.
10. Press Escape and select the “continue” option.
11. Prompts the user to press the “Enter”. Press enter key which then reboots
    the system.

The platform boots up to busybox login prompt with secure boot enabled. If the
authentication of the grub or the linux kernel fails, the boot fails and the
user is notified about the authentication failure.

To confirm that the boot is indeed a secure boot, the EFI firmware will display
messages in the boot log (same window where the secure boot was setup) as shown
bellow.

   ::

     Loading driver at 0x000F50A0000 EntryPoint=0x000F676A188
     Loading driver at 0x000F50A0000 EntryPoint=0x000F676A188
     EFI stub: Booting Linux Kernel...
     EFI stub: EFI_RNG_PROTOCOL unavailable, KASLR will be disabled
     EFI stub: UEFI Secure Boot is enabled.
     EFI stub: Using DTB from configuration table
     EFI stub: Exiting boot services and installing virtual address map...
     [    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd490]

This completes the validation of the secure boot functionality.
