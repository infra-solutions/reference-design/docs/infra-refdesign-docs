.. _distro_boot:

Distro Boot (and Install)
=========================

.. important::

    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

.. note::

    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Neoverse Reference Design (RD) platform software stack supports boot of various
linux distributions such as Debian, Ubuntu or Fedora.

This can be achieved either by using a pre-installed (raw) image of the
distribution or performing an installation from an iso image.

The prefered method is to use the pre-installed images as greatly reduces the
time needed to validate that the software stack can boot into Linux.

An installation from an iso image can take several hours, and will vary greatly
with the hardware used, but even on modern hardware can be around 8 hours.

Regardless of the approach selected, the common step is to build the software
stack as mentioned below in `Build the platform software`_.


Build the platform software
---------------------------

This section describes the procedure to build the platform firmware required to
boot or install a linux distribution on Neoverse RD platforms.

To build the RD software stack, the command to be used is

.. code-block:: shell

  ./build-scripts/build-test-uefi.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - ``clean``
  - ``build``
  - ``package``
  - ``all`` (all of the three above)


Using RD-N2 as an example:

- Command to clean, build and package the software stack:

.. code-block:: shell

    ./build-scripts/build-test-uefi.sh -p rdn2 all

- Command to remove the generated outputs (binaries):

.. code-block:: shell

    ./build-scripts/build-test-uefi.sh -p rdn2 clean

.. important::

   If using incremental builds, use target command ``build`` followed by
   ``package``, so the output binaries are correctly generated.

- Command to perform an incremental build of the software stack:

.. code-block:: shell

    ./build-scripts/build-test-uefi.sh -p rdn2 build
    ./build-scripts/build-test-uefi.sh -p rdn2 package


Boot a Linux Distribution
-------------------------

Pre-Installed (Raw) images
^^^^^^^^^^^^^^^^^^^^^^^^^^

Debian Distribution
"""""""""""""""""""

The cloud images for Debian can be obtained from the `Debian cloud images`_
page. A number of images listed by codename, along with the daily builds of the
latest release version are available on this page.

.. important::

    - It is recommended to use the nocloud variant of the Debian
      cloud image as it provisions the user to login as root without a password.

    - Select an image for the ``aarch64`` architecture. Which can also be named
      ``arm64``.

    - Select an image with file extension ``.raw``.

As an example, to download the image navigate as follows:

 - ``bookworm/`` >> ``latest`` >> ``debian-12-nocloud-arm64.raw``

Using RD-N2 as an example, set ``MODEL`` environment variable to the FVP path,
and run the boot script with argument ``-d`` to the downloaded image path.

.. code-block:: shell

    export MODEL=<absolute/path/to/FVP/binary>
    cd model-scripts/rdinfra
    ./distro.sh -p rdn2 -d <absolute/path/to/image>


Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -d <satadisk_path>

   -  Absolute path to the installed distro disk image created using the
      instructions listed in the previous section.

*  -n [true|false] (optional)

   -  Controls the use of network ports by the model. If network ports have
      to be enabled, use 'true' as the option. Default value is set to
      'false'.

*  -a <additional_params> (optional)

   -  Specify any additional model parameters to be passed. The model
      parameters and the data to be passed to those parameters can be found
      in the FVP documentation.


Install a Linux Distribution
----------------------------

After the build of the platform software stack is complete, a distribution can
be installed into a SATA disk image. Before beginning the installation process,
download the CD iso image of the required distribution version. See below
Linux distributions downloads pages:

- `Fedora <https://alt.fedoraproject.org/alt>`_
- `Ubuntu <https://ubuntu.com/download/server/arm>`_
- `Debian <https://www.debian.org/distrib/netinst>`_

.. important::

    Select an image for the ``aarch64`` architecture. Which can also be named
    ``arm64``.


The generic command to perform the installation is:

.. code-block:: shell

    ./distro.sh -p <platform name> -i <abs_iso_image_path> -s <disk size> -a <additional_params> -n [true|false]

Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -i <abs_iso_image_path>

   - Absolute path to the downloaded distribution installer disk image.

*  -s <disk_size>

   - Size of the SATA disk image (in GB) to be created. 12GB and above is
     good enough for most use cases.

*  -n [true|false] (optional)

   - Controls the use of network ports by the model. If network ports have
     to be enabled, use 'true' as the option. Default value is set to
     'false'.

*  -a <additional_params> (optional)

   - Specify any additional model parameters to be passed. The model
     parameters and the data to be passed to those parameters can be found
     in the FVP documentation.


As an example:

.. code-block:: shell

    export MODEL=<absolute/path/to/FVP/binary>
    cd model-scripts/rdinfra
    ./distro.sh -p rdn2 -i <absolute/path/to/iso> -s 16

- This command creates a 16GB SATA disk image, boots the selected platform
  software stack and starts the installation process.

- From here on, follow the instructions of the chosen distribution
  installer. For more information about the installation procedure, refer
  online installation manuals of the chosen distribution.

- After the installation is complete, a disk image with a random name
  ``<number>.satadisk`` will be created in *model-scripts/rdinfra/* folder.
  Use this disk image for booting the installed distribution.


Additional distribution specific instructions (if any)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Debian
""""""

During installation, the installer will prompt the user with the message *'Load
CD-ROM drivers from removable media?'* and display two options - *'Yes/No'*.
Select the option *'No'*. This is followed by another prompt *'Manually select
a CD-ROM module and device?'* and display two options - *'Yes/No'*. Select the
option *'Yes'*. This brings up the module list required for accessing CD-ROM
and lists two options - *'none'* and *'cdrom'*. Select the option *'none'* and
enter ``/dev/vda``. The installation media on the virtio disk will be detected
and installation continues.

Ubuntu
""""""

During installation, the installer will display options for additional
packages that may require internet connection, such as *'openssh-server'*.
It is important that these options are not selected. Choosing to install
additional packages may cause failure to boot after the satadisk image
is created.


.. _Debian cloud images: https://cloud.debian.org/images/cloud/
