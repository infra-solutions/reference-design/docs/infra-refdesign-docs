WinPE Boot
==========

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Neoverse Reference Design (RD) platform software stack supports the boot of
Windows Pre-installation Environment (WinPE) on RD platforms. A pre-built WinPE
disk image is connected as a SATA disk to the fixed virtual platform (FVP).
During boot, the platform firmware detects the connected WinPE disk image
and boots from it.


Build the platform software
---------------------------

This section describes the procedure to build the platform firmware required to
boot WinPE on Neoverse RD platforms.

To build the RD software stack, the command to be used is

::

  ./build-scripts/build-test-uefi.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - ``clean``
  - ``build``
  - ``package``
  - ``all`` (all of the three above)


Examples of the build command are


- Command to clean, build and package the RD-N2 software stack required for
  WinPE boot on the RD-N2 platform:

  ::

    ./build-scripts/build-test-uefi.sh -p rdn2 all

- Command to remove the generated outputs (binaries) of the software stack for
  the RD-N2 platform:

  ::

    ./build-scripts/build-test-uefi.sh -p rdn2 clean

- Command to perform an incremental build of the software components included in
  the software stack for the RD-N2 platform:

  ::

    ./build-scripts/build-test-uefi.sh -p rdn2 build

.. note::
   This command should be followed by the ``package`` command to complete the
   preparation of the fip image.

- Command to package the previously built software stack and prepares the fip
  image:

  ::

    ./build-scripts/build-test-uefi.sh -p rdn2 package


Obtain the WinPE disk image
---------------------------

Obtain a pre-built WinPE disk image to use it as the disk image to boot from.
Refer to `this page <https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/winpe-intro>`_
for more information.

.. note::
   WinPE version should be ``20262`` or higher.


Boot WinPE
----------

To boot from the WinPE disk image, the commands to be used are:

- Set ``MODEL`` path before launching the model:

  ::

    export MODEL=<absolute path to the platform FVP binary>

- If platform is SGI-575:

  ::

    cd model-scripts/sgi

- If platform is an RD:

  ::

    cd model-scripts/rdinfra

- Launch the FVP to boot WinPE:

  ::

    ./distro.sh -p <platform name> -d <satadisk_path> -a <additional_params> -n [true|false]


Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -d <satadisk_path>

   -  Absolute path to the WinPE disk image created using the previous section.

*  -n [true|false] (optional)

   -  Controls the use of network ports by the model. If network ports have
      to be enabled, use 'true' as the option. Default value is set to
      'false'.

*  -a <additional_params> (optional)

   -  Specify any additional model parameters to be passed. The model
      parameters and the data to be passed to those parameters can be found
      in the FVP documentation.


Example commands to boot WinPE are as listed below.

- Command to begin the WinPE boot on the RD-N2 platform using a
  ``WinPE_arm64.iso`` pre-built disk image. Follow the instructions on console
  to complete the WinPE boot.

  ::

    ./distro.sh -p rdn2 -d /absolute/path/to/WinPE_arm64.iso
