.. _Busybox_boot_label:

Busybox Boot
============

.. important::
    This feature might not be applicable to all Platforms. Please check
    individual Platform pages, section **Supported Features** to confirm if
    this feature is listed as supported.

.. note::
    This section assumes the user has completed the chapter
    :doc:`Getting Started </user_guides/getting_started>` and has a
    functional working environment.

Busybox is a lightweight executable which packages lots of POSIX compliant UNIX
utilities in a single file system. Busybox boot with Neoverse Reference Design
(RD) platform software stack demonstrates the integration of various software
components on the software stack resulting in the ability to boot linux kernel
on RD fixed virtual platform (FVP).

Booting to busybox is especially helpful when porting the software stack for new
platforms which are derivative of Neoverse reference design platform as this can
be quickly executed to ensure that the various software components are properly
integrated and verify the basic functionality of various software components.

This document describes how to build the Neoverse RD platform software stack and
and use it to boot upto busybox on the Neoverse RD FVP.


Build the platform software
---------------------------

This section describes the procedure to build the disk image for busybox boot.
The disk image consists of two partitions. The first partition is a EFI
partition and contains grub. The second partition is a ext3 partition which
contains the linux kernel image. Examples on how to use the build command for
busybox boot are listed below.

To build the software stack, the command to be used is

::

  ./build-scripts/rdinfra/build-test-busybox.sh -p <platform name> <command>

Supported command line options are listed below

* <platform name>

  - Lookup a platform name in :ref:`Platform Names <platform_names>`.

* <command>

  - Supported commands are

    - ``clean``
    - ``build``
    - ``package``
    - ``all`` (all of the three above)

.. note::
   On networks where git port is blocked, the build procedure might not
   progress. Refer the :ref:`troubleshooting guide <troubleshooting>`
   for possible ways to resolve this issue.

Examples of the build command are

- Command to clean, build and package the RD-N2 software stack required for
  busybox boot on RD-N2 platform:

  ::

    ./build-scripts/rdinfra/build-test-busybox.sh -p rdn2 all

- Command to perform an incremental build of the software components included in
  the software stack for the RD-N2 platform.

  ::

    ./build-scripts/rdinfra/build-test-busybox.sh -p rdn2 build

.. note::
   This command should be followed by the ``package`` command to complete the
   preparation of the FIP and the disk image.

- Command to package the previously built software stack and prepare the FIP and
  the disk image.

  ::

    ./build-scripts/rdinfra/build-test-busybox.sh -p rdn2 package


Boot upto Busybox
-----------------

After the build of the platform software stack for busybox boot is complete, the
following commands can be used to start the execution of the *selected platform
fastmodel* and boot the platform up to the busybox prompt. Examples on how to
use the command are listed below.

To boot up to the busybox prompt, the commands to be used are

- Set ``MODEL`` path before launching the model:

  ::

    export MODEL=<absolute path to the platform FVP binary>

- If platform is SGI-575:

  ::

    cd model-scripts/sgi

- If platform is an RD:

  ::

    cd model-scripts/rdinfra

- Launch busybox boot:

  ::

    ./boot.sh -p <platform name> -a <additional_params> -n [true|false]


Supported command line options are listed below

*  -p <platform name>

   - Lookup for a platform name in :ref:`Platform Names <platform_names>`.

*  -n [true|false] (optional)

   - Controls the use of network ports by the model. If network ports have
     to be enabled, use 'true' as the option. Default value is set to
     'false'.

*  -a <additional_params> (optional)

   - Specify any additional model parameters to be passed. The model
     parameters and the data to be passed to those parameters can be found
     in the FVP documentation.

Example commands to boot upto busybox are as listed below.

- Command to start the execution of the RD-N2 model to boot up to the
  Busybox prompt:

  ::

    ./boot.sh -p rdn2

- Command to start the execution of the RD-N2 model to boot up to the
  Busybox prompt with network enabled. The model supports virtio.net
  allowing the software running within the model to access the network:

  ::

    ./boot.sh -p rdn2 -n true

- Command to start the execution of the RD-N2 model with networking
  enabled and to boot up to the Busybox prompt. Additional parameters
  to the model are supplied using the ``-a`` command line parameter:

  ::

    ./boot.sh -p rdn2 -n true -a "-C board.flash0.diagnostics=1"
