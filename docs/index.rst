###########################################
Neoverse Reference Design Platform Software
###########################################

.. toctree::
   :maxdepth: 1
   :caption: About

   about/reference_design
   about/software_stack
   about/repo_tool
   about/troubleshooting
   about/vulnerability_report

.. toctree::
   :maxdepth: 1
   :caption: User Guides

   user_guides/getting_started
   user_guides/learningpaths

.. toctree::
   :maxdepth: 1
   :caption: Platforms

   RD-V3-R1 Cfg1 <platforms/rdv3r1cfg1>
   RD-V3-R1 <platforms/rdv3r1>
   RD-V3 Cfg2 <platforms/rdv3cfg2>
   RD-V3 Cfg1 <platforms/rdv3cfg1>
   RD-V3 <platforms/rdv3>
   RD-V2 <platforms/rdv2>
   RD-N2 Cfg3 <platforms/rdn2cfg3>
   RD-N2 Cfg2 <platforms/rdn2cfg2>
   RD-N2 Cfg1 <platforms/rdn2cfg1>
   RD-N2 <platforms/rdn2>
   RD-V1 MC <platforms/rdv1mc>
   RD-V1 <platforms/rdv1>
   RD-N1 Edge X2 <platforms/rdn1edgex2>
   RD-N1 Edge <platforms/rdn1edge>
   SGI-575 <platforms/sgi575>

.. toctree::
   :maxdepth: 1
   :caption: Features

   features/reset_to_bl31
   features/boot/index
   features/cxl
   features/mcp_sideband_channel
   features/mpam
   features/power/index
   features/ras/ras
   features/systemready_acs
   features/tftf
   features/uefi_sct
   features/virtualization/index
   features/virtio_p9

.. toctree::
   :maxdepth: 1
   :caption: Release Notes

   releases/RD-INFRA-2025.02.04/release_note
   releases/RD-INFRA-2024.12.20/release_note
   releases/RD-INFRA-2024.09.30/release_note
   releases/RD-INFRA-2024.07.15/release_note
   releases/RD-INFRA-2024.04.17/release_note
   releases/RD-INFRA-2024.01.16/release_note
   releases/RD-INFRA-2023.12.22/release_note
   releases/RD-INFRA-2023.09.29/release_note
   releases/RD-INFRA-2023.09.28/release_note
   releases/RD-INFRA-2023.06.30/release_note
   releases/RD-INFRA-2023.06.28/release_note
   releases/RD-INFRA-2023.03.31/release_note
   releases/RD-INFRA-2023.03.29/release_note
