.. _q423_fmt_label:

RD-INFRA-2024.01.16
===================

Release Description
-------------------

Change logs:

TF-M:

* Fremont support patches updated for upstream.
* Tower-NCI driver refactor and renamed to NI-Tower.
* SCP and MCP ATUs are configured as manage mode to let them configure
  respective ATU.
* With latest TF-M upstream, Non-Secure image is not build as part of TF-M.
* MHUv3 driver updated to use in-band communication using door bell channels
  instead of out-band communication.
* RSS to RSS communication enabled via MHUv2. RSS to RSS comms channel is used
  for handshaking and generating vHUK in case of multichip scenario
  (RD-Fremont-Cfg2).
* Reboot support added in RSS to receive and acknowledge any reboot request from
  SCP.

SCP:

* New product group introduced which incorporate all RD platforms. So rdfremont
  folder moved under product/neoverse-rd folder.
* CMN-Cyprus driver module updated match upstream revision.
* Component Port Aggregation (CPA), LCN SAM programming support included in the
  CMN-Cryprus driver module.
* SCP configured to manage its own ATU.
* SMCF support enabled by introducing amu_smcf_drv module.
* Reboot and Power down support enabled.
* CLI debugger enabled for RD-Fremont.
* RAS support.

TF-A:

* Poseidon VANE CPU core MIDR updated and Poseidon V CPU MIDR introduced.
* Moved away from using common arm_def.h header file to Neoverse RD specifc
  sgi_common_def.h header file.
* RD-Fremont variant specific CSS support files introduced which included
  definition for CSS and RoS address space.
* GPT setup from plat/arm/common/arm_bl2_setup.c moved to platform specific
  plat/arm/board/rdfremont/rdfremont_plat.c file.
* MHUv3 driver updated to support in-band communication.
* GPC SMMU block initialized for remote chips.
* Added support for Warm reboot.
* Added support for RAS EINJ.

RMM:

* RMM updated to align with RMM EAC5 specification.
* DRAM management moved to platform specific code.
* Platform setup code made common between FVP and RD-Fremont.

Hafnium:

* Latest upstream change removed clang toolchain from prebuilds. Clang toolchain
  need to be passed via $PATH environment variable.
* Hafnium builds now needs platform config name to be passed while invoking
  build.

edk2:

* EINJ specific structures introduced to ACPI header files.

edk2-platforms:

* Reduced PcdSystemMemorySize to accommodate growing needs to EL3 runtime and
  RMM.
* EINJ and AEST ACPI tables added for RD-Fremont-Cfg1.

Linux:

* Kernel updated to align with align with RMM EAC5 specification.
* AEST ACPI table parser support added.
* Support for vendor defined error injection mechanism added.

kvmtool and kvm-unit-tests:

* Update to align with RMM EAC5 specification.

build-scripts:

* TF-M Non-Secure image package is skipped to align with upstream TF-M change.
* TF-M Chip Manufacturing bundle is packaged on per chip basis.
* SCP build-scripts update to support product group (neoverse-rd).
* clang+llvm-15.0.6 toolchain added as dependency to support hafnium build.
* Toolchain upgraded from GCC 12.3 Rel1 to 13.2 Rel1.
* build-linux updated to support building debian packages. Respective dependency
  added to install prerequisties.
* RAS EINJ, Kernel First error injection and handling support enabled for
  RD-Fremont-Cfg1 config.

model-scripts:

* Load different CM provisioning bundle on per chip basis.
* Updated RSS to RSS MHUv2 doorbell channel count to 5 to support in-band
  communication.
* Updated AP to RSS MHUv3 doorbell channel count to 16 to support in-band
  communication.
* Enabled SMCF tag length input.
* Added shutdown string for MCP. Once this string is printed in MCP console,
  model will quit gracefully.

busybox:

* Upgraded to version 1.36.0

buildroot:

* Upgraded to latest master to include support for GCC 13.2 Rel support.

Miscellaneous:

* The documentation has been migrated to use the 'readthedocs' rendering syntax.
  So it would be essential to setup a readthedocs server to use the links to
  navigate the various pages in the documentation.

Supported Features
------------------

Power Management:

* Support for Shutdown, Cold and Warm reboot  is added . Code changes are done
  in SCP, TF-M for establishing MHU outband communications between SCP-MCP and
  SCP-RSS to relay Shutdown/Reboot SCMI messages.

  - :ref:`Reboot-Shutdown test <reboot_shutdown_label>`

* Necessary configurations for SMCF and AMU are added in SCP. Platform SMCF and
  Client SMCF modules are introduced in SCP. An user control, using AP-SCP
  Non-Secure MHU is added. On receiving MHU signal, SMCF client module will
  start SMCF sampling, capture AMU data for all cores and stop sampling.

  RdFremont FVP is enabled with tag_length support for SMCF sample. It needs
  model parameter to enable tag length, necessary model script change is added.

  - :ref:`RdFremont SMCF <rdv3_smcf_label>`

RAS:

* Error injection from linux kernel Non-Secure world for CPU and SRAM is
  supported. SRAM error, of CE type, handling happens in Root world in context
  of TF-A. CPU error, of type DE, can be handled either Kernel first or Firmware
  first manner. This RAS feature is supported only on RdFremontCfg1 platform.

  A build flag TF_A_RAS_FW_FIRST is present in build-script to opt for Firmware
  first or kernel first mode. Support is added in EDK2 PlatformErrorHandlerDxe
  for handling Vendor specific error injection in kernel. Necessary EINJ ACPI
  table is added. AEST ACPI table is added for error handling in kernel. In
  Linux a new driver for handling vendor specific error injection is added and
  necessary modifications are made in einj driver. AEST driver is added and
  modification are made in linux for handling CPU Deferred Error(DE) error in
  kernel. In TF-A code changes are done for enabling EHF framework, carving out
  region for CPER & EINJ buffers, enabling SRAM 1-bit Corrected Error(CE)
  injection & handling.

  - :ref:`Rdfremont RAS <ras>`

* A command line based RAS error injection and handling module is introduced in
  SCP. Using SCP CLI debugger interfaces, this module allows user to provide RAS
  error injection commands for various components: Peripheral SRAM, SCP TCM,
  RSM SRAM, AP core. This utility module helps in validating RAS capable
  hardware components' behavior when error is detected and reported.

  - :ref:`SCP RAS Error Injection Utility <scp_einj_util>`

Known Limitations
-----------------

* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8GiB. Therefore,
  total DRAM Memory for RD-Fremont-Cfg2 is limited to 8GiB to support Realm VMs
  and Realm KVM unit test.

Test Coverage
-------------

The following tests have been completed using 11.24.16 version of the FVP:

* RD-Fremont

  - Busybox boot, distro boot, buildroot boot, secure boot, virtual machine and
    kvm unit test in realm.

* RD-Fremont-Cfg1

  - Busybox boot, distro boot, buildroot boot, secure boot, virtual machine and
    kvm unit test in realm.
  - Feature test:

    - :ref:`CPPC <cppc_test_label>`
    - :ref:`Reboot-Shutdown test <reboot_shutdown_label>`
    - :ref:`RdFremont SMCF test <rdv3_smcf_label>`
    - :ref:`Rdfremont RAS related test <ras_tests>`
    - :ref:`SCP RAS Error injection utility <scp_einj_util>`

* RD-Fremont-Cfg2

  - Busybox boot, buildroot boot, virtual machine and kvm unit test in realm.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.01.16

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.01.16

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.01.16

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2024.01.16

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 9681574575c02764ff85b4c0903ab61a6327ed16

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.01.16

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.01.16

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2024.01.16

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R06_28_23

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-2.28.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://github.com/buildroot/buildroot
  - Tag/Hash : 3865d88423c18f28f74efd9878a386db9491246f

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Has  : cca/rmm-v1.0-eac5

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/linux-arm/kvm-unit-tests-cca
  - Tag/Has  : cca/rmm-v1.0-eac5
