.. _q323_label:

RD-INFRA-2023.09.29
===================

Release Description
-------------------

- Software stack refreshed for the following platforms.
  
  - :ref:`SGI-575 <SGI_575_readme_label>`
  - :ref:`RD-N1-Edge <RD_N1_Edge_readme_label>`
  - :ref:`RD-N1-Edge-x2 <RD_N1_Edgex2_readme_label>`
  - :ref:`RD-V1 <RD_V1_readme_label>`
  - :ref:`RD-V1-MC <RD_V1_MC_readme_label>`
  - :ref:`RD-N2 <RD_N2_readme_label>`
  - :ref:`RD-N2-Cfg1 <RD_N2_Cfg1_readme_label>`
  - :ref:`RD-N2-Cfg2 <RD_N2_Cfg2_readme_label>`
  - :ref:`RD-N2-Cfg3 <RD_N2_Cfg3_readme_label>`
  - :ref:`RD-V2 <RD_V2_readme_label>`

- Platform software stack build updated to use Arm GCC toolchain version
  12.3.rel1


Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-V2

  - Busybox boot, distro boot, buildroot boot, WinPE boot, ACS, Virtualization,
    tf-a-tests, secure boot.

* RD-N2

  - Busybox boot, distro boot, buildroot boot, WinPE boot, ACS, Virtualization,
    tf-a-tests, secure boot.

* RD-N2-Cfg1

  - Busybox boot, distro boot, buildroot boot, Virtualization, N2 RAS, SRAM RAS.

* RD-N2-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-N2-Cfg3

  - Busybox boot, distro boot, buildroot boot.

* RD-V1

  - Busybox boot, distro boot.

* RD-V1-MC

  - Busybox boot, distro boot.

* RD-N1-Edge

  - Busybox boot, distro boot.
 
* RD-N1-Edge-X2

  - Busybox boot.

* SGI-575

  - Busybox boot.


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* SCP Firmware

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware.git
  - Tag/Hash : RD-INFRA-2023.09.29

* Trusted Firmware-A

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a.git
  - Tag/Hash : RD-INFRA-2023.09.29

* EDK2

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/edk2.git
  - Tag/Hash : RD-INFRA-2023.09.29

* EDK2 Platforms

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms.git
  - Tag/Hash : RD-INFRA-2023.09.29

* Linux

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/linux.git
  - Tag/Hash : RD-INFRA-2023.09.29

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R06_28_23

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-2.28.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot.git
  - Tag/Hash : RD-INFRA-2023.09.29

* kvmtool

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool
  - Tag/Hash : e17d182ad3f797f01947fc234d95c96c050c534b

* kvm-unit-tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests.git
  - Tag/Hash : RD-INFRA-2023.09.29
