.. _q424_label:

RD-INFRA-2024.12.20
===================

Release Description
-------------------

Software stack refreshed for the following platforms:

- :doc:`RD-V3-R1 </platforms/rdv3r1>`
- :doc:`RD-V3-R1-Cfg1 </platforms/rdv3r1cfg1>`
- :doc:`RD-V3-Cfg2 </platforms/rdv3cfg2>`
- :doc:`RD-V3-Cfg1 </platforms/rdv3cfg1>`
- :doc:`RD-V3 </platforms/rdv3>`
- :doc:`RD-N2-Cfg3 </platforms/rdn2cfg3>`
- :doc:`RD-N2-Cfg2 </platforms/rdn2cfg2>`
- :doc:`RD-N2-Cfg1 </platforms/rdn2cfg1>`
- :doc:`RD-N2 </platforms/rdn2>`
- :doc:`RD-V2 </platforms/rdv2>`
- :doc:`RD-V1-MC </platforms/rdv1mc>`
- :doc:`RD-V1 </platforms/rdv1>`
- :doc:`RD-N1-Edge-X2 </platforms/rdn1edgex2>`
- :doc:`RD-N1-Edge </platforms/rdn1edge>`
- :doc:`SGI-575 </platforms/sgi575>`

FVP versions:

* RD-V3-R1 & RD-V3-R1-Cfg1       : 11.27.51
* RD-V3, RD-V3-Cfg1 & RD-V3-Cfg2 : 11.27.51
* RD-N2, RD-N2-Cfg1 & RD-N2-Cfg2 : 11.25.23
* RD-V2                          : 11.24.12
* RD-V1 and RD-N1 variants       : 11.17.29
* SGI-575                        : 11.15.26

Change Log
----------

TF-M:

* Rebased to latest main branch

SCP:

* Rebased to latest main branch
* Added compact HN table support
* Added memory region that targets GIC HNI
* Updated LCP ram size
* Fixed boot flash HNI target id in RD-V3-R1-Cfg1
* Updated DRAM2 base address in RD-V3-R1 and RD-V3-R1-Cfg1

TF-A:

* Rebased to latest main branch
* Added Local Chip Addressing (LCA) support for RD-N2-Cfg2 and RD-V3-Cfg2
* Updated console name to checksum calculation on RD-V3-R1 and RD-V3
* Enabled SMMUv3 polling timeout
* Updated DRAM2 base address in RD-V3-R1 and RD-V3-R1-Cfg1
* MbedTLS version has been updated to 3.6.2

RMM:

* Rebased to latest main branch

Hafnium:

* Kept at last release, FF-A version 1.2 is not supported by the platform

edk2:

* Rebased to latest main branch

edk2-platforms:

* Rebased to latest main branch
* Enabled support to autogenerate SoC expansion block iort table
* Added support to print Firmware Version
* Updated DRAM2 base address in RD-V3-R1 and RD-V3-R1-Cfg1

Linux:

* No updates

kvmtool and kvm-unit-tests:

* No updates

build-scripts:

* Added support to print Firmware Version in EDK2

container-scripts:

* Introduced rootless docker run feature

model-scripts:

* No updates

buildroot:

* No updates

Supported Features
------------------

* Introduced rootless docker run feature for the container environment

Known Limitations
-----------------

* Virtual machine in Realm state is not booting due to a defect in RMM
  component.
* For RD-V3-Cfg2, boot times have increased and it is suggested to use HEADLESS
  mode as a workaround by using the -j option with the boot scripts.
  Example: `./boot.sh -p rdv3cfg2 -j`.
  This will not launch any UART xterm windows, but the UART logs will be
  captured in the log file.
* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8 GiB.
  Therefore, total DRAM Memory for RD-V3-Cfg2 is limited to 8 GiB to support
  Realm VMs and Realm KVM unit test.
* *LocateHandleBuffer_Func* tests of UEFI SCT test suite, which are executed as
  part of the SystemReady Compliance Program are experiencing prolonged
  execution times and the suite may timeout before test completion.

Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-V3-R1

  - Busybox boot, buildroot boot, distro boot.

* RD-V3-R1-Cfg1

  - Busybox boot, buildroot boot, distro boot.

* RD-V3-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-V3-Cfg1

  - Busybox boot, distro boot, buildroot boot, realm tests.

* RD-V3

  - Busybox boot, distro boot, buildroot boot, ACS, Virtualization.

* RD-V2

  - Busybox boot, distro boot.

* RD-N2

  - Busybox boot, distro boot.

* RD-N2-Cfg1

  - Busybox boot, distro boot.

* RD-N2-Cfg2

  - Busybox boot, distro boot.

* RD-N2-Cfg3

  - Busybox boot, distro boot.

* RD-V1

  - Busybox boot.

* RD-V1-MC

  - Busybox boot.

* RD-N1-Edge

  - Busybox boot.

* RD-N1-Edge-X2

  - Busybox boot.

* SGI-575

  - Busybox boot.


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.12.20

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.12.20

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.12.20

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2024.12.20

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.12.20

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.12.20

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2024.12.20

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : G20240322

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-3.6.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_1

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2024.12.20

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Hash : cca/v2

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests
  - Tag/Hash : RD-INFRA-2024.12.20
