.. _q123_fmt_label:

RD-INFRA-2023.03.29
===================

Release Description
-------------------

* Introduce support for :ref:`RD-Fremont <RD_V3_readme_label>` and :ref:`RD-Fremont-Cfg1 <RD_V3_Cfg1_readme_label>` platforms.

Test Coverage
-------------

The following tests have been completed using 11.21.18 version of the FVP:

* RD-Fremont

  - Busybox boot  distro boot, buildroot boot.

* RD-Fremont-Cfg1

  - Busybox boot  distro boot, buildroot boot.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2023.03.29

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2023.03.29

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2023.03.29

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2023.03.29

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2023.03.29

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2023.03.29

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2023.03.29

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R09_25_20

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-2.28.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_33_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://github.com/buildroot/buildroot
  - Tag/Hash : 2020.05
