.. _q223_fmt_label:

RD-INFRA-2023.06.28
===================

Release Description
-------------------

-- Release introduces changes to the following platforms

- :ref:`RD-Fremont <RD_V3_readme_label>`
- :ref:`RD-Fremont-Cfg1 <RD_V3_Cfg1_readme_label>`
- :ref:`RD-Fremont-Cfg2 <RD_V3_Cfg2_readme_label>`

-- Changes introduced in this release

* Introduce support for RD-Fremont-Cfg2 (quad-chip) platform
* Introduce beta support for RME
* Introduce beta support for Measured Boot in TF-M and TF-A
* Enable BL1->BL2 based boot flow
* Add support for NI-Tower in TF-M
* Add support for HN-S Isolation feature in CMN-Cyprus driver
* Add support for Bypass Discovery feature in CMN-Cyprus driver

Known Limitations
-----------------

* System TCU+TBU is not present in the 11.22.16 version of the FVP. So GPC with
  System TCU+TBU is not enabled in the software.

Test Coverage
-------------

The following tests have been completed using 11.22.16 version of the FVP:

* RD-Fremont

  - Busybox boot, distro boot, buildroot boot.

* RD-Fremont-Cfg1

  - Busybox boot, distro boot, buildroot boot.

* RD-Fremont-Cfg2

  - Busybox boot, buildroot boot.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2023.06.28

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2023.06.28

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2023.06.28

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2023.06.28

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2023.06.28

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2023.06.28

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2023.06.28

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R09_25_20

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-2.28.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_33_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://github.com/buildroot/buildroot
  - Tag/Hash : 2023.02
