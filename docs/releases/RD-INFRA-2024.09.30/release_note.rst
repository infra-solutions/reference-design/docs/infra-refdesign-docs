.. _q324_label:

RD-INFRA-2024.09.30
===================

Release Description
-------------------

Added support for following new platforms:

- :doc:`RD-V3-R1 </platforms/rdv3r1>`
- :doc:`RD-V3-R1-Cfg1 </platforms/rdv3r1cfg1>`

Software stack refreshed for the following platforms:

- :doc:`RD-V3-Cfg2 </platforms/rdv3cfg2>`
- :doc:`RD-V3-Cfg1 </platforms/rdv3cfg1>`
- :doc:`RD-V3 </platforms/rdv3>`
- :doc:`RD-N2-Cfg3 </platforms/rdn2cfg3>`
- :doc:`RD-N2-Cfg2 </platforms/rdn2cfg2>`
- :doc:`RD-N2-Cfg1 </platforms/rdn2cfg1>`
- :doc:`RD-N2 </platforms/rdn2>`
- :doc:`RD-V2 </platforms/rdv2>`
- :doc:`RD-V1-MC </platforms/rdv1mc>`
- :doc:`RD-V1 </platforms/rdv1>`
- :doc:`RD-N1-Edge-X2 </platforms/rdn1edgex2>`
- :doc:`RD-N1-Edge </platforms/rdn1edge>`
- :doc:`SGI-575 </platforms/sgi575>`

FVP versions:

* RD-V3-R1 & RD-V3-R1-Cfg1       : 11.27.25
* RD-V3, RD-V3-Cfg1 & RD-V3-Cfg2 : 11.27.25
* RD-N2, RD-N2-Cfg1 & RD-N2-Cfg2 : 11.25.23
* RD-V2                          : 11.24.12
* RD-V1 and RD-N1 variants       : 11.17.29
* SGI-575                        : 11.15.26

*Change logs:*

TF-M:

* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.
* Configured ATU to access AP shared SRAM in RdV3.
* Added interrupt handler for SCP-RSE MHUv3.
* Added common area for Neoverse sub-platforms.
* Added BL2 config multiload support.
* Added AP reset to BL31 support.

SCP:

* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.
* Enabled warm reboot support in RdV3 platform variants.
* Added AP reset to BL31 support.

TF-A:

* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.
* Enabled warm reboot support in RdV3 platform variants.
* Added AP reset to BL31 support.

RMM:

* Rebased to latest main branch.
* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.

Hafnium:

* Kept at last release, FF-A version 1.2 is not supported by the platform.

edk2:

* Rebased to latest main branch.

edk2-platforms:

* Added new AEST node entries to AEST ACPI table to represent CMN RAS errors on
  RD-V3-Cfg1 platform.
* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.

Linux:

* Added support for CMN Cyprus (CMN S3) Kernel First Handling on RD-V3-Cfg1
  platform. This feature can be validated only on Pre-Silicon platform. The
  software (linux kernel drivers, ACPI tables) are all functional.

kvmtool and kvm-unit-tests:

* No updates.

build-scripts:

* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.
* Added AP reset to BL31 support.

model-scripts:

* Added support for new platforms RD-V3-R1 and RD-V3-R1-Cfg1.
* Added AP reset to BL31 support.

buildroot:

* No updates

Supported Features
------------------

* Warm Reset support
* Reset to BL31 support

Known Limitations
-----------------

* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8GiB. Therefore,
  total DRAM Memory for RD-V3-Cfg2 is limited to 8GiB to support Realm VMs
  and Realm KVM unit test.

Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-V3-R1

  - Busybox boot, buildroot boot, distro boot.

* RD-V3-R1-Cfg1

  - Busybox boot, buildroot boot, distro boot.

* RD-V3-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-V3-Cfg1

  - Busybox boot, distro boot, buildroot boot, realm tests.

* RD-V3

  - Busybox boot, distro boot, buildroot boot, ACS, Virtualization.

* RD-V2

  - Busybox boot, distro boot.

* RD-N2

  - Busybox boot, distro boot.

* RD-N2-Cfg1

  - Busybox boot, distro boot.

* RD-N2-Cfg2

  - Busybox boot, distro boot.

* RD-N2-Cfg3

  - Busybox boot, distro boot.

* RD-V1

  - Busybox boot.

* RD-V1-MC

  - Busybox boot.

* RD-N1-Edge

  - Busybox boot.

* RD-N1-Edge-X2

  - Busybox boot.

* SGI-575

  - Busybox boot.


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.09.30

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.09.30

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.09.30

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2024.09.30

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.09.30

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.09.30

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2024.09.30

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : G20240322

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-3.6.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2024.09.30

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Hash : cca/v2

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests
  - Tag/Hash : RD-INFRA-2024.09.30
