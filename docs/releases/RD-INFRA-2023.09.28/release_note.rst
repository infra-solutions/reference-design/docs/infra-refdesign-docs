.. _q323_fmt_label:

RD-INFRA-2023.09.28
===================

Release Description
-------------------

-- Release introduces changes to the following platforms

- :ref:`RD-Fremont <RD_V3_readme_label>`
- :ref:`RD-Fremont-Cfg1 <RD_V3_Cfg1_readme_label>`
- :ref:`RD-Fremont-Cfg2 <RD_V3_Cfg2_readme_label>`

-- Changes introduced in this release

* Introduce CCA CoT support in TF-A
* Updates to RMM, Linux and KVM tools to align to the RMM EAC2 specification
* Introduce support for Hafnium in RD-Fremont and RD-Fremont-Cfg1
* Enable Secure Boot support for RD-Fremotn and RD-Fremont-Cfg1
* Add support for NI-Tower in SCP
* Enable configuring IO Virtualization block with NI-Tower driver in SCP
* Enable Dynamic PCIe support
* Add alpha support for Component Port Aggregation (CPA) in CMN-Cyprus driver
* Add alpha support for Expanded RAID in CMN-Cyprus driver
* Add support for configuring the GPC SMMU (System TCU+TBU)
* Enable support for Branch Record Buffer Extension (BRBE)
* Update software compoenents to latest upstream

Known Limitations
-----------------

* Hafnium is not enabled for RD-Fremont-Cfg2

Test Coverage
-------------

The following tests have been completed using 11.23.11 version of the FVP:

* RD-Fremont

  - Busybox boot, distro boot, buildroot boot, secure boot, virtual machine and
    kvm unit test in realm.

* RD-Fremont-Cfg1

  - Busybox boot, distro boot, buildroot boot, secure boot, virtual machine and
    kvm unit test in realm.

* RD-Fremont-Cfg2

  - Busybox boot, buildroot boot, virtual machine and kvm unit test in realm.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2023.09.28

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2023.09.28

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2023.09.28

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2023.09.28

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2023.09.28

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2023.09.28

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2023.09.28

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R06_28_23

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-2.28.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2023.09.28
