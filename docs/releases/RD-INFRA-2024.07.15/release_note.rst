.. _q224_label:

RD-INFRA-2024.07.15
===================

Release Description
-------------------

Software stack refreshed for the following platforms.

- :doc:`RD-V3-Cfg2 </platforms/rdv3cfg2>`
- :doc:`RD-V3-Cfg1 </platforms/rdv3cfg1>`
- :doc:`RD-V3 </platforms/rdv3>`
- :doc:`RD-N2-Cfg3 </platforms/rdn2cfg3>`
- :doc:`RD-N2-Cfg2 </platforms/rdn2cfg2>`
- :doc:`RD-N2-Cfg1 </platforms/rdn2cfg1>`
- :doc:`RD-N2 </platforms/rdn2>`
- :doc:`RD-V2 </platforms/rdv2>`
- :doc:`RD-V1-MC </platforms/rdv1mc>`
- :doc:`RD-V1 </platforms/rdv1>`
- :doc:`RD-N1-Edge-X2 </platforms/rdn1edgex2>`
- :doc:`RD-N1-Edge </platforms/rdn1edge>`
- :doc:`SGI-575 </platforms/sgi575>`

*Change logs:*

TF-M:

* Renamed RdFremont to RdV3.

SCP:

* Renamed RdFremont to RdV3.

TF-A:

* Renamed RdFremont to RdV3.

RMM:

* Renamed RdFremont to RdV3.

Hafnium:

* Kept at last release, FF-A version 1.2 is not supported by the platform.

edk2:

* Rebased to latest master.

edk2-platforms:

* Added MPAM support for the RDV3 platform.

* Renamed RdFremont to RdV3.

Linux:

* Updated to CCA v3.

kvmtool and kvm-unit-tests:

* Updated to CCA v2.

build-scripts:

* Renamed all instances of RdFremont to RdV3 in config data.

* Build LKVM as a static binary to remove the dependancy on the target OS libc.

model-scripts:

* Renamed all instances of RdFremont to RdV3 in config data.

* Update run_model parameter to use iris interface for DS5 connection.

buildroot:

* Rebased to latest master.

Supported Features
------------------

MPAM:

* Added MPAM resctrl support for the RD-V3 platform. Please note that MPAM from
  a performance stand-point cannot be tried out on FVP. The software layers
  (programming schemata, discovering MSCs via ACPI) should all be functional.

* Unified MPAM support for RD-V3 and RD-N2-Cfg1 with the same kernel tag.

* MPAM kernel tag has been moved to v6.7-rc2.

Known Limitations
-----------------

* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8GiB. Therefore,
  total DRAM Memory for RD-V3-Cfg2 is limited to 8GiB to support Realm VMs
  and Realm KVM unit test.
* In RD-V3-Cfg2 FVP, the peripheral base address on the remote chip's IO
  Block is not within the chip address space. Due to this, their NoC S3 blocks
  cannot be initialised. Because of this, only Chip 0's PCIe devices are
  enumerated and published to the OS.

Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-V3-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-V3-Cfg1

  - Busybox boot, distro boot, buildroot boot, realm tests.

* RD-V3

  - Busybox boot, distro boot, buildroot boot, ACS, Virtualization.

* RD-V2

  - Busybox boot, distro boot.

* RD-N2

  - Busybox boot, distro boot.

* RD-N2-Cfg1

  - Busybox boot, distro boot.

* RD-N2-Cfg2

  - Busybox boot, distro boot.

* RD-N2-Cfg3

  - Busybox boot, distro boot.

* RD-V1

  - Busybox boot.

* RD-V1-MC

  - Busybox boot.

* RD-N1-Edge

  - Busybox boot.

* RD-N1-Edge-X2

  - Busybox boot.

* SGI-575

  - Busybox boot.


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.07.15

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.07.15

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.07.15

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2024.07.15

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.07.15

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.07.15

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2024.07.15

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : G20240322

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-3.6.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2024.07.15

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Hash : cca/v2

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests
  - Tag/Hash : RD-INFRA-2024.07.15
