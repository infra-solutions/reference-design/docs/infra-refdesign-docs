.. _q424-1:

RD-INFRA-2025.02.04
===================

Release Description
-------------------

This release fixes the Realm state virtual machine launch that was reported
as not working in :ref:`RD-INFRA-2024.12.20 <q424_label>`. The fixes are
provided in the RMM component.

- :doc:`RD-V3-R1 </platforms/rdv3r1>`
- :doc:`RD-V3-R1-Cfg1 </platforms/rdv3r1cfg1>`
- :doc:`RD-V3-Cfg2 </platforms/rdv3cfg2>`
- :doc:`RD-V3-Cfg1 </platforms/rdv3cfg1>`
- :doc:`RD-V3 </platforms/rdv3>`

FVP versions:

* RD-V3-R1 & RD-V3-R1-Cfg1       : 11.27.51
* RD-V3, RD-V3-Cfg1 & RD-V3-Cfg2 : 11.27.51

Change Log
----------

TF-M:

* No updates

SCP:

* No updates

TF-A:

* No updates

RMM:

* Fix to hide MPAM from Realm state

Hafnium:

* No updates

edk2:

* No updates

edk2-platforms:

* No updates

Linux:

* Update to cca/v6

kvmtool

* Update to cca/v4

kvm-unit-tests:

* No updates

build-scripts:

* No updates

container-scripts:

* No updates

model-scripts:

* No updates

buildroot:

* No updates

Supported Features
------------------

* No updates

Known Limitations
-----------------

* For RD-V3-Cfg2, boot times have increased and it is suggested to use HEADLESS
  mode as a workaround by using the -j option with the boot scripts.
  Example: `./boot.sh -p rdv3cfg2 -j`.
  This will not launch any UART xterm windows, but the UART logs will be
  captured in the log file.
* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8 GiB.
  Therefore, total DRAM Memory for RD-V3-Cfg2 is limited to 8 GiB to support
  Realm VMs and Realm KVM unit test.
* *LocateHandleBuffer_Func* tests of UEFI SCT test suite, which are executed as
  part of the SystemReady Compliance Program are experiencing prolonged
  execution times and the suite may timeout before test completion.

Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-V3-R1

  - Virtual machine boot in Realm state, KVM-UT

* RD-V3-R1-Cfg1

  - Virtual machine boot in Realm state, KVM-UT

* RD-V3-Cfg2

  - Virtual machine boot in Realm state, KVM-UT

* RD-V3-Cfg1

  - Virtual machine boot in Realm state, KVM-UT

* RD-V3

  - Virtual machine boot in Realm state, KVM-UT


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.12.20

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.12.20

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.12.20

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2025.02.04

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.12.20

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.12.20

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2025.02.04

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : G20240322

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-3.6.0

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_1

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2024.12.20

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Hash : cca/v4

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests
  - Tag/Hash : RD-INFRA-2024.12.20
