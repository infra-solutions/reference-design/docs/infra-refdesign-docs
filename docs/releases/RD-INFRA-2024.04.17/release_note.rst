.. _q124_label:

RD-INFRA-2024.04.17
===================

Release Description
-------------------

Software stack refreshed for the following platforms.

- :doc:`RD-Fremont-Cfg2 </platforms/rdv3cfg2>`
- :doc:`RD-Fremont-Cfg1 </platforms/rdv3cfg1>`
- :doc:`RD-Fremont </platforms/rdv3>`
- :doc:`RD-N2-Cfg3 </platforms/rdn2cfg3>`
- :doc:`RD-N2-Cfg2 </platforms/rdn2cfg2>`
- :doc:`RD-N2-Cfg1 </platforms/rdn2cfg1>`
- :doc:`RD-N2 </platforms/rdn2>`
- :doc:`RD-V2 </platforms/rdv2>`
- :doc:`RD-V1-MC </platforms/rdv1mc>`
- :doc:`RD-V1 </platforms/rdv1>`
- :doc:`RD-N1-Edge-X2 </platforms/rdn1edgex2>`
- :doc:`RD-N1-Edge </platforms/rdn1edge>`
- :doc:`SGI-575 </platforms/sgi575>`

*Change logs:*

TF-M:

* RSS renamed to RSE (Runtime Security Engine).
* Fremont support patches are upstreamed.
* Checks are added to identify overlapping regions when new PSAM or APU region
  is added in NI-Tower driver.
* Memory maps are segregated to host_*_memory_map.h files.
* Separate SMMUv3 driver added.

SCP:

* Added support for NoC S3 (NI-Tower) driver module.
* Added support for Address remapper driver module for providing APIs capable of
  doing read-write operations in AP memory map.
* Introduced IO Block module for RD-Fremont platform.
* Unified PCIe Setup module for all the RD platforms.
* Enabled support for PCIe setup in RD-Fremont-Cfg2 platform.

TF-A:

* Refactored platform support for neoverse platforms in
  plat/arm/board/neoverse_rd/ directory.

    * Generations of nrd (Neoverse-rd):

        - nrd1, nrd_plat1 for A75/N1/V1 platforms.
        - nrd2, nrd_plat2 for N2/V2 platforms.
        - nrd3, nrd_plat3 for V3 platforms.

* Setup code common to various generations of neoverse-rd platforms moved to
  plat/arm/board/neoverse_rd/common/ directory.
* Platform specific setup code kept in plat/arm/board/neoverse_rd/platform/rdn2,
  plat/arm/board/neoverse_rd/platform/rdfremont etc.
* Added support to read SDS data in a multichip setup.
* Added support for initialising IO Block SMMUs.
* Added support for Firmware first error handling for CPU, SRAM.
* Updated CPER buffer mapping.
* Enabled logical partition support for platform with Hafnium.
* Added support to delegate RAS interrupt to Secure partition.

RMM:

* Introduced a new console library at lib/console.
* Console information is now passed in boot manifest.
* PL011 driver now uses console library APIs and work with the console info
  from boot manifest to initialize console.
* plat/fvp and plat/rdfremont are now merged into a common plat/arm.
* RD-Fremont config is now reduced to minimum with configuration only for
  RMM_MAX_SIZE and RMM_MAX_GRANULES.

Hafnium:

* Rebased to latest master.

edk2:

* Rebased to latest master.

edk2-platforms:

* Rebased to latest master.
* Enabled HEST for Firmware first error logging in kernel.
* Enabled CPU and SRAM error handling and logging in secure partition.
* Converted Einj addresses to platform specific PCDs.

Linux:

* SMMU-test-engine patches integrated on top of EAC5 branch in order to support
  IO-virtualization use-case.
* Enabled SDEI.
* EDAC module added.
* Enabled Arm RAS trace events.

kvmtool and kvm-unit-tests:

* Added a new script (run_tests_kvmtool_arm.sh) to run non-secure
  kvm-unit-tests. This script allows running non-secure kvm-unit-tests on
  buildroot filesystem itself without booting into a Linux distro.

build-scripts:

* Make build support on SCP build-script is deprecated.
* Additional build flag can be passed to SCP through `SCP_BUILD_FLAGS` parameter
  from config data.
* SCP build system defaults to Ninja.
* Improved incremental build support for SCP.
* Renamed all instances of RSS to RSE in config data.
* TF-A build configs updated to accommodate the latest refactoring.
* TF-M build script is update to accommodate different provisioning bundle per
  chip.
* Toolchain base path in updated from `${WORKSPACE}/tools/gcc` to
  `${WORKSPACE}/tools`
* Enabled io-virtualization tests on RD-N2 and RD-Fremont platforms configs.
* RAS support enabled on RD-Fremont-Cfg1 config.
* RAS daemon support enabled for RD-N2-Cfg1 and RD-Fremont-Cfg1 buildroot
  configs.
* Enabled build support for SBSA ACS.

model-scripts:

* RSE CM bundle load location updated for RD-Fremont variants.
* Distro support enabled for RD-Fremont-Cfg2.
* ACS support enabled for RD-Fremont.

buildroot:

* Added rasdaemon tool.

Miscellaneous:

* The documentation has been restructured for better navigation.

Supported Features
------------------

Power Management:

* Support for Shutdown, Cold and Warm reboot  is added . Code changes are done
  in SCP, TF-M for establishing MHU outband communications between SCP-MCP and
  SCP-RSS to relay Shutdown/Reboot SCMI messages.

  - :ref:`Reboot-Shutdown test <reboot_shutdown_label>`

* Necessary configurations for SMCF and AMU are added in SCP. Platform SMCF and
  Client SMCF modules are introduced in SCP. An user control, using AP-SCP
  Non-Secure MHU is added. On receiving MHU signal, SMCF client module will
  start SMCF sampling, capture AMU data for all cores and stop sampling. In TF-A
  MPMM and AMU Aux counters are enabled using fconf.

  - :ref:`RdFremont SMCF <rdv3_smcf_label>`

RAS:

* Error injection from linux kernel for CPU and SRAM is supported. SRAM error,
  of CE type, handling happens in Root world in context of TF-A. CPU error, of
  type DE, can be handled either Kernel first or Firmware first manner. This
  RAS feature is supported only on RdFremontCfg1 and RdN2Cfg1 platform.

  A build flag TF_A_RAS_FW_FIRST is present in build-script to opt for Firmware
  first or kernel first mode.

  Support is added in EDK2 PlatformErrorHandlerDxe for handling Vendor specific
  error injection in kernel. Necessary EINJ ACPI table is added. AEST ACPI table
  is added for error handling in kernel.

  In Linux a new driver for handling vendor specific error injection is added
  and necessary modifications are made in einj driver. AEST driver is added and
  modification are made in linux for handling CPU Deferred Error(DE) error in
  kernel. In kernel, also EDAC module is added for logging CPU errors in EDAC
  sysfs interfaces. FTRACE is enabled in kernel to log ARM RAS traces.

  In TF-A code changes are done for enabling EHF framework, carving out
  region for CPER & EINJ buffers, enabling SRAM 1-bit Corrected Error(CE)
  injection & handling. During Firmware first handling, error is logged in CPER
  and using SDEI mechanism passed onto kernel.

  In Buildroot, Rasdaemon is enabled to capture Arm RAS trace events.

  - :ref:`Rdfremont RAS <ras>`

* A command line based RAS error injection and handling module is introduced in
  SCP. Using SCP CLI debugger interfaces, this module allows user to provide RAS
  error injection commands for various components: Peripheral SRAM, SCP TCM,
  RSM SRAM, AP core. This utility module helps in validating RAS capable
  hardware components' behavior when error is detected and reported.

  - :ref:`SCP RAS Error Injection Utility <scp_einj_util>`


Known Limitations
-----------------

* AArch64 host native build doesn't support launch of virtual machine and kvm
  unit test in realm due to missing library dependency in buildroot. Boot to
  shell of busybox and buildroot is supported.
* Current RMM release does not support creating Granules beyond 8GiB. Therefore,
  total DRAM Memory for RD-Fremont-Cfg2 is limited to 8GiB to support Realm VMs
  and Realm KVM unit test.
* In RD-Fremont-Cfg2 FVP, the peripheral base address on the remote chip's IO
  Block is not within the chip address space. Due to this, their NoC S3 blocks
  cannot be initialised. Because of this, only Chip 0's PCIe devices are
  enumerated and published to the OS.

Test Coverage
-------------

The following tests have been completed for this release. The FVP version
used is platform specific and can be found in the in the release tags section
of the platform readme.

* RD-Fremont-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-Fremont-Cfg1

  - Busybox boot, distro boot, buildroot boot, realm tests.

* RD-Fremont

  - Busybox boot, distro boot, buildroot boot, ACS, Virtualization.

* RD-V2

  - Busybox boot, distro boot, buildroot boot, WinPE boot, ACS, Virtualization,
    tf-a-tests, secure boot.

* RD-N2

  - Busybox boot, distro boot, buildroot boot, WinPE boot, ACS, Virtualization,
    tf-a-tests, secure boot.

* RD-N2-Cfg1

  - Busybox boot, distro boot, buildroot boot, Virtualization, N2 RAS, SRAM RAS.

* RD-N2-Cfg2

  - Busybox boot, distro boot, buildroot boot.

* RD-N2-Cfg3

  - Busybox boot, distro boot, buildroot boot.

* RD-V1

  - Busybox boot, distro boot.

* RD-V1-MC

  - Busybox boot, distro boot.

* RD-N1-Edge

  - Busybox boot, distro boot.

* RD-N1-Edge-X2

  - Busybox boot.

* SGI-575

  - Busybox boot.


Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.


* Trusted Firmware-M

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-m
  - Tag/Hash : RD-INFRA-2024.04.17

* SCP Firmware

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware
  - Tag/Hash : RD-INFRA-2024.04.17

* Trusted Firmware-A

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/trusted-firmware-a
  - Tag/Hash : RD-INFRA-2024.04.17

* Trusted Firmware-RMM

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/tf-rmm
  - Tag/Hash : RD-INFRA-2024.04.17

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* EDK2

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2
  - Tag/Hash : RD-INFRA-2024.04.17

* EDK2 Platforms

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/edk2-platforms
  - Tag/Hash : RD-INFRA-2024.04.17

* Linux

  - Source   : https://gitlab.arm.com/infra-solutions/reference-design/platsw/linux
  - Tag/Hash : RD-INFRA-2024.04.17

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.04

* ACPICA

  - Source   : https://github.com/acpica/acpica
  - Tag/Hash : R09_25_20

* Mbed TLS

  - Source   : https://github.com/ARMmbed/mbedtls.git
  - Tag/Hash : mbedtls-3.4.1

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* EFI Tools

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools
  - Tag/Hash : v1.9.2

* Buildroot

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/buildroot
  - Tag/Hash : RD-INFRA-2024.04.17

* KVM tool

  - Source   : https://git.gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag/Hash : cca/rmm-v1.0-eac5

* KVM unit tests

  - Source   : https://git.gitlab.arm.com/infra-solutions/reference-design/valsw/kvm-unit-tests
  - Tag/Hash : RD-INFRA-2024.04.17
