.. _RD_V1_MC_readme_label:

RD-V1 MC Platform
=================

Overview
--------

RD (Reference Design) is a collection of resources to provide a representative
view of typical compute subsystems that can be designed and implemented using
specific generations of Arm IP.

RD-V1 MC is a quad-chip platform in which four identical chips are connected
through high speed CCIX link. The CCIX link is enabled through CMN-650 Coherent
Multichip Link (CML) feature. RD-V1 MC in particular is based on the following
hardware configuration.

- 128xMP1 Neoverse V1 CPUs
- CMN-650 interconnect
- Multiple AXI expansion ports for I/O Coherent PCIe, Ethernet, offload
- Arm Cortex-M7 for System Control Processor (SCP) and
  Manageability Control Processor (MCP)

The Fixed Virtual Platform of RD-V1 MC supports a reduced configuration of the
above configuration. That is, there are a total of 16xMP1 Neoverse V1 CPUs,
4xMP1 Neoverse CPUs on each chip on RD-V1 MC FVP.

The components integrated into this stack are described in
:ref:`Software Stack <software_stack>` section.


Supported Features
------------------

RD-V1 MC platform software stack supports the following features.

- :ref:`Busybox Boot <Busybox_boot_label>`.
- :ref:`Low power idle <low_power_idle_label>`
- :ref:`Collaborative processor performance control <cppc_test_label>`

Follow the links above for detailed information about the build and execute
steps for each of the supported features.


Obtaining FVP
-------------

The latest version of the RD-V1 MC fixed virtual platform (FVP) can be
downloaded from the `Arm Ecosystem FVPs`_ page. On this page, navigate to
"Neoverse Infrastructure FVPs" section to download the RD-V1 platform
FVP installer.

Follow the instructions of the installer and setup the FVP. The installer, by
default, selects the home directory to install the FVP. To opt for different
directory than the one selected by the installer, provide an absolute path to
that directory when prompted for during the FVP installation process.


Release Tags
------------

Table below lists the release tags for the RD-V1 Quad-Chip platform software
stack and the corresponding RD-V1 MC FVP version that is recommended to be used
along with the listed release tag. The summary of the changes introduced and
tests validated in each release is listed in the release note, the link to
which is in the 'Release Tag' column in the table below.

+-------------------------------------------+------------------------------+
|     Release Tag                           |     RD-V1 MC FVP Version     |
+===========================================+==============================+
|  :ref:`RD-INFRA-2024.12.20 <q424_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.09.30 <q324_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.07.15 <q224_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.04.17 <q124_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.12.22 <q423_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.09.29 <q323_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.06.30 <q223_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.03.31 <q123_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+


.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
