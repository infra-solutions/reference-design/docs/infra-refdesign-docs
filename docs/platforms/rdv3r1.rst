.. _RD_V3_R1_readme_label:

RD-V3-R1 Platform
=================

Overview
--------

RD (Reference Design) is a collection of resources to provide a representative
view of typical compute subsystems that can be designed and implemented using
specific generations of Arm IP.

RD-V3-R1 is a dual-chip platform in which two identical chips are connected
through high speed CCG link. The CCG link is enabled through CMN S3 Coherent
Multichip Link (CML) feature. RD-V3-R1 platform also supports the Realm
Management Extension (RME). The RD-V3-R1 platform in particular has the
following hardware configuration on each chip.

- Up to 70xMP1 Neoverse Poseidon-V3 cores with Direct Connect and 2MB of
  dedicated, private L2 cache for each core.
- 7 Shared LCP Groups, 10 AP cores per Shared LCP Group.
- CMN S3 Revision 2 (CMN S3 R2) interconnect with 9x8 mesh network.
- Multiple AXI expansion ports for I/O Coherent PCIe, Ethernet, offload
- Arm Cortex-M55 processor for Runtime Security Engine (RSE) to support Hardware
  Enforced Security (HES)
- Arm Cortex-M7 for System Control Processor (SCP) and
  Manageability Control Processor (MCP)
- Arm Cortex-M55 processor for Local Control Processor (LCP) for local power
  management of each Application Processor (AP)

The Fixed Virtual Platform of RD-V3-R1 config supports dual chip with
14xMP1 Neoverse Poseidon-V3 CPUs per chip (2 AP cores per LCP Group)

The components integrated into this stack are described in
:ref:`Software Stack <software_stack>` section.


Platform Specific Details
-------------------------

The following documents provide specific details applicable for
RD-V3-R1 Platform:

- :ref:`Boot Flow <rdv3_boot_flow_mc>`
- :ref:`CMN Cyprus Driver Module <cmn_cyprus>`
- :ref:`CMN Cyprus Multichip Configuration <cmn_cyprus_multichip_config>`
- :ref:`Image Loading via MCUboot <mcuboot>`
- :ref:`Local Control Processor <lcp_label>`
- :ref:`Multichip Memory Map <rd_multichip_memory_map>`
- :ref:`NI-Tower System Control <ni_tower_system_control>`
- :ref:`Realm Management Extension <Realm_Management_Extension_label>`
- :ref:`Runtime Security Engine <rse>`
- :ref:`SCP Address Translation Unit Configuration <scp_atu_config>`
- :ref:`SCP - RSE Communication <scp_rse_communication>`


Supported Features
------------------

RD-V3-R1 platform software stack supports the following features.

- :ref:`Busybox Boot <Busybox_boot_label>`
- :ref:`Buildroot boot <Buildroot_boot_label>`
- :ref:`Linux Distribution Boot <distro_boot>`
- :ref:`Low power idle <low_power_idle_label>`
- :ref:`Collaborative processor performance control <cppc_test_label>`

Follow the links above for detailed information about the build and execute
steps for each of the supported features.


Obtaining FVP
-------------

The latest version of the RD-V3-R1 fixed virtual platform (FVP) can be
downloaded from the `Arm Ecosystem FVPs`_ page. On this page,

- Navigate to "Neoverse Infrastructure FVPs" section.
- Click on "Neoverse V3 r1 Reference Design FVP" link to obtain the
  list of available Neoverse RD-V3-R1 Reference Design FVPs.
- Select a FVP build under the section "Download RD-V3-R1" based on the
  host machine architecture.

  - For AArch64 host machine, click "Download Linux - Arm Host (DEV)" link.
  - For x86-64 host machine, click "Download Linux" link.

The RD-V3-R1 FVP executable is included in the downloaded installer and
named as "FVP_RD_V3_R1". Follow the instructions of the installer and
setup the FVP. The installer, by default, selects the home directory to install
the FVP. To opt for different directory than the one selected by the installer,
provide an absolute path to that directory when prompted for during the FVP
installation process.


Release Tags
------------

Table below lists the release tags for the RD-V3-R1 platform software
stack and the corresponding RD-V3-R1 FVP version that is recommended to
be used along with the listed release tag. The summary of the changes introduced
and tests validated in each release is listed in the release note, the link to
which is in the 'Release Tag' column in the table below.

+---------------------------------------------+------------------------------+
|                Release Tag                  |      RD-V3-R1 FVP Version    |
+=============================================+==============================+
|  :ref:`RD-INFRA-2025.02.04 <q424-1>`        |           11.27.51           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.12.20 <q424_label>`    |           11.27.51           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.09.30 <q324_label>`    |           11.27.25           |
+---------------------------------------------+------------------------------+


.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
