.. _RD_N2_Cfg2_readme_label:

RD-N2 Cfg2 Platform
===================

Overview
--------

RD (Reference Design) is a collection of resources to provide a representative
view of typical compute subsystems that can be designed and implemented using
specific generations of Arm IP. RD-N2 Cfg2 platform (which is a variant of the
the :ref:`RD-N2 <RD_N2_readme_label>` platform) is a quad-chip platform in
which four identical chips are connected through high speed CCG link. The CCG
link is enabled through CMN-700 Coherent Multichip Link (CML) feature. RD-N2
Cfg2 in particular has the following hardware configuration on each chip.

- 4xMP1 Neoverse N2 CPUs
- CMN-700 interconnect (mesh size 6x6)
- Multiple AXI expansion ports for I/O Coherent PCIe, Ethernet, offload
- Arm Cortex-M7 for System Control Processor (SCP) and
  Manageability Control Processor (MCP)

The components integrated into this stack are described in
:ref:`Software Stack <software_stack>` section.


Supported Features
------------------

RD-N2 Cfg2 platform software stack supports the following features.

- :ref:`Busybox Boot <Busybox_boot_label>`
- :ref:`Buildroot boot <Buildroot_boot_label>`
- :ref:`Linux Distribution Boot <distro_boot>`
- :ref:`Low power idle <low_power_idle_label>`
- :ref:`Collaborative processor performance control <cppc_test_label>`

Follow the links above for detailed information about the build and execute
steps for each of the supported features.


Obtaining FVP
-------------

The latest version of the RD-N2 Cfg2 fixed virtual platform (FVP) can be
downloaded from the `Arm Ecosystem FVPs`_ page. On this page,

- Navigate to "Neoverse Infrastructure FVPs" section.
- Click on "Neoverse N2 Reference Design FVP" link to obtain the
  list of available Neoverse RD-N2 Reference Design FVPs.
- Select a FVP build under the section "Download RD-N2" based on the host
  machine architecture.

  - For AArch64 host machine, click "Download Linux - Arm Host (DEV)" link.
  - For x86-64 host machine, click "Download Linux" link.

The RD-N2 Cfg2 FVP executable is included in the downloaded installer and
named as "FVP_RD_N2_Cfg2". Follow the instructions of the installer and
setup the FVP. The installer, by default, selects the home directory to install
the FVP. To opt for different directory than the one selected by the installer,
provide an absolute path to that directory when prompted for during the FVP
installation process.


Release Tags
------------

Table below lists the release tags for the RD-N2 Cfg2 platform software stack
and the corresponding RD-N2 Cfg2 FVP version that is recommended to be used
along with the listed release tag. The summary of the changes introduced and
tests validated in each release is listed in the release note, the link to
which is in the 'Release Tag' column in the table below.

+-------------------------------------------+------------------------------+
|     Release Tag                           |    RD-N2 Cfg2 FVP Version    |
+===========================================+==============================+
|  :ref:`RD-INFRA-2024.12.20 <q424_label>`  |           11.25.23           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.09.30 <q324_label>`  |           11.25.23           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.07.15 <q224_label>`  |           11.25.23           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.04.17 <q124_label>`  |           11.25.23           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.12.22 <q423_label>`  |           11.24.12           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.09.29 <q323_label>`  |           11.20.18           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.06.30 <q223_label>`  |           11.20.18           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.03.31 <q123_label>`  |           11.20.18           |
+-------------------------------------------+------------------------------+


.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
