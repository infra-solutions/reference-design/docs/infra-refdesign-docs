.. _RD_V3_Cfg1_readme_label:

RD-V3 Cfg1 Platform
===================

Overview
--------

RD (Reference Design) is a collection of resources to provide a representative
view of typical compute subsystems that can be designed and implemented using
specific generations of Arm IP. RD-V3-Cfg1 platform (a variant of the
:ref:`RD-V3 <RD_V3_readme_label>` platform) also supports the Realm
Management Extension (RME) and is based on the following hardware
configuration.

- 8xMP1 Neoverse Poseidon-V cores with Direct Connect and 2MB of
  dedicated, private L2 cache for each core.
- CMN-Cyprus interconnect with 3x3 mesh network.
- Multiple AXI expansion ports for I/O Coherent PCIe, Ethernet, offload
- Arm Cortex-M55 processor for Runtime Security Engine (RSE) to support Hardware
  Enforced Security (HES)
- Arm Cortex-M7 for System Control Processor (SCP) and
  Manageability Control Processor (MCP)
- Arm Cortex-M55 processor for Local Control Processor (LCP) for local power
  management of each Application Processor (AP)

The components integrated into this stack are described in
:ref:`Software Stack <software_stack>` section.


Platform Specific Details
-------------------------

The following documents provide specific details applicable for
RD-V3-Cfg1 Platform:

- :ref:`AP - RSE Attestation Service <ap_rse_attestation_service>`
- :ref:`Boot Flow <rdv3_boot_flow_sc>`
- :ref:`CMN Cyprus Driver Module <cmn_cyprus>`
- :ref:`Image Loading via MCUboot <mcuboot>`
- :ref:`Local Control Processor <lcp_label>`
- :ref:`NI-Tower Network-on-Chip Interconnect <ni_tower_label>`
- :ref:`NI-Tower System Control <ni_tower_system_control>`
- :ref:`Realm Management Extension <Realm_Management_Extension_label>`
- :ref:`Runtime Security Engine <rse>`
- :ref:`SCP Address Translation Unit Configuration <scp_atu_config>`
- :ref:`SCP - RSE Communication <scp_rse_communication>`


Supported Features
------------------

RD-V3-Cfg1 platform software stack supports the following features.

- :ref:`Busybox Boot <Busybox_boot_label>`
- :ref:`Buildroot boot <Buildroot_boot_label>`
- :ref:`Linux Distribution Boot <distro_boot>`
- :ref:`UEFI Secure Boot <UEFI_secureboot_label>`

- :ref:`Reliability, Availability, and Serviceability <ras>`

  - :ref:`Poseidon CPU/RAM Error Injection Tests <ras_tests>`
  - :ref:`CMN Cyprus Kernel First Handling <cmn_cyprus_kfh>`
  - :ref:`SCP Error Injection Utility <scp_einj_util>`

- :ref:`MPAM-resctrl <mpam_resctrl_label>`
- :ref:`Low power idle <low_power_idle_label>`
- :ref:`Collaborative processor performance control <cppc_test_label>`
- :ref:`AP Reset to BL31 <reset_to_bl31_label>`

Obtaining FVP
-------------

The latest version of the RD-V3-Cfg1 fixed virtual platform (FVP) can be
downloaded from the `Arm Ecosystem FVPs`_ page. On this page,

- Navigate to "Neoverse Infrastructure FVPs" section.
- Click on "Neoverse V3 Reference Design FVP" link to obtain the
  list of available Neoverse RD-V3-Cfg1 Reference Design FVPs.
- Select a FVP build under the section "Download RD-V3-Cfg1" based on the
  host machine architecture.

  - For AArch64 host machine, click "Download Linux - Arm Host (DEV)" link.
  - For x86-64 host machine, click "Download Linux" link.

The RD-V3-Cfg1 FVP executable is included in the downloaded installer and
named as "FVP_RD_V3_Cfg1". Follow the instructions of the installer and
setup the FVP. The installer, by default, selects the home directory to install
the FVP. To opt for different directory than the one selected by the installer,
provide an absolute path to that directory when prompted for during the FVP
installation process.


Release Tags
------------

Table below lists the release tags for the RD-V3-Cfg1 platform software stack
and the corresponding RD-V3-Cfg1 FVP version that is recommended to be used
along with the listed release tag. The summary of the changes introduced and
tests validated in each release is listed in the release note, the link to
which is in the 'Release Tag' column in the table below.

+---------------------------------------------+------------------------------+
|     Release Tag                             |  RD-V3-Cfg1 FVP Version      |
+=============================================+==============================+
|  :ref:`RD-INFRA-2025.02.04 <q424-1>`        |           11.27.51           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.12.20 <q424_label>`    |           11.27.51           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.09.30 <q324_label>`    |           11.27.25           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.07.15 <q224_label>`    |           11.26.15           |
+---------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.04.17 <q124_label>`    |           11.24.16           |
+---------------------------------------------+------------------------------+
| :ref:`RD-INFRA-2024.01.16 <q423_fmt_label>` |           11.24.16           |
+---------------------------------------------+------------------------------+
| :ref:`RD-INFRA-2023.09.28 <q323_fmt_label>` |           11.23.11           |
+---------------------------------------------+------------------------------+
| :ref:`RD-INFRA-2023.06.28 <q223_fmt_label>` |           11.22.16           |
+---------------------------------------------+------------------------------+
| :ref:`RD-INFRA-2023.03.29 <q123_fmt_label>` |           11.21.18           |
+---------------------------------------------+------------------------------+


.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
