.. _RD_N1_Edgex2_readme_label:

RD-N1 Edge X2 Platform
======================

Overview
--------

RD (Reference Design) is a collection of resources to provide a representative
view of typical compute subsystems that can be designed and implemented using
specific generations of Arm IP. RD-N1 Edge in particular is based on the
following hardware configuration.

- 8x `Neoverse N1 Cores <https://developer.arm.com/products/processors/neoverse/neoverse-n1>`_
  with DynamIQ Shared Unit (DSU)
- Dedicated L2 cache: 512KB per core
- Shared L3 cache: 2MB per cluster
- CMN-600 with CML option: 8MB System Level Cache and 16MB Snoop Filter
- DMC-620 with 2xRDIMM DDR4-3200
- Multiple AXI expansion ports for I/O Coherent PCIe, Ethernet, offload
- Arm Cortex-M7 for System Control Processor (SCP) and
  Manageability Control Processor (MCP)

RD-N1 Edge dual-chip is a platform configuration in which two RD-N1 Edge
platforms are connected through high speed CCIX link. The CCIX link is enabled
by CMN600's Coherent Multichip Link. Such platforms are called RD-N1 Edge-Dual
hereafter.

The components integrated into this stack are described in
:ref:`Software Stack <software_stack>` section.


Supported Features
------------------

RD-N1 Edge-Dual platform software stack supports the following features.

- :ref:`Busybox Boot <Busybox_boot_label>`.
- :ref:`Low power idle <low_power_idle_label>`

Follow the links above for detailed information about the build and execute
steps for each of the supported features.


Obtaining FVP
-------------

The latest version of the RD-N1 Edge-Dual fixed virtual platform (FVP) can be
downloaded from the `Arm Ecosystem FVPs`_ page. On this page, navigate to
"Neoverse Infrastructure FVPs" section to download the RD-N1 Edge platform
FVP installer.

Follow the instructions of the installer and setup the FVP. The installer, by
default, selects the home directory to install the FVP. To opt for different
directory than the one selected by the installer, provide an absolute path to
that directory when prompted for during the FVP installation process.


Release Tags
------------

Table below lists the release tags for the RD-N1 Edge dual-chip platform
software stack and the corresponding RD-N1 Edge dual-chip FVP version that is
recommended to be used along with the listed release tag. The summary of the
changes introduced and tests validated in each release is listed in the release
note, the link to which is in the 'Release Tag' column in the table below.

+-------------------------------------------+------------------------------+
|     Release Tag                           | RD-N1 Edge-Dual FVP Version  |
+===========================================+==============================+
|  :ref:`RD-INFRA-2024.12.20 <q424_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.09.30 <q324_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.07.15 <q224_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2024.04.17 <q124_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.12.22 <q423_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.09.29 <q323_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.06.30 <q223_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+
|  :ref:`RD-INFRA-2023.03.31 <q123_label>`  |           11.17.29           |
+-------------------------------------------+------------------------------+


.. _Arm Ecosystem FVPs: https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
