# -- Project Information --------------------------------------------------- #
project = "Neoverse Reference Design Platform Software"

copyright = "2020-2024, Arm Limited. All rights reserved"

# -- General Configuration ------------------------------------------------- #
numfig = True

# Syntax highlighting style
pygments_style = "sphinx"

# -- Options for HTML output ----------------------------------------------- #
html_theme = "sphinx_rtd_theme"

## Options for the "sphinx-rtd-theme" theme
html_theme_options = {
    "collapse_navigation": False,  # Can expand and collapse sidebar entries
}

## Don't show the "Built with Sphinx" footer
html_show_sphinx = False
