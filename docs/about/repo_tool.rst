*********************
Repo Tool & Manifests
*********************

`repo tool`_ is a wrapper around the git version control system to manage
multiple git repositories, it simplifies several git operations, in particular
the *sync* (download) of software sources to a local environment.

To achieve this, repo uses a manifest file, which is a collection of
repositories with some attributes to specify for example the revision of a
particular repository or the local path that repository will have once
downloaded to our local environment.

Manifest File in Detail
=======================

A manifest file is written in Extensible Markup Language (XML), for a full list
of supported elements and attributes please see `repo manifest format`_.

Take the excerpt of an existing manifest file in Neoverse reference design
gitlab repository to understand how manifests are defined.

.. code-block:: xml
    :linenos:

    <manifest>
      <remote fetch="https://git.gitlab.arm.com/infra-solutions/reference-design/" name="arm"/>
      <remote fetch="https://github.com/" name="github"/>

      <project remote="arm" name="platsw/scp-firmware" path="scp" revision="refs/tags/RD-INFRA-2024.12.20"/>
    </manifest>

| Line #2, the element ``remote`` defines two attributes, ``fetch`` and
  ``name``.
|   - **fetch** is the base url of a git repository.
|   - **name** is an alias for that repository.

| Line #5, the element ``project`` also defines the attributes ``name``,
  ``path`` and ``revision``.
|   - **name** is the name of the project in the git repository.
|   - **path** is the local path where the project will be cloned to, relative
        to where the manifest is initialised.
|   - **revision** is the git revision to clone, this can be a *branch*, a *tag*
        or a *commit sha id*.

A remote can have multiple repositories, the link from a remote to a project is
given by the alias set in line #2 and used in line #5 as ``remote="arm"``.

The ``revision`` attribute when set to *tag* or a *commit sha id* is what
makes the software stack being reproducible because it will fetch the same
software sources consistently. More details provided in the section
:ref:`Pinned vs Non-Pinned <pinned-vs-nonpinned>`.

Translating the syntax of a manifest file to git, the underlying command
of line #5 to run would be:

.. code-block:: shell

    git clone --branch refs/tags/RD-INFRA-2024.12.20 https://git.gitlab.arm.com/infra-solutions/reference-design/platsw/scp-firmware ./scp

.. _pinned-vs-nonpinned:

Manifest (Pinned vs Non-Pinned)
===============================

Now that the user knows how a manifest file is defined, let's stablish the
concept of *Pinned* and *Non-Pinned* manifests.

When we release a software stack, we test the integration of all the components
and validate that they work together to achieve the goal of providing users
with a usable code base, thus we tag the manifest and the components to have a
reference of this code base validity. This is the *Pinned* manifest and the
file name convention is ``pinned-<platform>.xml``.

But as software evolves, new features and/or security fixes will be made
available in the participating components of the software stack, therefore we
provide a manifest where the revision of the components is set to a branch that
is updated more frequently with their upstream counterparts. This is the
*Non-Pinned* manifest, and the file name simply is the platform name, i.e.:
``<platform>.xml``.

.. _repo tool: https://source.android.com/setup/develop/repo
.. _repo manifest format: https://gerrit.googlesource.com/git-repo/+/main/docs/manifest-format.md