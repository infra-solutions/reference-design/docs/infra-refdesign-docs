****************
Reference Design
****************

A Reference Design (RD) is a collection of resources to provide a
representative view of typical compute subsystems that can be designed and
implemented using specific generations of Arm IP.

Specifically, Neoverse reference designs provide resources with best practices
on how to integrate a Neoverse compute subsystem within a larger SoC. These
compute subsystems are targeted at addressing requirements for applications in
the cloud-to-edge infrastructure markets.

Neoverse products are categorized as follows:

- Neoverse V-Series: Maximum Performance
- Neoverse N-Series: Scale Out Performance
- Neoverse E-Series: Efficient Throughput

Refer to `Arm Neoverse`_ to learn more about the intended use cases for this
products.

The Neoverse reference designs are also available as fixed virtual platform
models and when used with a software stack, they provide a way to explore
the features available in the reference design compute subsystem through
software.

We provide a companion software stack for each of the Neoverse reference
designs and the source code can be found in Arm's Gitlab repository, located
at https://gitlab.arm.com/infra-solutions/reference-design

This documentation lists the available reference designs, the features each
support and how to interact with them.

.. note::

   A reference design is also referred to as platform.

Table below provides the user an overview of Neoverse reference designs, and
their current status.

.. include:: ../shared/platforms_overview.rst

Visit the :doc:`Getting Started </user_guides/getting_started>` chapter to
learn how to setup an host machine to download, compile and execute a platform
feature (i.e.: boot an OS), on a fixed virtual platform.

For questions about the Neoverse Reference Design platform software stack, write
to support@arm.com.


.. _Arm Neoverse: https://www.arm.com/products/silicon-ip-cpu/neoverse