.. _troubleshooting:

***************
Troubleshooting
***************

The documentation for Neoverse reference design platform software typically
suffices in most cases. But there could be certain host development machine
dependencies that could cause failures either during build or execution
stages. This page provides solutions for known issues that could affect the
use of the platform software stack.

.. _repo-tool-python-version:

Error while using repo command
==============================

The *repo init* or *repo sync* command fails with the below listed error
message.

.. code-block:: shell

  File "<path-to-workspace>/.repo/repo/main.py", line 79
  file=sys.stderr)
      ^
  SyntaxError: invalid syntax


The typical reason for this failure could be that the default version of python
on the development machine is not python3.6. To resolve this issue, install the
latest version of python, if not already installed on the development machine
and invoke the repo command from */usr/bin/* with *python3* as listed below.

.. code-block:: shell

  python3 /usr/bin/repo init -u https://git.gitlab.arm.com/infra-solutions/reference-design/infra-refdesign-manifests.git -m pinned-rdv1.xml -b refs/tags/RD-INFRA-2021.02.24
  python3 /usr/bin/repo sync -c -j $(nproc) --fetch-submodules --force-sync --no-clone-bundle

On systems with python version less than 3.6, there could be further failures as
listed below.

.. code-block:: shell

  Traceback (most recent call last):
    File "<path-to-workspace>/.repo/repo/main.py", line 42, in <module>
      from git_config import RepoConfig
    File "<path-to-workspace>/.repo/repo/git_config.py", line 774
      self._Set(f'superproject.{key}', value)
                                    ^
  SyntaxError: invalid syntax

If *python3* version cannot be updated using the package manager, use the
following commands to build and install *python3.7.2* from the source.

.. code-block:: shell

  sudo apt update
  sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget libsqlite3-dev python-openssl bzip2
  cd /tmp
  wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tar.xz
  tar -xf Python-3.7.2.tar.xz
  cd Python-3.7.2
  ./configure
  make -j
  sudo make altinstall

This will install install *python3.7* in */usr/local/bin/* path and the *repo*
command can be invoked using this version.

.. code-block:: shell

  /usr/local/bin/python3.7 /usr/bin/repo init -u https://git.gitlab.arm.com/infra-solutions/reference-design/infra-refdesign-manifests.git -m pinned-rdv1.xml -b refs/tags/RD-INFRA-2021.02.24
  /usr/local/bin/python3.7 /usr/bin/repo sync -c -j $(nproc) --fetch-submodules --force-sync --no-clone-bundle


Builds do not progress to completion
====================================

During the build of the platform software stack, components such as grub
download additional code from remote repositories using the git port (or the
git protocol). Development machines on which git port is blocked, the build
does not progress to completion, waiting for the additional code to be
downloaded. This typically is observed when setting up a new platform software
workspace.

As a workaround, use https instead of git protocol for cloning required git
submodules of the various components in the software stack. A patch, as an
example of this change in the grub component, is listed below.

.. code-block:: shell

  diff --git a/bootstrap b/bootstrap
  index 5b08e7e2d..031784582 100755
  --- a/bootstrap
  +++ b/bootstrap
  @@ -47,7 +47,7 @@ PERL="${PERL-perl}"

   me=$0

  -default_gnulib_url=git://git.sv.gnu.org/gnulib
  +default_gnulib_url=https://git.savannah.gnu.org/git/gnulib.git

   usage() {
     cat <<EOF


FVP closes abruptly
===================

Tests such as distro installation take few hours to complete on Neoverse
Reference Design platform FVPs. If the model quits abruptly during its
execution without any particular error message displayed in the model
launch window, the host machine's memory requirements has to be
rechecked. This issue is typically seen if the host machine has a
configuration below that of on the one listed at
:ref:`recommended configuration <host-requirements>`.


Repo sync fails when downloading linux repo


If the download of the linux repo fails during the execution of the *repo sync*
command, rerun the repo init command with the ``--depth=1`` parameter appended
to the repo init command. The parameter ``--depth=1`` reduces the commit
history that is downloaded and can address this failure in downloading linux
repo.


Error: "/usr/bin/env: ‘python’: No such file or directory"
==========================================================

repo init could fail if it can't find a compatible reference to python. Please
make sure you have the required version of python as mentioned in :ref:`install
repo <python-version-required>` prerequisites section.

If the error still persists, check if */usr/bin* has a binary named python. If
you find the binary name to be *python3* (or any *python3.x* for that matter)
and /usr/bin/python is not found, then create a softlink to work around this
issue as shown below:

.. code-block:: shell

  sudo ln -s /usr/bin/python3 /usr/bin/python
