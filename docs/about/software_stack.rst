.. _software_stack:

**************
Software Stack
**************

The Neoverse software stack integrates multiple software components to provide
a reference implementation of a software solution that can be used demonstrate
various capabilities of the respective platform.

A typical software stack is illustrated in :numref:`sw-stack`.

.. _sw-stack:
.. figure:: /resources/sw_stack/sw_stack.png

   High-level software illustration of a Neoverse Reference Design.

Some reference designs support the Realm Management Extension (RME), and their
software stack is ilustrated in :numref:`sw-stack-rme`.

.. _sw-stack-rme:
.. figure:: /resources/sw_stack/sw_stack_rme.png

   High-level software illustration of a Neoverse Reference Design with RME.

The following sections list the various software components that are included
the Neoverse software stack.


MSCP Firmware
=============

Neoverse reference design platforms include a System Control Processor (SCP)
sub-system and a Manageability Control Processor (MCP) sub-system. The SCP
sub-system is tasked with the management of system clocks, power control,
configuring the system interconnect, memory controllers, PCIe controllers and
many other functionalities. The MCP sub-system is tasked with the management of
communications with an external Baseboard Management Controller (BMC). The
firmware executed by the SCP and MCP processors is sourced from the
`SCP-firmware`_ open-source project.


Trusted Firmware
================

`Trusted Firmware-A`_ (TF-A) software component provides a reference open-source
implementation of a secure monitor executing at EL3 exception level. It
implements various Arm interface standards including the Power State
Coordination Interface (PSCI), Trusted Board Boot Requirements (TBBR), SMC
Calling Convention, System Control and Management Interface and others. The
trusted firmware executes in various stages -  Boot Loader stage 1 (BL1) AP
Trusted ROM, Boot Loader stage 2 (BL2) Trusted Boot Firmware, Boot Loader stage
3-1 (BL3-1) EL3 Runtime Firmware, Boot Loader stage 3-2 (BL3-2) Secure-EL1
Payload (optional) and Boot Loader stage 3-3 (BL3-3) Non-trusted Firmware.


EDK2
====

EFI Development Kit 2 (edk2) is a firmware development environment for the
UEFI and PI specifications. UEFI is a specification that defines an interface
between the firmware and an Operating System (OS). UEFI defines the firmware
interfaces and boot services that are required for booting a standards-based OS.
UEFI also defines run-time services, for example, time, variable that an OS can
invoke at runtime. The reference design platform stack integrates both the
`edk2`_ and `edk2-platforms`_ open-source projects to support an implementation
of EFI API for the platform.


Linux Kernel
============

`Linux kernel`_ is used as the host operating system kernel for the reference
design platforms. ACPI tables are used to describe the platform to the linux
kernel. All the capabilities of the linux kernel are used to demonstrate the
various functionalities of the platform software including power management,
device assignment, RAS and many others.


Other software components
=========================

The platform software stack uses the following additional software components to
provide an integrated software solution for the Neoverse reference design
platforms.

- `Trusted Firmware Test Framework`_
- `Grub`_
- `Busybox`_
- `Buildroot`_
- `ACPICA`_
- `Mbed TLS`_
- `ACS`_
- `EFI Tools`_
- `UEFI SCT`_


.. _SCP-firmware: https://github.com/ARM-software/SCP-firmware
.. _Trusted Firmware-A: https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
.. _edk2: https://github.com/tianocore/edk2
.. _edk2-platforms: https://github.com/tianocore/edk2-platforms
.. _Linux kernel: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/
.. _Trusted Firmware Test Framework: https://git.trustedfirmware.org/TF-A/tf-a-tests.git/about/
.. _Grub: https://git.savannah.gnu.org/cgit/grub.git
.. _Busybox: https://github.com/mirror/busybox
.. _Buildroot: https://github.com/buildroot/buildroot
.. _ACPICA: https://github.com/acpica/acpica
.. _Mbed TLS: https://github.com/ARMmbed/mbedtls
.. _ACS: https://github.com/ARM-software/arm-enterprise-acs
.. _EFI Tools: https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools.git
.. _UEFI SCT: https://github.com/tianocore/edk2-test/tree/master/uefi-sct
