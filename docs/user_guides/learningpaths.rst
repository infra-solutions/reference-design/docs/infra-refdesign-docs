**************
Learning Paths
**************

Arm Learning Paths, available on the Developer Hub, are community-created
how-to articles about software development for the Arm architecture. They offer
detailed tutorials designed to help developers create quality Arm software
faster. A Learning Path is a concise tutorial with detailed steps on how to
complete a specific task.

The Learning Paths are segmented into categories, each covering different kinds
of computer hardware. These categories include Smartphones and Mobile, Laptops
and Desktops, Servers and Cloud Computing, Embedded Systems, and
Microcontrollers. Neoverse Reference Designs are a part of servers and cloud
computing.

The sections below list the available learning paths that are applicable to
Neoverse Reference Designs.

Get started with the Neoverse Reference Design software stack
=============================================================

Follow the link below for this tutorial:

https://learn.arm.com/learning-paths/servers-and-cloud-computing/refinfra-quick-start/

Debug Neoverse N2 Reference Design with Arm Development Studio
==============================================================

Follow the link below for this tutorial:

https://learn.arm.com/learning-paths/servers-and-cloud-computing/refinfra-debug/
