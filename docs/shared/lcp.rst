:orphan:

.. _lcp_label:

*****************************
Local Control Processor (LCP)
*****************************

Introduction
============

The local control processor (LCP) is introduced in Arm Neoverse Reference
Design platforms with System Architecture version 6 in order to support
scalable power architecture, which enables per application processor ‘Dynamic
Voltage Frequency Scaling’ (DVFS) to increase the responsiveness of the system
to changes in the workload running on that application processor. Supporting
individual core DVFS efficiently in a system requires additional power control
functionality. DVFS control under a separate LCP is done to avoid overloading
the system control processor (SCP) for a system with higher core count.

Reference design platforms with LCP support a scalable power control solution
that is enabled by having one an LCP for each application core (AP) in the
system under the management of the System Control Processor (SCP).

LCP Hardware Configuration
==========================

* Cortex-M55 CPU.
* 64KB instruction (ITCM) and 32KB data (DTCM) memory.


Functionality
=============

* Per-core DVFS, Thermal management, Max Power Mitigation Mechanism (MPMM) and
  Power limit enforcement.
* Sensor data collection: LCP uses System Monitoring and Control Framework
  (SMCF) to collect data from thermal sensors and activity monitors and sends
  the data to SCP for further processing.


Power Domain
============

LCP is in AON power domain and SYSTOP reset domain.


Boot Sequence
=============

1. The Runtime Security Engine (RSE) authenticates and loads the LCP RAM
   firmware to the LCP instruction tightly coupled memory (ITCM).

   References:

   a. RSE getting the LCP images base address and size from cluster utility
      (refer:
      ``<workspace>/tf-m/platform/ext/target/arm/rse/rdv3/bl2/flash_map_bl2.c:
      boot_get_image_exec_ram_info()``
      ).

   b. RSE loading the LCP binary from cluster utility (refer:
      ``<workspace>/tf-m/platform/ext/target/arm/rse/rdv3/bl2/boot_hal_bl2.c:
      boot_platform_pre_load(), boot_platform_post_load()``
      ).

2. LCP comes out of reset when SCP turns on the SYSTOP power domain, but the
   boot pauses in the CPUWAIT state.

3. SCP releases the LCP from CPUWAIT state by programming the CPUWAIT register
   in system control block (SCB) after the inteconnect setup is done.
   (refer:
   ``<workspace>/scp/product/rdv3/module/platform_system/src/mod_platform_system.c:
   release_lcp()``
   ).


Peripherals
===========

DVFS Handler
------------

The DVFS (Dynamic Voltage and Frequency Scaling) Handler allows a software
agent to abstract the details of programming the DVFS settings for components'
clock frequency and voltage level. The interface allows software to program in
set of registers a frequency and voltage level, making the hardware responsible
for managing the lower level details of applying these settings to the required
components. This removes the need for software to support SoC specific and
complex programming sequences and reduces the overall software load.

References:

1. Module: ``<workspace>/scp/module/dvfs_handler/src/mod_dvfs_handler.c``
2. Example configuration: ``<workspace>/scp/product/rdv3/lcp_ramfw/config_dvfs_handler.c``


Message Handling Unit
---------------------

Message handling unit (MHU) v3 is supported in LCP to enable communication
between LCP-AP and LCP-SCP.

References:

1. Module: ``<workspace>/scp/module/mhu3/src/mod_mhu3.c``
2. Example configuration: ``<workspace>/scp/product/rdv3/lcp_ramfw/config_mhu3.c``
