:orphan:

***********************
Multichip Memory Map(s)
***********************

.. _rd_multichip_memory_map:

Introduction
============

In a multichip chip platform. One of the chip is identified as
primary chip and the other three as secondary chip. Each chip is allocated a
different address region in the system memory map. The CMN interconnect on each
chip is programmed with its own RN-SAM at boot time.

- The PCIe ECAM/MMIO regions are split across multiple chips by configuring the
  CMN RN-SAM.
- The DRAM memory regions are split across multiple chips by configuring the
  CMN RN-SAM.


Reference Memory Map
====================

In a multichip platform, the per chip address size for peripherals is 64GB,
and the base address for peripherals residing in each chip are:

+----------------+------------------+-----------+------------------------------+
|   Start Addr   |     End Addr     |   Size    |            Chiplet           |
+================+==================+===========+==============================+
| 0x00_0000_0000 | 0x0F_FFFF_FFFF   | 64 GB     | Chip0 Onchip Peripheral      |
|                |                  |           | space base address           |
+----------------+------------------+-----------+------------------------------+
| 0x10_0000_0000 | 0x1F_FFFF_FFFF   | 64 GB     | Chip1 Onchip Peripheral      |
|                |                  |           | space base address           |
+----------------+------------------+-----------+------------------------------+
| 0x20_0000_0000 | 0x2F_FFFF_FFFF   | 64 GB     | Chip2 Onchip Peripheral      |
|                |                  |           | space base address           |
+----------------+------------------+-----------+------------------------------+
| 0x30_0000_0000 | 0x3F_FFFF_FFFF   | 64 GB     | Chip3 Onchip Peripheral      |
|                |                  |           | space base address           |
+----------------+------------------+-----------+------------------------------+
| 0x40_0000_0000 | 0xFFFF_FFFF_FFFF | 255.75 TB | Can be mapped to any chiplet |
+----------------+------------------+-----------+------------------------------+


Interrupt Map
=============

On the shared peripheral interrupt (SPI), each chip are assigned with 480 SPIs.
The SPI mappings are:

+---------+----------------+
| Chiplet | SPI ID mapping |
+=========+================+
| Chip0   | 32 - 511       |
+---------+----------------+
| Chip1   | 512 - 991      |
+---------+----------------+
| Chip2   | 4096 - 4575    |
+---------+----------------+
| Chip3   | 4576 - 5055    |
+---------+----------------+


DRAM Map
========

Each chip of a multichip FVP is equiped with two DRAM blocks, a 2GB and a
6GB. The 6GB DRAM of the secondary chips are kept outside of the 64GB address
per chip. The DRAM mappings are:

RD-V3-Cfg2
----------

+---------+------+------------------+------------------+
| Chiplet | Size | Start Addr       | End Addr         |
+=========+======+==================+==================+
| Chip0   | 2GB  | 0x0000_8000_0000 | 0x0000_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip0   | 6GB  | 0x0080_8000_0000 | 0x0081_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 2GB  | 0x0010_8000_0000 | 0x0010_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 6GB  | 0x2080_8000_0000 | 0x2081_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip2   | 2GB  | 0x0020_8000_0000 | 0x0020_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip2   | 6GB  | 0x3080_8000_0000 | 0x3081_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip3   | 2GB  | 0x0030_8000_0000 | 0x0030_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip3   | 6GB  | 0x4080_8000_0000 | 0x4081_FFFF_FFFF |
+---------+------+------------------+------------------+

RD-V3-R1
--------

+---------+------+------------------+------------------+
| Chiplet | Size | Start Addr       | End Addr         |
+=========+======+==================+==================+
| Chip0   | 2GB  | 0x0000_8000_0000 | 0x0000_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip0   | 6GB  | 0x0200_8000_0000 | 0x0201_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 2GB  | 0x0010_8000_0000 | 0x0010_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 6GB  | 0x0300_8000_0000 | 0x0301_FFFF_FFFF |
+---------+------+------------------+------------------+

RD-V3-R1-Cfg1
-------------

+---------+------+------------------+------------------+
| Chiplet | Size | Start Addr       | End Addr         |
+=========+======+==================+==================+
| Chip0   | 2GB  | 0x0000_8000_0000 | 0x0000_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip0   | 6GB  | 0x0200_8000_0000 | 0x0201_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 2GB  | 0x0010_8000_0000 | 0x0010_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip1   | 6GB  | 0x0300_8000_0000 | 0x0301_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip2   | 2GB  | 0x0020_8000_0000 | 0x0020_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip2   | 6GB  | 0x0400_8000_0000 | 0x0401_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip3   | 2GB  | 0x0030_8000_0000 | 0x0030_FFFF_FFFF |
+---------+------+------------------+------------------+
| Chip3   | 6GB  | 0x0500_8000_0000 | 0x0501_FFFF_FFFF |
+---------+------+------------------+------------------+
