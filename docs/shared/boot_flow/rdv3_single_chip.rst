:orphan:

.. _rdv3_boot_flow_sc:

*********************************************
Boot Flow for RD-V3 Single Chip Variants
*********************************************

Overview
========

This section describes the software boot process for the following platforms:

* :ref:`RD_V3_readme_label`
* :ref:`RD_V3_Cfg1_readme_label`

A simplified boot flow diagram is shown below.

.. image:: /resources/boot_flow/rdf_single_chip.png

Subsystem Roles
===============

Runtime Security Engine
-----------------------

On platform reset, RSE is released out of reset and SCP and MCP are held in
reset state. RSE begin executing the TF-M's BL1_1 from RSE ROM and provision
the BL1_2 image into the One Time Programmable flash (OTP) and transfers the
execution to the BL1_2 stage. More details on the provisioning can be found in
the TF-M's `RSE provisioning page`_. BL1_2 authenticates and loads the TF-M's
BL2 (MCUBoot) stage which is responsible for authenticated loading of the next
stage images as well as images of the other components. More details on BL2 can
be found in :ref:`Image Loading Using MCUBoot <mcuboot>`
page. BL2 stage is also responsible for :ref:`setting up the SCP's ATU
<scp_atu_config>` and releasing SCP and MCP out of reset.


System Control Processor
------------------------

SCP is responsible for managing the system power and setting up of the
interconnect. More details on the interconnect setup can be found in
:ref:`CMN Cyprus Driver Module <cmn_cyprus>` page. SCP is also
responsible for releasing the LCPs out of reset after RSE loads the LCP
firmware into ITCMs. RSE has to wait for the SCP to turn on the SYSTOP power
domain  before loading the LCP images. To maintain this synchronization, SCP
and RSE communicates messages through MHUv3. More details on this can be found
in :ref:`SCP-RSE Communication <scp_rse_communication>` page.


Local Control Processor
-----------------------

LCP is responsible for managing per Application Processor's power. The boot
sequence of LCP is summarized in :ref:`Local Control Processor <lcp_label>`
page.


Application Processor
---------------------

Application Processor (AP) is responsible for hosting the user's software stack.
Trusted Firmware (TF-A) is executed at boot time to setup the root monitor,
which executes at root EL3 mode. User's software stack includes a host
hypervisor hosted at Non-Secure EL2 mode, a Realm Management Monitor (TF-RMM)
deployed at Realm EL2 mode, guest virtual machine spawned in Realm/Non-secure
EL1 and host/guest userspace applications running at Realm/Non-secure EL0.

In RD-V3 platform, TF-M BL2 (MCUBoot) stage authenticates and loads TF-A
BL1 image binary into AP SRAM. When SCP releases AP out of reset, AP starts
with TF-A BL1 stage, following which TF-A BL1 authenticates and loads TF-A BL2
and passes the control to it. Furthermore TF-A BL2 follows same process to load
the next stage TF-A BL31. TF-A BL31 is the runtime root monitor which sets up
the TF-RMM interface and triggers the RMM initialization. TF-RMM communicates
with TF-A BL31 on few occasions during its init. Once TF-RMM returns control
back, TF-A BL31 hands off the control to Non-Secure BL33 image which is UEFI
in current implementation which then take care of host OS/hypervisor boot.
At each stage of authenticated loading of image, measurements of the loaded
image is computed and extended to RSE, that is later used to produce delegated
attestation key and token required by TF-RMM. More detailed info can be found
here in :ref:`AP-RSE Attestation Service <ap_rse_attestation_service>`.

.. _RSE provisioning page: https://tf-m-user-guide.trustedfirmware.org/platform/arm/rse/rse_provisioning.html
