:orphan:

.. _rdv3_boot_flow_mc:

********************************************
Boot Flow for RD-V3 Multi Chip Variants
********************************************

Overview
========

This section describes the software boot process for the following platforms:

* :ref:`RD_V3_Cfg2_readme_label`

A simplified boot flow diagram is shown below.

.. image:: /resources/boot_flow/rdf_multi_chip.png


Subsystem Roles
===============

Runtime Security Engine
-----------------------

On platform reset, RSE on each chip are released out of reset and SCP and MCP
are held in reset state. RSE on each chip begin executing the TF-M's BL1_1 from
RSE ROM and provision the BL1_2 image into the One Time Programmable flash
(OTP) of the respective chip and transfers the execution to the BL1_2 stage.
More details on the provisioning can be found in the TF-M's
`RSE provisioning page`_. BL1_2 authenticates and loads the TF-M's BL2
(MCUBoot) stage which is responsible for authenticated loading of the next
stage images as well as images of the other components in the respective chip.
More details on BL2 can be found in :ref:`Image Loading Using MCUBoot
<mcuboot>` page. BL2 stage is also responsible for
:ref:`setting up the SCP's ATU <scp_atu_config>`, :ref:`NI-Tower
<ni_tower_label>` instances and releasing SCP and MCP out of reset.


System Control Processor
------------------------

SCP is responsible for managing the per chip system power, setting up of the
interconnect and configuring the interconnect for cross chip communication.
More details on the interconnect setup can be found in :ref:`CMN Cyprus Driver
Module <cmn_cyprus>` and in - :ref:`CMN Cyprus Multichip Configuration
<cmn_cyprus_multichip_config>` page. SCP is also responsible for releasing
the LCPs out of reset after RSE loads the LCP firmware into ITCMs. RSE has to
wait for the SCP to turn on the SYSTOP power domain before loading the LCP
images. To maintain this synchronization, SCP and RSE communicates messages
through MHUv3. More details on this can be found in
:ref:`SCP-RSE Communication <scp_rse_communication>` page. Only the SCP
on the primary chip releases the primary core out of reset. At this point, SCP
on the secondary chips will wait for the SCMI messages.


Local Control Processor
-----------------------

LCP is responsible for managing per Application Processor's power. The boot
sequence of LCP is summarized in :ref:`Local Control Processor <lcp_label>`
page.


.. _RSE provisioning page: https://tf-m-user-guide.trustedfirmware.org/platform/arm/rse/rse_provisioning.html
