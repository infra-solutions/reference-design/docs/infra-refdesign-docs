:orphan:

.. _scp_rse_communication:

***********************
SCP - RSE Communication
***********************

Introduction
============

This document gives an overview of the SCP to RSE communication stack.

.. code-block:: text

   +----------------+        +-----------+        +-----------+
   |                |        |           |        |           |
   |  SCP PLATFORM  |<------>| TRANSPORT |<------>|   MHUv3   |
   |                |        |           |        |           |
   +----------------+        +-----------+        +-----------+

Overview
========

The RD-V3 platform SCP communicates with the RSE via MHU doorbell during
the boot sequence.

In SCP firmware, the scp_platform module binds to the transport module. The
transport HAL module is bound to the MHUv3 driver.

The SCP transport module configuration info can be found in the following file
in the software stack:
``<workspace>/scp/product/neoverse-rd/rdv3/scp_ramfw/config_transport.c``

- The transport channel ``SCP_PLATFORM_TRANSPORT`` is used by the scp platform
  module during the initial boot to perform handshake with the RSE firmware via
  MHU doorbell events.

The SCP MHUv3 driver configuration info can found in the following file in the
software stack:
``<workspace>/scp/product/neoverse-rd/rdv3/scp_ramfw/config_mhu3.c``

- In this config data, the element ``SCP2RSS_S_MHU_DBCH`` is used to
  communicate with the RSE.

- The SCP Platform transport channel uses the doorbell channel 1, flag 0 to
  perform handshake with the RSE firmware.

SCP-RSE Handshake
=================

The RSE firmware BL2 stage loads the MCP, SCP and LCP firmware images. The LCP
firmware must be loaded to the LCP ITCM (Instruction - Tightly Coupled Memory)
which is accessed via the cluster utility region in the AP memory space through
the RSE ATU. But, in order to access the LCP ITCM, the RSE must wait for the
``SYSTOP`` power domain to be in ON state. So, the RSE firmware loads the SCP
firmware image, releases the SCP and waits for the SCP to turn on the ``SYSTOP``
power domain.

After the SYSTOP is turned on and the platform is initialized, the SCP platform
system module triggers the SCP to RSE MHUv3 doorbell and waits for a doorbell
back from RSE which would indicate that the RSE has successfully loaded the LCP
firmware images to the LCPs.

The RSE firmware on receiving the doorbell from SCP, loads the LCP firmware to
the LCP ITCMs and triggers RSE to SCP MHUv3 doorbell. The SCP platform system
module receives the doorbell event, configures the LCP UART and releases all the
LCPs from reset.

The LCP initialization code in the SCP platform module can be found in the
function ``platform_setup_lcp()`` in the following file in the software stack:
``<workspace>/scp/product/neoverse-rd/rdv3/module/scp_platform/src/
platform_lcp.c``
