:orphan:

.. _ni_tower_label:

*************************************
NI-Tower Network-on-Chip Interconnect
*************************************

Introduction
============

NI-Tower Network-on-Chip Interconnect is a configurable system-level
interconnect that is compliant with AMBA protocols. It provides various
configuration options for upstream/downstream interfaces, xSNIs and xMNIs.
For more details, see NI-Tower NCI TRM.

Overview
========

In addition, NI-Tower has two address map type; static and programmable
address map (PSAM). Also, NI-Tower implements optional Access Protection Units
(APU) that the software can use to implement memory protection and isolation
from devices with different integrity levels.

RSE provides with drivers to program the PSAMs and APUs of the system control
NI-Tower and APUs of the peripheral block NI-Tower. This enables having robust
flexibility for modifying address map and memory protection based on the
software implementation of that platform.

Discovery
=========

Discovery is an algorithm that is used to determine the structure of the
NI-Tower configuration. This helps RSE identify the address offsets for NI-Tower
domains, components and sub-features without the knowledge of any configuration
details.

Discovery flow
--------------

Following is the hierarchy of NI-Tower configuration nodes:

1. Global configuration node
2. Voltage domain
3. Power domain
4. Clock domain
5. Component
6. Subfeature

Component type includes all PMUs, xSNIs and xMNIs. Subfeatures include APU, PSAM
etc.

Following the common header structure for all domain level nodes:

.. code-block::

    struct ni_tower_domain_cfg_hdr {
        __IM uint32_t node_type;
        __IM uint32_t child_node_info;
        __IM uint32_t x_pointers[];
    };

Following the common header structure for all component nodes (except FMUs):

.. code-block::

    struct ni_tower_component_cfg_hdr {
        __IM  uint32_t node_type;
        const uint32_t reserved_0[4];
        __IM  uint32_t interface_id_0_3;
        const uint32_t reserved_1[58];
        __IM  uint32_t num_subfeatures;
        const uint32_t reserved_2;
        struct {
            __IM uint32_t type;
            __IM uint32_t pointer;
        } subfeature[];
    };

For more details, check Section 15.2 of NI-Tower TRM.

Programmable Address Map
========================

A programmable address map provides flexibility to configure or change address
regions and targets after RTL configuration and provides more options than a
static address map.

For more details, check Section 11.4 of NI-Tower TRM.

Access Protection Unit
======================

The APU supports freedom with mixed memory protection requirements and preserves
the integrity of critical memory and peripherals.

For more details, check Section 9.1 of NI-Tower TRM.
