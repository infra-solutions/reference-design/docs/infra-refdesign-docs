:orphan:

.. _platform_manifests:

Platform Manifest Names
=======================

The repo tool uses a manifest file in order to download the source code. The
manifest file lists the location of the various repositories and the branches
in those repositories from which the code has to be downloaded. Each of the
Neoverse RD platform has a unique manifest that is supplied to the repo tool to
download the corresponding platform software. The following table lists the
platform names and the corresponding manifest file names.

+--------------------------+----------------------------+
| Reference Platform       | Manifest File Name         |
+==========================+============================+
| RD-V3-R1                 | pinned-rdv3r1.xml          |
+--------------------------+----------------------------+
| RD-V3-R1-Cfg1            | pinned-rdv3r1cfg1.xml      |
+--------------------------+----------------------------+
| RD-V3                    | pinned-rdv3.xml            |
+--------------------------+----------------------------+
| RD-V3-Cfg1               | pinned-rdv3cfg1.xml        |
+--------------------------+----------------------------+
| RD-V3-Cfg2               | pinned-rdv3cfg2.xml        |
+--------------------------+----------------------------+
| RD-V2                    | pinned-rdv2.xml            |
+--------------------------+----------------------------+
| RD-N2                    | pinned-rdn2.xml            |
+--------------------------+----------------------------+
| RD-N2-Cfg1               | pinned-rdn2cfg1.xml        |
+--------------------------+----------------------------+
| RD-N2-Cfg2               | pinned-rdn2cfg2.xml        |
+--------------------------+----------------------------+
| RD-N2-Cfg3               | pinned-rdn2cfg3.xml        |
+--------------------------+----------------------------+
| RD-V1 (Single Chip)      | pinned-rdv1.xml            |
+--------------------------+----------------------------+
| RD-V1 (Quad Chip)        | pinned-rdv1mc.xml          |
+--------------------------+----------------------------+
| RD-N1-Edge (Single Chip) | pinned-rdn1edge.xml        |
+--------------------------+----------------------------+
| RD-N1-Edge (Dual Chip)   | pinned-rdn1edgex2.xml      |
+--------------------------+----------------------------+
| SGI-575                  | pinned-sgi575.xml          |
+--------------------------+----------------------------+
