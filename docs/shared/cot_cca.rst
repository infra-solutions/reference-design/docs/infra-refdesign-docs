:orphan:

.. _CCA_CoT_label:

***************************************************************
Chain of Trust (CoT) for Confidential Compute Arcitecture (CCA)
***************************************************************

Introduction
============

RD-V3 platform has the support for trusted board boot (TBB), which prevents
malicious firmware from running on the platform by authenticating all firmware
images up to and including the BL33 bootloader (UEFI firmware).

The TBB Chain of Trust (CoT) starts with a set of implicitly trusted components.
On the Arm development platforms, these components are:

* An SHA-256 hash of the Root of Trust Public Key (ROTPK). It is stored in the
  trusted root-key storage registers. In the V3 reference design, the ROTPK
  and hash can be found in plat/arm/board/common/rotpk/ folder of TF-A and is
  intended for development purposes. This SHA256 hash of the ROTPK is embedded
  into the BL1 and BL2 images.

* The BL1 image: The APUs in RD-V3 platforms are designed to prevent any
  unauthorized tampering or modification of the BL1 image.

The remaining components in the CoT are certificates and boot loader images.
The certificates follow the `X.509 v3`_ standard. It supports adding custom
extensions to the certificates, which are used to store essential information to
establish the CoT.

In the TBB CoT all certificates are self-signed. There is no need for a
Certificate Authority (CA) because the CoT is not established by verifying the
validity of a certificate's issuer but by the content of the certificate
extensions.

The certificates are categorised as "Key" and "Content" certificates. Key
certificates are used to verify public keys which have been used to sign content
certificates. Content certificates are used to store the hash of a boot loader
image. An image can be authenticated by calculating its hash and matching it
with the hash extracted from the content certificate. RD-V3 platform uses
SHA256 hash algorithm.


Required keys
=============

The keys used to establish the CCA vased CoT are:


Root of trust key
-----------------

The private part of this key is used to sign CCA content certificate. CCA
content certificate holds the hashes of BL2, BL31 and RMM images.


Platform root of trust key
--------------------------

The private part is used to sign platform key certificate. The platform key
certificate holds the hash of non-trusted firmware content certificate, and
the non-trusted firmware content certificate holds the hash of UEFI (BL33)
binary.


Core secure world key
---------------------

The private part of this key is used to sign core secure world key certificate.
The core secure world key certificate holds the hash of secure partition content
certificate, and secure partition content certificate hold the hash of SPM
binary (BL32).


Required certificates
=====================

The root of trust public key (ROTPK) will verify the CCA content certificate,
and the platform root of trust public key (PROTPK) will verify the platform key
certificate. All the firmware images are verified with the hash present in the
respective parent certificate.


CCA content certificate
-----------------------

BL1 stage authenticate the signature of CCA content certificate using ROTPK.
On success, the BL1 firmware loads BL2, calculate the BL2 image hash and compare
the hash with the one present in the certificate. If hash match, then BL2
is a trusted binary and BL1 handover the execution to BL2.

The same way BL2 image loads BL31 and RMM images and verify the hash with
CCA content certificate.


Platform key certificate
------------------------

Platform key certificate is used to authenticate the non-trusted firmware
content certificate.


Non-trusted firmware content certificate
----------------------------------------

Non-trusted firmware content certificate is used to authenticate the BL33
non-secure bootloader.


Core secure world key certificate
---------------------------------

Core secure world key certificate is used to authenticate the secure partition
content certificate.


Secure partition content certificate
------------------------------------

Secure partition content certificate is used to authenticate the BL32 image.



Chain of trust authentication
=============================

.. image:: /resources/cot/cot_cca.png


.. _X.509 v3: https://www.rfc-editor.org/rfc/rfc5280.txt
