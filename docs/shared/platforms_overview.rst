
.. list-table::
    :header-rows: 1

    * - Platform
      - Status
    * - :doc:`RD-V3-R1 Cfg1 </platforms/rdv3r1cfg1>`
      - Active
    * - :doc:`RD-V3-R1 </platforms/rdv3r1>`
      - Active
    * - :doc:`RD-V3 Cfg2 </platforms/rdv3cfg2>`
      - Active
    * - :doc:`RD-V3 Cfg1 </platforms/rdv3cfg1>`
      - Active
    * - :doc:`RD-V3 </platforms/rdv3>`
      - Active
    * - :doc:`RD-V2 </platforms/rdv2>`
      - Maintenance
    * - :doc:`RD-N2 Cfg3 </platforms/rdn2cfg3>`
      - Maintenance
    * - :doc:`RD-N2 Cfg2 </platforms/rdn2cfg2>`
      - Maintenance
    * - :doc:`RD-N2 Cfg1 </platforms/rdn2cfg1>`
      - Maintenance
    * - :doc:`RD-N2 </platforms/rdn2>`
      - Maintenance
    * - :doc:`RD-V1 </platforms/rdv1>`
      - Legacy
    * - :doc:`RD-V1 MC </platforms/rdv1mc>`
      - Legacy
    * - :doc:`RD-N1 Edge </platforms/rdn1edge>`
      - Legacy
    * - :doc:`RD-N1 Edge X2 </platforms/rdn1edgex2>`
      - Legacy
    * - :doc:`SGI-575 </platforms/sgi575>`
      - Legacy
