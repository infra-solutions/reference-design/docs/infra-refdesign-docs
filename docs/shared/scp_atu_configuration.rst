:orphan:

.. _scp_atu_config:

SCP Address Translation Unit (ATU) Configuration
================================================

Introduction
------------

The ATU provides means to convert the 32-bit logical address from the sub-system
memory map to the physical address residing in the system memory map with rich
security attributes.

The SCP is treated as a secure processor in the RD-V3 platform variants and
hence cannot access Root or Realm physical address space (PAS). But during the
platform boot, the SCP is required to generate transactions to root PAS for
configuring the CMN via the ATU.

Overview
--------

The SCP can access the AP memory map via Address Translation Window which must
be configured in the ATU. The ATU driver module in the SCP firmware configures
the ATU regions.

The SCP ATU is configured with the following translation regions:

+-------------+---------------------------+-------------------------------+--------+
| Region      | Logical Address Range     | Physical Address Range        | PAS    |
+=============+===========================+===============================+========+
| Cluster     | 0x6000_0000 - 0x6FFF_FFFF | 0x2_0000_0000 - 0x2_FFFF_FFFF | Root   |
| Utility     |                           |                               |        |
+-------------+---------------------------+-------------------------------+--------+
| AP Shared   | 0x7000_0000 - 0x77FF_FFFF | 0x0000_0000 - 0x07FF_FFFF     | Root   |
| SRAM        |                           |                               |        |
+-------------+---------------------------+-------------------------------+--------+
| GPC SMMU    | 0x7800_0000 - 0x780F_FFFF | 0x3_0000_0000 - 0x3_000F_FFFF | Root   |
|             |                           |                               |        |
+-------------+---------------------------+-------------------------------+--------+
| AP          | 0x7810_0000 - 0x784F_FFFF | 0x2F00_0000 - 0x2F3F_FFFF     | Secure |
| Peripherals |                           |                               |        |
+-------------+---------------------------+-------------------------------+--------+
| CMN         | 0xA000_0000 - 0xDFFF_FFFF | 0x1_0000_0000 - 0x1_3FFF_FFFF | Root   |
| CFGM        |                           |                               |        |
+-------------+---------------------------+-------------------------------+--------+

This SCP ATU driver implementation can be found in the in the following file in
the software stack: ``<workspace>/scp/module/atu/``

Runtime Configuration of ATU
----------------------------

In addition to the statically mapped translation regions if an additional ATU
region needs to be configured during the runtime, then the ATU API can be used
by the modules.

Please refer ``<workspace>/scp/module/atu/doc/atu.md``
