:orphan:

.. _rse:

***********************
Runtime Security Engine
***********************

Introduction
============

The Runtime Security Engine processor (RSE), formally the Runtime Security
Subsystem (RSS), is tasked with being the secure root of trust for platforms it
is included. The main tasks are securely loading firmware and provides secure
services such as storing encryption keys safely and secure
encryption/decryption without exposing the stored keys as part of CCA Hardware
Enforced Security (CCA HES) (see `CCA Security Model`). In addition RSE
provides attestation services and can store and return measurement data.

RSE firmware is a platform of the open source project `Trusted Firmware-M`_
(TF-M).

For further documentation see ``<workspace>/tf-m/docs/`` and ``<workspace>/tf-m
/docs/platform/arm/rse/``

Hardware Specification
======================

RSE integrates Cortex-M55 processor core, two volatile memory banks, Memory
Protection Controller (MPC), Non-Volatile ROM. In addition, RSE uses the
following peripherals:

* Address Translation Unit (ATU) - Maps part of RSE address space to address
  space of other subsystems.
* Crypto Accelerator - A private accelerator to perform encryption and
  decryption operation without involvement of the processor.
* Key Management Unit (KMU) - Stores keys securely in a way they can only be
  accessed by Crypto Accelerator ensuring keys are secure even from RSE.
* One Time Programable memory (OTP) - Memory that allows data to only be
  written once.

Bootloader
==========

The RSE Bootloader is split into 3 parts: BL1_1, BL1_2 and BL2 to provide a
balance between security and ease of update.

BL1_1 is run from the ROM and has the main job of initializing the OTP, loading
and running the provisioning bundles, and loading BL1_2 from the OTP into SRAM.
It is kept relatively simple as it cannot be changed once the ROM is
manufactured. For more details on provisioning see: `RSE Provisioning`_.

BL1_2 is loaded from OTP to run from SRAM and has the main job of initializing
the flash and loading BL2 from the flash into SRAM. This is easier to change as
it is in OTP which can be changed at manufacturing without changing hardware
like the ROM.

BL2 is loaded from flash to run from SRAM and has the main job of loading and
verifying the firmware for all subsystems. BL2 will load the firmware for SCP,
MCP, all LCPs, AP BL-1 and the RSE runtime firmware. The ATU is required to
access the address space of the other processors. To access LCP, the SCP needs
to have setup the CMN so this stage also initializes communication to
coordinate with the other processors (see :ref:`SCP-RSE Communication
<scp_rse_communication>`). This is also the point where the SCP ATU is
setup (see :ref:`SCP Address Translation Unit (ATU) Configuration
<scp_atu_config>`).

BL2 is a configuration of MCUBoot which uses headers and trailers to validate
the images. For more details see :ref:`Image Loading Using MCUBoot <mcuboot>`.

Lifecycle
=========

The chip lifecycle is split into 4 states.

1. Chip Manufacture (CM)
2. Device Manufacture (DM)
3. Secure Enabled (SE)
4. Return to Manufacture (RTM)

During CM and DM stages, secure data is loaded into the OTP and KMU from the
provisioning bundles for use in the SE stage. SE is the main stage the chip is
used in and the only stage that boots past BL1_1. RTM is only used for
decommissioning the chip.

Secure Encryption
=================

In RSE systems with a crypto accelerator and KMU secure keys for encryption are
only stored in the KMU. This provides an extra level of security as once keys
are stored and locked in the KMU they cannot be accessed by software. The only
way to use keys in the KMU is through a secure channel between the KMU and
crypto accelerator. This means even if the RSE runs compromised code the crypto
keys will still be secure as they are never in memory or accessible to the
processor during SE state.

If a crypto accelerator software cryptography must be used which will require
either the KMU is not locked or it is not used. If software cryptography is
used the keys will also need to be in memory. This is considered less secure.

Keys
----

The RSE stores several system keys in its KMU. Some of these are provisioned
during CM and DM lifecycle stages others are derived during the bootloader.
The KMU is split into two sections: up to 8 hardware keys and software keys in
the remaining slots. The hardware keys are fundamental keys required for secure
operation of the RSE and can only be set during secure provisioning using the
lifecycle manager and are always locked to software. The software keys can be
set during and phase until they are locked by software. These slots are often
used for keys derived from the hardware keys, so the hardware keys are not
used directly. They can also be used for specific but non-essential systems
such as encrypting RAM or storing keys for applications.

* **KRTL**

  * **Storage**: KMU HW
  * **Loading**: Configured in RTL by CM
  * **Usage**: Used to derive keys for secure provisioning

* **HUK**

  * **Storage**: KMU HW
  * **Loading**: Loaded to KMU by LCM via OTP during CM provisioning
  * **Usage**: Hardware Unique Key (HUK) is unique to this device and can be
    used to validate the device via derived keys. Do not use directly.
* **GUK**

  * **Storage**: KMU HW
  * **Loading**: Loaded to KMU by LCM via OTP during CM provisioning
  * **Usage**: Group Unique Key (GUK) is unique to a group of devices and is
    used to derive a variety of keys

* **KCE CM**

  * **Storage**: KMU HW
  * **Loading**: Loaded to KMU by LCM via OTP during CM provisioning
  * **Usage**: CM code encryption key, used to encrypt parts of OTP

* **KCE DM**

  * **Storage**: KMU HW
  * **Loading**: Loaded to KMU by LCM via OTP during DM provisioning
  * **Usage**: DM code encryption key, currently unused

* **CM Provisioning**

  * **Storage**: KMU SW
  * **Loading**: Derived from KRTL during CM provisioning
  * **Usage**: Used to decrypt the CM provisioning bundle

* **DM Provisioning**

  * **Storage**: KMU SW
  * **Loading**: Derived from KRTL during DM provisioning
  * **Usage**: Used to decrypt the DM provisioning bundle

* **VHUK**

  * **Storage**: KMU SW
  * **Loading**: Derived from HUK and GUK during BL1_2.
  * **Usage**: Virtual HUK (VHUK) is used as a unique identifier for the system
    and can be accessed by secure partitions in runtime. Currently only used in
    protected storage. Special case for multichip systems see :ref:`rse
    multichip keysharing`

* **IAK/CPAK SEED**

  * **Storage**: KMU SW
  * **Loading**: Derived from GUK during BL1_2
  * **Usage**: Used in attestation

* **DAK SEED**

  * **Storage**: KMU SW
  * **Loading**: Derived from GUK during BL1_2
  * **Usage**: Used in delegated attestation to create attestation key

* **Secure Encryption**

  * **Storage**: KMU SW
  * **Loading**: Stored into OTP during DM provisioning and decrypted with KCE
    CM and moved to KMU during BL1_2
  * **Usage**: Can be used to decrypt secure runtime firmware during BL2

* **Non-Secure Encryption**

  * **Storage**: KMU SW
  * **Loading**: Stored into OTP during DM provisioning and decrypted with KCE
    CM and moved to KMU during BL1_2
  * **Usage**: Can be used to decrypt non-secure runtime firmware during BL2

* **BL2 Encryption**

  * **Storage**: Encrypted OTP
  * **Loading**: Key is stored in OTP during DM provisioning and decrypted with
    KCE CM
  * **Usage**: Used to decrypt BL2 image during BL1_2

* **ROTPK**

  * **Storage**: OTP
  * **Loading**: Keys are stored in OTP during DM provisioning.
  * **Usage**: Root of Trust Public Key (ROTPK), there are multiple used for
    different parts of the system. Currently there is one key for BL2, one for
    Secure firmware, one for non-secure firmware and one for CCA. Accessible
    during runtime

.. _rse multichip keysharing:

Multichip Keysharing
--------------------

On a multichip system each chip will have its own RSE.

The whole system should have the same VHUK but each RSE has a different HUK so
data from each RSE needs to be used to derive the VHUK so a handshake is used
to share the data and cross validate each RSE.

To do this in BL1 the RSE need to be able to communicate without the main
interconnect as that will not be loaded until a later stage so require a
sideband communication method.

First the RSE must secure the connection over the sideband. Each RSE will
generate random data and share it with each other RSE. This will be combined
with the shared GUK to generate a shared session key. This key will then be
used to encrypt further communication on the sideband. This also validates that
each RSE has the same GUK as if they do not they will not generate the correct
session key.

Next the VHUK seed for each RSE is shared with each other RSE using the session
key to encrypt the data. The seeds from each RSE are combined and used to
generate the VHUK key.

The recommended way to do this handshake is using a Client-Server topology.
One RSE is allocated to be the server (normally RSE 0). Each client RSE will
send its data to the server RSE. The server RSE will collect this with its own
data then send all the data out to each client RSE. This will happen twice once
for the session key then again for the VHUK seeds.

.. _CCA Security Model_: https://developer.arm.com/documentation/DEN0096/A_a/?lang=en
.. _Trusted Firmware-M: https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git/
.. _RSE Provisioning: https://tf-m-user-guide.trustedfirmware.org/platform/arm/rse/rse_provisioning.html
