:orphan:

.. _ni_tower_system_control:

***********************
NI-Tower System Control
***********************

The system control NI-Tower is the interconnect in the system control block
which connects the AXI interfaces of RSE, MSCP, Debug block to the CMN-Cyprus
interconnect.

The System Control NI-Tower needs to be able to operate in a multi-chip
environment, and therefore the RSE is required to program the following memory
regions based on the value in the CHIP_ID register in the RSE Integration Layer
register block.

Following are the requester side interfaces present in the system control
NI-Tower:

1. rse_main_axis: Request from RSE.
2. scp_axis: Request from SCP.
3. mcp_axis: Request from MCP.
4. app_axis: Request from AP.
5. lcp_axis: Request from LCP targeting LCP/SCP
6. rse_scp_axis: RSE/SCP access which is filtered out by NIC400
   (specifically for region 0x200000000 - 0x23FFFFFFF) and targeted to LCP
   address space.

Following table shows the connections in the System Control Interconnect:


+---------------------------------------------+---------------+----------+----------+----------+----------+--------------+
|               |                             | rse_main_axis | scp_axis | mcp_axis | app_axis | lcp_axis | rse_scp_axis |
+===============+=============================+===============+==========+==========+==========+==========+==============+
|               |Accesses allowed to SCP TCM  |               |          |          |          |          |              |
| rse_scp_axim  |regions, SCP ATU, SCP RAS    |       X       |          |          |          |          |              |
|               |and INIT Ctrl registers.     |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|               |Accesses allowed to MCP TCM  |               |          |          |          |          |              |
| rse_mcp_axim  |regions, MCP ATU, MCP RAS    |       X       |          |          |          |          |              |
|               |and INIT Ctrl registers.     |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|    rsm_axim   |Shared RAM between RSE, SCP  |       X       |    X     |    X     |          |          |              |
|               |and MCP.                     |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|               |ECC Error record register    |       X       |    X     |    X     |          |          |              |
|    rsm_apbm   |block for Shared RAM between |               |          |          |          |          |              |
|               |RSE, SCP and MCP.            |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|    cmn_apbm   |CMN Configuration Space      |       X       |    X     |    X     |          |          |              |
|               |access.                      |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|               |TCU configuration path for   |               |          |          |          |          |              |
|    tcu_apbm   |RSE before the System Control|       X       |          |          |          |          |              |
|               |TBU is enabled.              |               |          |          |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|    lcp_axim   |LCP Access                   |               |          |          |          |          |      X       |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
|    app_axim   |AP Access                    |       X       |    X     |    X     |          |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
| app_scp_axim  |SCP Access                   |               |          |    X     |    X     |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
| app_mcp_axim  |MCP Access                   |               |          |          |    X     |          |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+
| lcp_mcp_axim  |SCP Access                   |               |          |          |          |    X     |              |
+---------------+-----------------------------+---------------+----------+----------+----------+----------+--------------+

The address map for PSAM and APU configuration can found here:
``<workspace>/tf-m/platform/ext/target/arm/rse/rdv3/bl2/ni_tower_sysctrl_lib.c``
