:orphan:

.. _platform_names:

Platform Names
==============

Arm's Neoverse reference design platforms are assigned names in order to allow
the build and execute scripts to recognize the platform for which the software
has to be built and executed. The names for the reference platforms are listed
below. Please make a note of the name of the platform of your choice. This name
is required to start the build and execute procedure. The names under 'Platform
Name' column has to be used in place of the placeholder ``<platform>`` as
mentioned in the commands in other documents.

+--------------------------+-------------------------+
| Reference Platform       | Platform Name           |
+==========================+=========================+
| RD-V3-R1                 | rdv3r1                  |
+--------------------------+-------------------------+
| RD-V3-R1-Cfg1            | rdv3r1cfg1              |
+--------------------------+-------------------------+
| RD-V3                    | rdv3                    |
+--------------------------+-------------------------+
| RD-V3-Cfg1               | rdv3cfg1                |
+--------------------------+-------------------------+
| RD-V3-Cfg2               | rdv3cfg2                |
+--------------------------+-------------------------+
| RD-V2                    | rdv2                    |
+--------------------------+-------------------------+
| RD-N2                    | rdn2                    |
+--------------------------+-------------------------+
| RD-N2-Cfg1               | rdn2cfg1                |
+--------------------------+-------------------------+
| RD-N2-Cfg2 (Quad chip)   | rdn2cfg2                |
+--------------------------+-------------------------+
| RD-N2-Cfg3               | rdn2cfg3                |
+--------------------------+-------------------------+
| RD-V1 (Single Chip)      | rdv1                    |
+--------------------------+-------------------------+
| RD-V1 (Quad Chip)        | rdv1mc                  |
+--------------------------+-------------------------+
| RD-N1-Edge (Single Chip) | rdn1edge                |
+--------------------------+-------------------------+
| RD-N1-Edge (Dual Chip)   | rdn1edgex2              |
+--------------------------+-------------------------+
| SGI-575                  | sgi575                  |
+--------------------------+-------------------------+
